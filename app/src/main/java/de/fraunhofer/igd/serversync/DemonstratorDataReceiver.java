package de.fraunhofer.igd.serversync;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import adapter.DoffedGetSet;
import adapter.FalldetectionGetSet;
import adapter.SOSGetSet;
import adapter.SleepGetSet;
import adapter.TUGGetSet;
import adapter.ThreeMWalkGetSet;
import adapter.TransactionGetSet;
import adapter.WalkGetSet;
import adapter.WaveGetSet;
import background.BatteryLevelReceiver;
import background.CommonMethods;
import background.ConnectionDetector;
import background.DataBaseHandler;
import background.JavaServiceCall;
import background.WatchServiceCall;

/**
 * Created by mhaescher on 23.08.16.
 */

public class DemonstratorDataReceiver extends BroadcastReceiver {
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    String imei_common;

    Bundle extras;

    int[] arrEventBattery;
    String[] arrStTimestamp;
    String[] arrEndTimestamp;
    int[] arrCurrentPosture;
    int[] arrTransitionMode;
    double[] arrPosTransitionTime;
    double[] arrPosTransitionIntensity;
    double[] arrDistanceWalked;
    long[] arrNumberOfSteps;
    double[] arrStepLength;
    double[] arrStepImpact;
    double[] arrInactivityScore;

    //ArrayList<Integer> arrEventBattery = new ArrayList<>();

    int solSleep;
    int tibSleep;
    int tstSleep;
    int wasoSleep;
    int seSleep;
    int postionChangeSleep;

    int eventBattery;
    String stTimestamp;
    String endTimestamp;
    double distanceWalked;
    long numberOfSteps;
    long doffedtime_pos_total_doffed_time;
    double inactivityScore;
    int hrMeasurement;
    int attentionRequest;
    double stepLength;
    double stepImpact;
    double fallGForceLevel;
    int fallConfidence;
    int arr_flag;
    int heartrate_hr_measurement;
    String eventMessage;
    File globalFile;

    public SaveToFile saveToFile = new SaveToFile();

    ArrayList<Integer> hrArrListEventBattery = new ArrayList<>();
    ArrayList<String> hrArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> hrArrListHrMeasurement = new ArrayList<>();

    ArrayList<Integer> dfArrListEventBattery = new ArrayList<>();
    ArrayList<String> dfArrListStTimestamp = new ArrayList<>();
    ArrayList<Long> dfArrListTotalDoffedTime = new ArrayList<>();

    ArrayList<Integer> tranArrListEventBattery = new ArrayList<>();
    ArrayList<String> tranArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> tranArrListCurrentPosture = new ArrayList<>();
    ArrayList<Integer> tranArrListTransitionMode = new ArrayList<>();
    ArrayList<Double> tranArrListPosTransitionTime = new ArrayList<>();
    ArrayList<Double> tranArrListPosTransitionIntensity = new ArrayList<>();

    ArrayList<Integer> walkArrListEventBattery = new ArrayList<>();
    ArrayList<String> walkArrListStTimestamp = new ArrayList<>();
    ArrayList<String> walkArrListEndTimestamp = new ArrayList<>();
    ArrayList<Double> walkArrListDistanceWalked = new ArrayList<>();
    ArrayList<Long> walkArrListNumberOfSteps = new ArrayList<>();
    ArrayList<Double> walkArrListStepLength = new ArrayList<>();
    ArrayList<Double> walkArrListStepImpact = new ArrayList<>();
    ArrayList<Double> walkArrListInactivityScore = new ArrayList<>();

    Context context_new;

    private static Timer timer = null;
    // private final int timerInterval = 2 * 60 * 60 * 1000;
    private final int timerInterval = 1 * 30 * 60 * 1000;
    //SOS  SQL Array
    ArrayList<String> sosArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> sosArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> sosArrListHrMeasurementdb = new ArrayList<>();

    // Walk SQL Array
    ArrayList<String> arrEventBatterydbMain = new ArrayList<>();
    ArrayList<String> arrStTimestampdbMain = new ArrayList<>();
    ArrayList<String> arrEndTimestampdbMain = new ArrayList<>();
    ArrayList<String> arrDistanceWalkeddbMain = new ArrayList<>();
    ArrayList<String> arrNumberOfStepsdbMain = new ArrayList<>();
    ArrayList<String> arrStepLengthdbMain = new ArrayList<>();
    ArrayList<String> arrStepImpactdbMain = new ArrayList<>();
    ArrayList<String> arrInactivityScoredbMain = new ArrayList<>();

    //TUG SQL Array
    ArrayList<String> tugArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> tugArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> tugArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> tugArrListDistanceWalkeddb = new ArrayList<>();
    ArrayList<String> tugArrListNumberOfStepsdb = new ArrayList<>();
    ArrayList<String> tugArrListInactivityScoredb = new ArrayList<>();

    //Wave SQL Array
    ArrayList<String> waveArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> waveArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> waveArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> waveArrListHrMeasurementdb = new ArrayList<>();
    ArrayList<String> waveArrListAttentionRequestdb = new ArrayList<>();

    //Fall Detection SQL Array
    ArrayList<String> fallArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> fallArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> fallArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> fallArrListDistanceWalkeddb = new ArrayList<>();
    ArrayList<String> fallArrListNumberOfStepsdb = new ArrayList<>();
    ArrayList<String> fallArrListStepImpactdb = new ArrayList<>();
    ArrayList<String> fallArrListStepLengthdb = new ArrayList<>();
    ArrayList<String> fallArrListForceLeveldb = new ArrayList<>();
    ArrayList<String> fallArrListFallConfidencedb = new ArrayList<>();

    //Sleep SQL Array
    ArrayList<String> sleepArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> sleepArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> sleepArrListEndTimestampb = new ArrayList<>();
    ArrayList<String> sleepArrListSolSLeepddb = new ArrayList<>();
    ArrayList<String> sleepArrListTibSleepsdb = new ArrayList<>();
    ArrayList<String> sleepArrListTstSleepdb = new ArrayList<>();
    ArrayList<String> sleepArrListWasoSleepdb = new ArrayList<>();
    ArrayList<String> sleepArrListSeSleepdb = new ArrayList<>();
    ArrayList<String> sleepArrListPositionChangeSleepdb = new ArrayList<>();

    //3 Min Walk SQL Array
    ArrayList<String> tmwArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> tmwArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> tmwArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> tmwArrListDistanceWalkeddb = new ArrayList<>();
    ArrayList<String> tmwArrListNumberOfStepsdb = new ArrayList<>();
    ArrayList<String> tmwArrListStepLengthdb = new ArrayList<>();
    ArrayList<String> tmwArrListStepImpactdb = new ArrayList<>();

    //Transaction SQL Array
    ArrayList<String> tranArrListEventBatterydbMain = new ArrayList<>();
    ArrayList<String> tranArrListStTimestampdbMain = new ArrayList<>();
    ArrayList<String> tranArrListCurrentPosturedbMain = new ArrayList<>();
    ArrayList<String> tranArrListTransitionModedbMain = new ArrayList<>();
    ArrayList<String> tranArrListPosTransitionTimedbMain = new ArrayList<>();
    ArrayList<String> tranArrListPosTransitionIntensitydbMain = new ArrayList<>();

    //Doffed SQL Array
    ArrayList<String> doffedArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> doffedArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> doffedArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> doffedArrListDoffedTimedb = new ArrayList<>();

    BatteryLevelReceiver mBatInfoReceiver;
    TextToSpeech t1;

    @Override
    public void onReceive(Context context, Intent intent) {
        // Log.e("CLASS_NAME ", "DemonstratorDataReceiver");

        context_new = context;
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        imei_common = CommonMethods.getIMEI(context.getApplicationContext());
        //  Log.e("IMEI", "" + imei_common);

        cd = new ConnectionDetector(context.getApplicationContext());

        eventMessage = intent.getStringExtra("MESSAGE_TYPE");



        Toast.makeText(context_new,eventMessage,Toast.LENGTH_SHORT).show();
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            getRuntimeConfigValues();

        }

     /*    t1=new TextToSpeech(context_new, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        t1.speak(eventMessage, TextToSpeech.QUEUE_FLUSH, null);
        t1.setPitch(300);*/

     /*   ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);*/





        /*try {
            if (timer == null) {
                scheduleUploadPeriodicTimer(context);
            } else {
            }
        } catch (Exception e) {


        }
*/
        //   Toast.makeText(context.getApplicationContext(), eventMessage, Toast.LENGTH_SHORT).show();
        if (intent.getStringExtra("MESSAGE_TYPE").equals("TRANSITION")) {


            extras = intent.getExtras();
            arrEventBattery = extras.getIntArray("EVENT_BATTERY");
            arrStTimestamp = extras.getStringArray("ST_TIMESTAMP");
            arrCurrentPosture = extras.getIntArray("CURRENT_POSTURE");
            arrTransitionMode = extras.getIntArray("TRANSITION_MODE");
            arrPosTransitionTime = extras.getDoubleArray("POS_TRANSITION_TIME");
            arrPosTransitionIntensity = extras.getDoubleArray("POS_TRANSITION_INTENSITY");

            arr_flag = extras.getInt("ARR_FLAG", 0);

            //  Toast.makeText(context.getApplicationContext(), "TRANSITION::" + arrTransitionMode, Toast.LENGTH_SHORT).show();

            for (int i = 0; i < extras.getInt("ARR_FLAG", 0); i++) {
                tranArrListEventBattery.add(arrEventBattery[i]);
                tranArrListStTimestamp.add(arrStTimestamp[i]);
                tranArrListCurrentPosture.add(arrCurrentPosture[i]);
                tranArrListTransitionMode.add(arrTransitionMode[i]);
                tranArrListPosTransitionTime.add(arrPosTransitionTime[i]);
                tranArrListPosTransitionIntensity.add(arrPosTransitionIntensity[i]);
            }


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arr_flag; i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBattery[i]);
                        jObjd.put("CURRENT_POSTURE", arrCurrentPosture[i]);
                        jObjd.put("TRANSITION_MODE", arrTransitionMode[i]);
                        jObjd.put("ST_TIMESTAMP", arrStTimestamp[i]);
                        jObjd.put("POS_TRANSITION_TIME", arrPosTransitionTime[i]);
                        jObjd.put("POS_TRANSITION_INTENSITY", arrPosTransitionIntensity[i]);

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);


                    JSONObject trnObj = new JSONObject();
                    trnObj.put("data", jsonArr);

                    SaveNSDSmartWatchData("nsd.smartwatch.transition", trnObj.toString());

                    //   TransactionSQLDataRetrieve(context);

                    // Toast.makeText(context.getApplicationContext(), "Transition online data uploading!", Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_transition);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKTransactionTask().execute(urlEncoded);
                }
                catch (Exception e)
                {

                }

            } else {

                try {
                    if (tranArrListEventBattery.size() > 0) {
                        String tranArrListEventBatteryCsv = tranArrListEventBattery.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String tranArrListStTimestampCsv = tranArrListStTimestamp.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String tranArrListCurrentPostureCsv = tranArrListCurrentPosture.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String tranArrListTransitionModeCsv = tranArrListTransitionMode.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String tranArrListPosTransitionTimeCsv = tranArrListPosTransitionTime.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String tranArrListPosTransitionIntensityCsv = tranArrListPosTransitionIntensity.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");

                        dataBaseHandler.addLocalDataTRANSACTION(new TransactionGetSet(tranArrListEventBatteryCsv, tranArrListStTimestampCsv, tranArrListCurrentPostureCsv, tranArrListTransitionModeCsv, tranArrListPosTransitionTimeCsv, tranArrListPosTransitionIntensityCsv));

                    } else {

                        //Toast.makeText(context.getApplicationContext(), "TRANSITION: Empty", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "TRANSITION: Exception", Toast.LENGTH_SHORT).show();

                }

                tranArrListEventBattery.clear();
                tranArrListStTimestamp.clear();
                tranArrListCurrentPosture.clear();
                tranArrListTransitionMode.clear();
                tranArrListPosTransitionTime.clear();
                tranArrListPosTransitionIntensity.clear();

            }
        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("TUG")) {


            extras = intent.getExtras();
            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
            numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
            inactivityScore = extras.getDouble("INACTIVITY_SCORE", 0);

            // Toast.makeText(context.getApplicationContext(), "TUG::" + inactivityScore, Toast.LENGTH_SHORT).show();

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                try
                {
                    JSONObject jsonObj = new JSONObject();

                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("INACTIVITY_SCORE", inactivityScore);


                    JSONObject tugObj = new JSONObject();
                    tugObj.put("data", jsonObj);

                    SaveNSDSmartWatchData("nsd.smartwatch.tug", tugObj.toString());


                    //TugSQLDataRetrieve(context);
                    //Toast.makeText(context.getApplicationContext(), "TUG online data uploading!", Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_tug);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKTugTask().execute(urlEncoded);
                }
                catch (Exception e)
                {

                }


            } else {

                try {

                    dataBaseHandler.addLocalDataTUG(new TUGGetSet(String.valueOf(eventBattery), stTimestamp, endTimestamp, String.valueOf(distanceWalked), String.valueOf(numberOfSteps), String.valueOf(inactivityScore)));

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "TUG: Exception", Toast.LENGTH_SHORT).show();

                }
            }


        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("SOS")) {


            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            hrMeasurement = extras.getInt("HR_MEASUREMENT", 0);

            // Toast.makeText(context.getApplicationContext(), "SOS::" + hrMeasurement, Toast.LENGTH_SHORT).show();


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("HR_MEASUREMENT", hrMeasurement);


                    JSONObject sosObj = new JSONObject();
                    sosObj.put("data", jsonObj);

                    SaveNSDSmartWatchData("nsd.smartwatch.sos", sosObj.toString());




                    // SOSSQLDataRetrieve(context);
                    //Toast.makeText(context.getApplicationContext(), "SOS online data uploading!", Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_sos);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKSOSTask().execute(urlEncoded);
                }
                catch (Exception e)
                {

                }


            } else {

                try {

                    dataBaseHandler.addLocalDataSOS(new SOSGetSet(String.valueOf(eventBattery), stTimestamp, String.valueOf(hrMeasurement)));

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "SOS: Exception", Toast.LENGTH_SHORT).show();

                }
            }


        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("WAVE")) {


            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            hrMeasurement = extras.getInt("HR_MEASUREMENT", 0);
            attentionRequest = extras.getInt("ATTENTION_REQUEST", 0);

            //  Toast.makeText(context.getApplicationContext(), "WAVE::" + attentionRequest, Toast.LENGTH_SHORT).show();

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("HR_MEASUREMENT", hrMeasurement);
                    jsonObj.put("ATTENTION_REQUEST", attentionRequest);



                    JSONObject wavwObj = new JSONObject();
                    wavwObj.put("data", jsonObj);

                    SaveNSDSmartWatchData("nsd.smartwatch.wave", wavwObj.toString());

                    // WaveSQLDataRetrieve(context);

                    //Toast.makeText(context.getApplicationContext(), "Wave online data uploading!", Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_wave_detected);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKWaveDetected().execute(urlEncoded);
                }
                catch (Exception e)
                {

                }

            } else {
                try {

                    dataBaseHandler.addLocalDataWave(new WaveGetSet(String.valueOf(eventBattery), stTimestamp, endTimestamp, String.valueOf(hrMeasurement), String.valueOf(attentionRequest)));

                } catch (Exception e) {
                    //    Toast.makeText(context.getApplicationContext(), "Wave: Exception", Toast.LENGTH_SHORT).show();

                }
            }
        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("FALL_DETECTION")) {


            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
            numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
            stepLength = extras.getDouble("STEP_LENGTH", 0);
            stepImpact = extras.getDouble("IMPACT", 0);
            fallGForceLevel = extras.getDouble("FALL_G_FORCE_LEVEL", 0);
            fallConfidence = extras.getInt("FALL_CONFIDENCE", 0);


            // Toast.makeText(context.getApplicationContext(), "Fall Detection::" + fallConfidence, Toast.LENGTH_SHORT).show();


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                // FallDetectionSQLDataRetrieve(context);

                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("FALL_CONFIDENCE", fallConfidence);
                    jsonObj.put("FALL_G_FORCE_LEVEL", fallGForceLevel);
                    jsonObj.put("STEP_LENGTH", stepLength);
                    jsonObj.put("IMPACT", stepImpact);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);


                    JSONObject fdObj = new JSONObject();

                    fdObj.put("data", jsonObj);

                    SaveNSDSmartWatchData("nsd.smartwatch.fall_detection", fdObj.toString());


                    String url_login = context.getResources().getString(R.string.ws_falldetection);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKFallDetectionTask().execute(urlEncoded);
                }
                catch (Exception e)
                {

                }

            } else {
                try {

                    dataBaseHandler.addLocalDataFallDetection(new FalldetectionGetSet(String.valueOf(eventBattery), stTimestamp, endTimestamp, String.valueOf(distanceWalked), String.valueOf(numberOfSteps), String.valueOf(stepLength), String.valueOf(stepImpact), String.valueOf(fallGForceLevel), String.valueOf(fallConfidence)));

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "Fall Detection: Exception", Toast.LENGTH_SHORT).show();

                }
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("WALK")) {


            extras = intent.getExtras();

            arrEventBattery = extras.getIntArray("EVENT_BATTERY");
            arrStTimestamp = extras.getStringArray("ST_TIMESTAMP");
            arrEndTimestamp = extras.getStringArray("END_TIMESTAMP");
            arrDistanceWalked = extras.getDoubleArray("DISTANCE_WALKED");
            arrNumberOfSteps = extras.getLongArray("NUMBER_OF_STEPS");
            arrStepLength = extras.getDoubleArray("STEP_LENGTH");
            arrStepImpact = extras.getDoubleArray("IMPACT");
            arrInactivityScore = extras.getDoubleArray("INACTIVITY_SCORE");
            arr_flag = extras.getInt("ARR_FLAG", 0);


            //Toast.makeText(context.getApplicationContext(), "WALK::" + arrDistanceWalked, Toast.LENGTH_SHORT).show();


            for (int i = 0; i < intent.getIntExtra("ARR_FLAG", 0); i++) {
                walkArrListEventBattery.add(arrEventBattery[i]);
                walkArrListStTimestamp.add(arrStTimestamp[i]);
                walkArrListEndTimestamp.add(arrEndTimestamp[i]);
                walkArrListDistanceWalked.add(arrDistanceWalked[i]);
                walkArrListNumberOfSteps.add(arrNumberOfSteps[i]);
                walkArrListStepLength.add(arrStepLength[i]);
                walkArrListStepImpact.add(arrStepImpact[i]);
                walkArrListInactivityScore.add(arrInactivityScore[i]);
            }

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {


                try {


                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arr_flag; i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBattery[i]);
                        jObjd.put("DISTANCE_WALKED", arrDistanceWalked[i]);
                        jObjd.put("NUMBER_OF_STEPS", arrNumberOfSteps[i]);
                        jObjd.put("ST_TIMESTAMP", arrStTimestamp[i]);
                        jObjd.put("END_TIMESTAMP", arrEndTimestamp[i]);
                        jObjd.put("STEP_LENGTH", arrStepLength[i]);
                        jObjd.put("IMPACT", arrStepImpact[i]);
                        jObjd.put("INACTIVITY_SCORE", arrInactivityScore[i]);

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);

                    SaveNSDSmartWatchData("nsd.smartwatch.walking",jsonObj.toString());

                    String url_login = context.getResources().getString(R.string.ws_walk);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKWalkingTask().execute(urlEncoded);


                } catch (JSONException ex) {

                }


                //  WalkSQLDataRetrieve(context);

                //Toast.makeText(context.getApplicationContext(), "Walk live data uploading!" , Toast.LENGTH_SHORT).show();


            } else {


                try {
                    if (walkArrListEventBattery.size() > 0) {

                        String arrEventBatteryCsv = walkArrListEventBattery.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrStTimestampCsv = walkArrListStTimestamp.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrEndTimestampCsv = walkArrListEndTimestamp.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrDistanceWalkedCsv = walkArrListDistanceWalked.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrNumberOfStepsCsv = walkArrListNumberOfSteps.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrStepLengthCsv = walkArrListStepLength.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrStepImpactCsv = walkArrListStepImpact.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");
                        String arrInactivityScoreCsv = walkArrListInactivityScore.toString().replace("[", "").replace("]", "")
                                .replace(", ", ",");

                        dataBaseHandler.addLocalDataWalk(new WalkGetSet(arrEventBatteryCsv, arrStTimestampCsv, arrEndTimestampCsv, arrDistanceWalkedCsv, arrNumberOfStepsCsv, arrStepLengthCsv, arrStepImpactCsv, arrInactivityScoreCsv));

                    } else {

                        //   Toast.makeText(context.getApplicationContext(), "Walking: Empty", Toast.LENGTH_SHORT).show();

                    }

                    walkArrListEventBattery.clear();
                    walkArrListStTimestamp.clear();
                    walkArrListEndTimestamp.clear();
                    walkArrListDistanceWalked.clear();
                    walkArrListNumberOfSteps.clear();
                    walkArrListStepLength.clear();
                    walkArrListStepImpact.clear();
                    walkArrListInactivityScore.clear();


                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "Walking: Exception", Toast.LENGTH_SHORT).show();

                }


            }


        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("SLEEP")) {


            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            solSleep = extras.getInt("SLP_SOL", 0);
            tibSleep = extras.getInt("SLP_TIB", 0);
            tstSleep = extras.getInt("SLP_TST", 0);
            wasoSleep = extras.getInt("SLP_WASO", 0);
            seSleep = extras.getInt("SLP_SE", 0);
            postionChangeSleep = extras.getInt("SLP_POSITION_CHANGES", 0);

            // Toast.makeText(context.getApplicationContext(), "SLEEP::" + seSleep, Toast.LENGTH_SHORT).show();


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("SLP_SOL", solSleep);
                    jsonObj.put("SLP_TIB", tibSleep);
                    jsonObj.put("SLP_TST", tstSleep);
                    jsonObj.put("SLP_WASO", wasoSleep);
                    jsonObj.put("SLP_SE", seSleep);
                    jsonObj.put("SLP_POSITION_CHANGES", postionChangeSleep);

                    JSONObject slpObj = new JSONObject();
                    slpObj.put("data", jsonObj);

                    SaveNSDSmartWatchData("nsd.smartwatch.sleep", slpObj.toString());


                    String url_login = context.getResources().getString(R.string.ws_sleeping);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKSleepingTask().execute(urlEncoded);
                }
                catch (Exception e)
                {

                }

            } else {

                try {

                    dataBaseHandler.addLocalDataSleep(new SleepGetSet(String.valueOf(eventBattery), stTimestamp, endTimestamp, String.valueOf(solSleep), String.valueOf(tibSleep), String.valueOf(tstSleep), String.valueOf(wasoSleep), String.valueOf(seSleep), String.valueOf(postionChangeSleep)));


                } catch (Exception e) {
                    //Toast.makeText(context.getApplicationContext(), "SLEEP: Exception", Toast.LENGTH_SHORT).show();

                }

            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("THREE_MIN_WALK")) {


            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
            numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
            stepLength = extras.getDouble("STEP_LENGTH", 0);
            stepImpact = extras.getDouble("IMPACT", 0);

            //   Toast.makeText(context.getApplicationContext(), "THREE_MIN_WALK::" + numberOfSteps, Toast.LENGTH_SHORT).show();


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                try{

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("STEP_LENGTH", stepLength);
                    jsonObj.put("IMPACT", stepImpact);


                    SaveNSDSmartWatchData("nsd.smartwatch.threeminwalk", jsonObj.toString());

                    // ThreeMinWalkSQLDataRetrieve(context);

                    // Toast.makeText(context.getApplicationContext(), "3MW Live data uploading!", Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_tmw);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);


                    new BKTMWTask().execute(urlEncoded);
                }
                catch(Exception e)
                {

                }

            } else {
                try {

                    dataBaseHandler.addLocalDataTMWalk(new ThreeMWalkGetSet(String.valueOf(eventBattery), stTimestamp, endTimestamp, String.valueOf(distanceWalked), String.valueOf(numberOfSteps), String.valueOf(stepLength), String.valueOf(stepImpact)));

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "3M Walk: Exception", Toast.LENGTH_SHORT).show();

                }

            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equalsIgnoreCase("HEART_RATE")) {

            //  Toast.makeText(context.getApplicationContext(), "HEART_RATE", Toast.LENGTH_SHORT).show();

            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            heartrate_hr_measurement = extras.getInt("HR_MEASUREMENT", 0);


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("i_imei", imei_common);
                    jsonObj.put("i_event_battery", eventBattery);
                    jsonObj.put("i_st_timestamp", stTimestamp);
                    jsonObj.put("i_hr_measurement", heartrate_hr_measurement);

                    // Toast.makeText(context.getApplicationContext(), "HR::" + jsonObj.toString(), Toast.LENGTH_LONG).show();

                    heartRateUpdate(context_new, jsonObj.toString());

                    SaveNSDSmartWatchData("nsd.smartwatch.heartrate", jsonObj.toString());

                } catch (JSONException ex) {
                    //    Log.e("REQUEST_EXCEPTION", "" + ex);
                }


            } else {


                try {

                    dataBaseHandler.addLocalDataHEARTRATE(String.valueOf(eventBattery), stTimestamp, String.valueOf(heartrate_hr_measurement));

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "3M Walk: Exception", Toast.LENGTH_SHORT).show();

                }


            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("DOFFED")) {


            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");

            //  endTimestamp = "0";
            doffedtime_pos_total_doffed_time = extras.getLong("POS_TOTAL_DOFFED_TIME", 0);

            Toast.makeText(context.getApplicationContext(), "DOFFED::" + doffedtime_pos_total_doffed_time, Toast.LENGTH_SHORT).show();


            // doffedtime_pos_total_doffed_time = extras.getLong("TOTAL_DOFFED_TIME", 0);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                // DoffedSQLDataRetrieve(context);
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("POS_TOTAL_DOFFED_TIME", doffedtime_pos_total_doffed_time);


                    SaveNSDSmartWatchData("nsd.smartwatch.doffed", jsonObj.toString());


                    String url_login = context.getResources().getString(R.string.ws_doffedtime);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKDoffedTask().execute(urlEncoded);

                }
                catch (Exception e)
                {

                }
            } else {

                try {

                    dataBaseHandler.addLocalDataDoffed(new DoffedGetSet(String.valueOf(eventBattery), stTimestamp, endTimestamp, String.valueOf(doffedtime_pos_total_doffed_time)));

                } catch (Exception e) {

                    //  Toast.makeText(context.getApplicationContext(), "3M Walk: Exception", Toast.LENGTH_SHORT).show();

                }
                //  Toast.makeText(context.getApplicationContext(), "Save Doffed Time offline", Toast.LENGTH_SHORT).show();
            }

        }

        //Added on 09-Feb-2018

        else if (intent.getStringExtra("MESSAGE_TYPE").equalsIgnoreCase("RESPIRATION_RATE")) {

            extras = intent.getExtras();
            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            int rr_measurement = extras.getInt("RR_MEASUREMENT", 0);


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("i_imei", imei_common);
                    jsonObj.put("i_event_battery", eventBattery);
                    jsonObj.put("i_st_timestamp", stTimestamp);
                    jsonObj.put("i_rr_measurement", rr_measurement);

                    // Toast.makeText(context.getApplicationContext(), "RR::" + jsonObj.toString(), Toast.LENGTH_LONG).show();

                    respirationRateUpdate(context_new, jsonObj.toString());

                    SaveNSDSmartWatchData("nsd.smartwatch.respirationrate", jsonObj.toString());

                } catch (JSONException ex) {

                }


            } else {

                try {
                    dataBaseHandler.addLocalDataRESPIRATIONRATE(String.valueOf(eventBattery), stTimestamp, String.valueOf(rr_measurement));
                } catch (Exception e) {

                }


            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("UNREGISTER_BROADCAST_RECEIVER")) {

            DataService.unregisterReceiver();

        }


    }

    public void heartRateUpdate(Context context, String req) {
        try {

            new JavaServiceCall(context) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {
                        if (getStatuscode().equals("200")) {

                            String hrres = getResult();
                            // Toast.makeText(context.getApplicationContext(), "HRResp::" + hrres, Toast.LENGTH_SHORT).show();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }


            }.execute(req, "SaveHeartRate");

        } catch (Exception ex) {

        }


    }

    public void respirationRateUpdate(Context context, String req) {
        try {

            new JavaServiceCall(context) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {
                        if (getStatuscode().equals("200")) {
                            String rres = getResult();
                            // Toast.makeText(context.getApplicationContext(), "RRResp::" + rres, Toast.LENGTH_SHORT).show();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }


            }.execute(req, "SaveRespirationRate");

        } catch (Exception ex) {

        }


    }

    private class BKHeartRateTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* dialog_login.show();
            dialog_login.setCancelable(false);
            dialog_login.setMessage("Data Uploading......");*/

        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("HR_MEASUREMENT", heartrate_hr_measurement);

                    // Log.e("URL_PARAMETERS", "" + jsonObj.toString());
                    writer.write(jsonObj.toString());
                    writer.flush();
                    //Log.e("Test", jsonObj.toString());
                } catch (JSONException ex) {
                    //    Log.e("REQUEST_EXCEPTION", "" + ex);
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            //Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                    // Log.e("code", "" + code);
                    // String message = new String(jsonObject.getString("message").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        //   WifiStateOff(context);


                        //  tvhint_msg.setText("Data Upload Success......");
                        //   Log.e("ERROR ", "Data Upload Success");


                    } else if (Integer.parseInt(code) == 1) {
                        // tvhint_msg.setText("Data Upload Failure......");
                        Log.e("ERROR ", "Data Upload failure");


                    } else {
                        //  tvhint_msg.setText("Data Failure......");
                        //  Log.e("ERROR ", "Data Upload failure");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                //  Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    private class BKDoffedTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* dialog_login.show();
            dialog_login.setCancelable(false);
            dialog_login.setMessage("Data Uploading......");*/

        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("POS_TOTAL_DOFFED_TIME", doffedtime_pos_total_doffed_time);

                    // Log.e("URL_PARAMETERS", "" + jsonObj.toString());
                    writer.write(jsonObj.toString());
                    writer.flush();
                    //Log.e("Test", jsonObj.toString());
                } catch (JSONException ex) {
                    // Log.e("REQUEST_EXCEPTION", "" + ex);
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            //   Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                    // Log.e("code", "" + code);
                    // String message = new String(jsonObject.getString("message").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        //  tvhint_msg.setText("Data Upload Success......");
                        // Log.e("ERROR ", "Data Upload Success");


                    } else if (Integer.parseInt(code) == 1) {
                        // tvhint_msg.setText("Data Upload Failure......");
                        // Log.e("ERROR ", "Data Upload failure");


                    } else {
                        //  tvhint_msg.setText("Data Failure......");
                        //   Log.e("ERROR ", "Data Upload failure");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                //   Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    // Start of Three minute walk Task
    private class BKTMWTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("STEP_LENGTH", stepLength);
                    jsonObj.put("IMPACT", stepImpact);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //    Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                //  Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Three minute walk Task

    // Start of Sleep Task
    private class BKSleepingTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("SLP_SOL", solSleep);
                    jsonObj.put("SLP_TIB", tibSleep);
                    jsonObj.put("SLP_TST", tstSleep);
                    jsonObj.put("SLP_WASO", wasoSleep);
                    jsonObj.put("SLP_SE", seSleep);
                    jsonObj.put("SLP_POSITION_CHANGES", postionChangeSleep);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);

                    // Toast.makeText(context_new.getApplicationContext(), "Sleepres::" + output, Toast.LENGTH_SHORT).show();
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //   Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                //  Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Sleep Task

    // Start of Walk Task
    private class BKWalkingTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arr_flag; i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBattery[i]);
                        jObjd.put("DISTANCE_WALKED", arrDistanceWalked[i]);
                        jObjd.put("NUMBER_OF_STEPS", arrNumberOfSteps[i]);
                        jObjd.put("ST_TIMESTAMP", arrStTimestamp[i]);
                        jObjd.put("END_TIMESTAMP", arrEndTimestamp[i]);
                        jObjd.put("STEP_LENGTH", arrStepLength[i]);
                        jObjd.put("IMPACT", arrStepImpact[i]);
                        jObjd.put("INACTIVITY_SCORE", arrInactivityScore[i]);

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);
                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        walkArrListEventBattery.clear();
                        walkArrListStTimestamp.clear();
                        walkArrListEndTimestamp.clear();
                        walkArrListDistanceWalked.clear();
                        walkArrListNumberOfSteps.clear();
                        walkArrListStepLength.clear();
                        walkArrListStepImpact.clear();
                        walkArrListInactivityScore.clear();

                        //   Log.e("ERROR ", "Data Upload Success");
                    } else {
                        walkArrListEventBattery.clear();
                        walkArrListStTimestamp.clear();
                        walkArrListEndTimestamp.clear();
                        walkArrListDistanceWalked.clear();
                        walkArrListNumberOfSteps.clear();
                        walkArrListStepLength.clear();
                        walkArrListStepImpact.clear();
                        walkArrListInactivityScore.clear();

                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    walkArrListEventBattery.clear();
                    walkArrListStTimestamp.clear();
                    walkArrListEndTimestamp.clear();
                    walkArrListDistanceWalked.clear();
                    walkArrListNumberOfSteps.clear();
                    walkArrListStepLength.clear();
                    walkArrListStepImpact.clear();
                    walkArrListInactivityScore.clear();

                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    walkArrListEventBattery.clear();
                    walkArrListStTimestamp.clear();
                    walkArrListEndTimestamp.clear();
                    walkArrListDistanceWalked.clear();
                    walkArrListNumberOfSteps.clear();
                    walkArrListStepLength.clear();
                    walkArrListStepImpact.clear();
                    walkArrListInactivityScore.clear();

                    e.printStackTrace();
                }
            } else {
                walkArrListEventBattery.clear();
                walkArrListStTimestamp.clear();
                walkArrListEndTimestamp.clear();
                walkArrListDistanceWalked.clear();
                walkArrListNumberOfSteps.clear();
                walkArrListStepLength.clear();
                walkArrListStepImpact.clear();
                walkArrListInactivityScore.clear();

                //  Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Walk Task

    // End of Fall Detection Task
    private class BKFallDetectionTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();


                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("FALL_CONFIDENCE", fallConfidence);
                    jsonObj.put("FALL_G_FORCE_LEVEL", fallGForceLevel);
                    jsonObj.put("STEP_LENGTH", stepLength);
                    jsonObj.put("IMPACT", stepImpact);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //    Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                //   Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Fall Detection Task

    // Start  of Wave Detection
    private class BKWaveDetected extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("HR_MEASUREMENT", hrMeasurement);
                    jsonObj.put("ATTENTION_REQUEST", attentionRequest);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //  Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                //   Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Wave Detection Task

    // Start of SOS Task
    private class BKSOSTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("HR_MEASUREMENT", hrMeasurement);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //  Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                //    Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    // End of SOS Task
    // Start of Tug Task
    private class BKTugTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("INACTIVITY_SCORE", inactivityScore);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            //  Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //   Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                //   Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    private class BKTransactionTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arr_flag; i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBattery[i]);
                        jObjd.put("CURRENT_POSTURE", arrCurrentPosture[i]);
                        jObjd.put("TRANSITION_MODE", arrTransitionMode[i]);
                        jObjd.put("ST_TIMESTAMP", arrStTimestamp[i]);
                        jObjd.put("POS_TRANSITION_TIME", arrPosTransitionTime[i]);
                        jObjd.put("POS_TRANSITION_INTENSITY", arrPosTransitionIntensity[i]);

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                    //Log.e("Test", jsonObj.toString());
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            //   Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                    if (Integer.parseInt(code) == 0) {
                        tranArrListEventBattery.clear();
                        tranArrListStTimestamp.clear();
                        tranArrListCurrentPosture.clear();
                        tranArrListTransitionMode.clear();
                        tranArrListPosTransitionTime.clear();
                        tranArrListPosTransitionIntensity.clear();
                        //  Log.e("ERROR ", "Data Upload Success");
                    } else {
                        tranArrListEventBattery.clear();
                        tranArrListStTimestamp.clear();
                        tranArrListCurrentPosture.clear();
                        tranArrListTransitionMode.clear();
                        tranArrListPosTransitionTime.clear();
                        tranArrListPosTransitionIntensity.clear();
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    tranArrListEventBattery.clear();
                    tranArrListStTimestamp.clear();
                    tranArrListCurrentPosture.clear();
                    tranArrListTransitionMode.clear();
                    tranArrListPosTransitionTime.clear();
                    tranArrListPosTransitionIntensity.clear();
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    tranArrListEventBattery.clear();
                    tranArrListStTimestamp.clear();
                    tranArrListCurrentPosture.clear();
                    tranArrListTransitionMode.clear();
                    tranArrListPosTransitionTime.clear();
                    tranArrListPosTransitionIntensity.clear();
                    e.printStackTrace();
                }
            } else {
                tranArrListEventBattery.clear();
                tranArrListStTimestamp.clear();
                tranArrListCurrentPosture.clear();
                tranArrListTransitionMode.clear();
                tranArrListPosTransitionTime.clear();
                tranArrListPosTransitionIntensity.clear();

                ///  tvhint_msg.setText("Data Upload Failure......");
                //   Log.e("ERROR ", "Data Upload failure");


            }

        }
    }


    private class BKSOSArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {
                    //Toast.makeText(context_new.getApplicationContext(), "ARRAY_LENGTH_1:-" + sosArrEVENT_BATTERY.size(), Toast.LENGTH_SHORT).show();

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < sosArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", sosArrListEventBatterydb.get(i));
                        jObjd.put("HR_MEASUREMENT", sosArrListHrMeasurementdb.get(i));
                        jObjd.put("ST_TIMESTAMP", sosArrListStTimestampdb.get(i));

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                //  Toast.makeText(context_new.getApplicationContext(), "SOS_URL_PAR:- Exception1", Toast.LENGTH_SHORT).show();

                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //  Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        //   Log.e("ERROR ", "Data Upload Success");

                        try {

                            for (int i = 0; i < sosArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataSOS();

                            }

                            sosArrListEventBatterydb.clear();
                            sosArrListStTimestampdb.clear();
                            sosArrListHrMeasurementdb.clear();


                        } catch (Exception e) {

                            sosArrListEventBatterydb.clear();
                            sosArrListStTimestampdb.clear();
                            sosArrListHrMeasurementdb.clear();

                        }

                    } else {

                        sosArrListEventBatterydb.clear();
                        sosArrListStTimestampdb.clear();
                        sosArrListHrMeasurementdb.clear();
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    sosArrListEventBatterydb.clear();
                    sosArrListStTimestampdb.clear();
                    sosArrListHrMeasurementdb.clear();
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    sosArrListEventBatterydb.clear();
                    sosArrListStTimestampdb.clear();
                    sosArrListHrMeasurementdb.clear();
                    e.printStackTrace();
                }
            } else {
                sosArrListEventBatterydb.clear();
                sosArrListStTimestampdb.clear();
                sosArrListHrMeasurementdb.clear();
                //   Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKTugArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                //Toast.makeText(context_new.getApplicationContext(), "ARRAY_LENGTH:-" + sosArrEVENT_BATTERY.size(), Toast.LENGTH_SHORT).show();


                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tugArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tugArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", tugArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", tugArrListEndTimestampdb.get(i));
                        jObjd.put("DISTANCE_WALKED", tugArrListDistanceWalkeddb.get(i));
                        jObjd.put("NUMBER_OF_STEPS", tugArrListNumberOfStepsdb.get(i));
                        jObjd.put("INACTIVITY_SCORE", tugArrListInactivityScoredb.get(i));

                        jsonArr.put(jObjd);
                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);
                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //   Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {


                        //tugArrListEventBatterydb, tugArrListStTimestampdb, tugArrListEndTimestampdb,
                        //tugArrListDistanceWalkeddb, tugArrListNumberOfStepsdb,tugArrListInactivityScoredb

                        try {
                            for (int i = 0; i < tugArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataTUG();

                            }
                        } catch (Exception e) {

                        }

                        tugArrListEventBatterydb.clear();
                        tugArrListStTimestampdb.clear();
                        tugArrListEndTimestampdb.clear();
                        tugArrListDistanceWalkeddb.clear();
                        tugArrListNumberOfStepsdb.clear();
                        tugArrListInactivityScoredb.clear();
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");

                        tugArrListEventBatterydb.clear();
                        tugArrListStTimestampdb.clear();
                        tugArrListEndTimestampdb.clear();
                        tugArrListDistanceWalkeddb.clear();
                        tugArrListNumberOfStepsdb.clear();
                        tugArrListInactivityScoredb.clear();
                    }
                } catch (JSONException e) {

                    tugArrListEventBatterydb.clear();
                    tugArrListStTimestampdb.clear();
                    tugArrListEndTimestampdb.clear();
                    tugArrListDistanceWalkeddb.clear();
                    tugArrListNumberOfStepsdb.clear();
                    tugArrListInactivityScoredb.clear();
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {

                    tugArrListEventBatterydb.clear();
                    tugArrListStTimestampdb.clear();
                    tugArrListEndTimestampdb.clear();
                    tugArrListDistanceWalkeddb.clear();
                    tugArrListNumberOfStepsdb.clear();
                    tugArrListInactivityScoredb.clear();
                    e.printStackTrace();
                }
            } else {

                tugArrListEventBatterydb.clear();
                tugArrListStTimestampdb.clear();
                tugArrListEndTimestampdb.clear();
                tugArrListDistanceWalkeddb.clear();
                tugArrListNumberOfStepsdb.clear();
                tugArrListInactivityScoredb.clear();
                //  Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKWaveArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < waveArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", waveArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", waveArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", waveArrListEndTimestampdb.get(i));
                        jObjd.put("HR_MEASUREMENT", waveArrListHrMeasurementdb.get(i));
                        jObjd.put("ATTENTION_REQUEST", waveArrListAttentionRequestdb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //   Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {


                        try {
                            for (int i = 0; i < waveArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataWave();

                            }
                        } catch (Exception e) {

                        }
                        waveArrListEventBatterydb.clear();
                        waveArrListStTimestampdb.clear();
                        waveArrListEndTimestampdb.clear();
                        waveArrListHrMeasurementdb.clear();
                        waveArrListAttentionRequestdb.clear();


                    } else {

                        waveArrListEventBatterydb.clear();
                        waveArrListStTimestampdb.clear();
                        waveArrListEndTimestampdb.clear();
                        waveArrListHrMeasurementdb.clear();
                        waveArrListAttentionRequestdb.clear();

                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {

                    waveArrListEventBatterydb.clear();
                    waveArrListStTimestampdb.clear();
                    waveArrListEndTimestampdb.clear();
                    waveArrListHrMeasurementdb.clear();
                    waveArrListAttentionRequestdb.clear();

                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {

                    waveArrListEventBatterydb.clear();
                    waveArrListStTimestampdb.clear();
                    waveArrListEndTimestampdb.clear();
                    waveArrListHrMeasurementdb.clear();
                    waveArrListAttentionRequestdb.clear();

                    e.printStackTrace();
                }
            } else {

                waveArrListEventBatterydb.clear();
                waveArrListStTimestampdb.clear();
                waveArrListEndTimestampdb.clear();
                waveArrListHrMeasurementdb.clear();
                waveArrListAttentionRequestdb.clear();

                //  Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKFallArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < fallArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", fallArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", fallArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", fallArrListEndTimestampdb.get(i));
                        jObjd.put("DISTANCE_WALKED", fallArrListDistanceWalkeddb.get(i));
                        jObjd.put("NUMBER_OF_STEPS", fallArrListNumberOfStepsdb.get(i));
                        jObjd.put("STEP_LENGTH", fallArrListStepLengthdb.get(i));
                        jObjd.put("IMPACT", fallArrListStepImpactdb.get(i));
                        jObjd.put("FALL_G_FORCE_LEVEL", fallArrListForceLeveldb.get(i));
                        jObjd.put("FALL_CONFIDENCE", fallArrListFallConfidencedb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //   Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {


                        try {
                            for (int i = 0; i < fallArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataFallDetection();

                            }
                        } catch (Exception e) {

                        }
                        fallArrListEventBatterydb.clear();
                        fallArrListStTimestampdb.clear();
                        fallArrListEndTimestampdb.clear();
                        fallArrListDistanceWalkeddb.clear();
                        fallArrListNumberOfStepsdb.clear();
                        fallArrListStepLengthdb.clear();
                        fallArrListStepImpactdb.clear();
                        fallArrListForceLeveldb.clear();
                        fallArrListFallConfidencedb.clear();


                    } else {
                        fallArrListEventBatterydb.clear();
                        fallArrListStTimestampdb.clear();
                        fallArrListEndTimestampdb.clear();
                        fallArrListDistanceWalkeddb.clear();
                        fallArrListNumberOfStepsdb.clear();
                        fallArrListStepLengthdb.clear();
                        fallArrListStepImpactdb.clear();
                        fallArrListForceLeveldb.clear();
                        fallArrListFallConfidencedb.clear();

                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    fallArrListEventBatterydb.clear();
                    fallArrListStTimestampdb.clear();
                    fallArrListEndTimestampdb.clear();
                    fallArrListDistanceWalkeddb.clear();
                    fallArrListNumberOfStepsdb.clear();
                    fallArrListStepLengthdb.clear();
                    fallArrListStepImpactdb.clear();
                    fallArrListForceLeveldb.clear();
                    fallArrListFallConfidencedb.clear();

                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    fallArrListEventBatterydb.clear();
                    fallArrListStTimestampdb.clear();
                    fallArrListEndTimestampdb.clear();
                    fallArrListDistanceWalkeddb.clear();
                    fallArrListNumberOfStepsdb.clear();
                    fallArrListStepLengthdb.clear();
                    fallArrListStepImpactdb.clear();
                    fallArrListForceLeveldb.clear();
                    fallArrListFallConfidencedb.clear();

                    e.printStackTrace();
                }
            } else {
                fallArrListEventBatterydb.clear();
                fallArrListStTimestampdb.clear();
                fallArrListEndTimestampdb.clear();
                fallArrListDistanceWalkeddb.clear();
                fallArrListNumberOfStepsdb.clear();
                fallArrListStepLengthdb.clear();
                fallArrListStepImpactdb.clear();
                fallArrListForceLeveldb.clear();
                fallArrListFallConfidencedb.clear();

                //  Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKSleepArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < sleepArrListEventBatterydb.size(); i++) {


                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", sleepArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", sleepArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", sleepArrListEndTimestampb.get(i));
                        jObjd.put("SLP_SOL", sleepArrListSolSLeepddb.get(i));
                        jObjd.put("SLP_TIB", sleepArrListTibSleepsdb.get(i));
                        jObjd.put("SLP_TST", sleepArrListTstSleepdb.get(i));
                        jObjd.put("SLP_WASO", sleepArrListWasoSleepdb.get(i));
                        jObjd.put("SLP_SE", sleepArrListSeSleepdb.get(i));
                        jObjd.put("SLP_POSITION_CHANGES", sleepArrListPositionChangeSleepdb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //  Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        try {

                            for (int i = 0; i < sleepArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataSleep();

                            }

                        } catch (Exception e) {

                        }
                        sleepArrListEventBatterydb.clear();
                        sleepArrListStTimestampdb.clear();
                        sleepArrListEndTimestampb.clear();
                        sleepArrListSolSLeepddb.clear();
                        sleepArrListTibSleepsdb.clear();
                        sleepArrListTstSleepdb.clear();
                        sleepArrListWasoSleepdb.clear();
                        sleepArrListSeSleepdb.clear();
                        sleepArrListPositionChangeSleepdb.clear();


                    } else {
                        sleepArrListEventBatterydb.clear();
                        sleepArrListStTimestampdb.clear();
                        sleepArrListEndTimestampb.clear();
                        sleepArrListSolSLeepddb.clear();
                        sleepArrListTibSleepsdb.clear();
                        sleepArrListTstSleepdb.clear();
                        sleepArrListWasoSleepdb.clear();
                        sleepArrListSeSleepdb.clear();
                        sleepArrListPositionChangeSleepdb.clear();

                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    sleepArrListEventBatterydb.clear();
                    sleepArrListStTimestampdb.clear();
                    sleepArrListEndTimestampb.clear();
                    sleepArrListSolSLeepddb.clear();
                    sleepArrListTibSleepsdb.clear();
                    sleepArrListTstSleepdb.clear();
                    sleepArrListWasoSleepdb.clear();
                    sleepArrListSeSleepdb.clear();
                    sleepArrListPositionChangeSleepdb.clear();

                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    sleepArrListEventBatterydb.clear();
                    sleepArrListStTimestampdb.clear();
                    sleepArrListEndTimestampb.clear();
                    sleepArrListSolSLeepddb.clear();
                    sleepArrListTibSleepsdb.clear();
                    sleepArrListTstSleepdb.clear();
                    sleepArrListWasoSleepdb.clear();
                    sleepArrListSeSleepdb.clear();
                    sleepArrListPositionChangeSleepdb.clear();

                    e.printStackTrace();
                }
            } else {
                sleepArrListEventBatterydb.clear();
                sleepArrListStTimestampdb.clear();
                sleepArrListEndTimestampb.clear();
                sleepArrListSolSLeepddb.clear();
                sleepArrListTibSleepsdb.clear();
                sleepArrListTstSleepdb.clear();
                sleepArrListWasoSleepdb.clear();
                sleepArrListSeSleepdb.clear();
                sleepArrListPositionChangeSleepdb.clear();

                //  Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKTMWArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tmwArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tmwArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", tmwArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", tmwArrListEndTimestampdb.get(i));
                        jObjd.put("DISTANCE_WALKED", tmwArrListDistanceWalkeddb.get(i));
                        jObjd.put("NUMBER_OF_STEPS", tmwArrListNumberOfStepsdb.get(i));
                        jObjd.put("STEP_LENGTH", tmwArrListStepLengthdb.get(i));
                        jObjd.put("IMPACT", tmwArrListStepImpactdb.get(i));
                        jsonArr.put(jObjd);

                    }
                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //  Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        try {

                            for (int i = 0; i < tmwArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataTMWalk();

                            }

                        } catch (Exception e) {

                        }

                        tmwArrListEventBatterydb.clear();
                        tmwArrListStTimestampdb.clear();
                        tmwArrListEndTimestampdb.clear();
                        tmwArrListDistanceWalkeddb.clear();
                        tmwArrListNumberOfStepsdb.clear();
                        tmwArrListStepLengthdb.clear();
                        tmwArrListStepImpactdb.clear();

                    } else {
                        tmwArrListEventBatterydb.clear();
                        tmwArrListStTimestampdb.clear();
                        tmwArrListEndTimestampdb.clear();
                        tmwArrListDistanceWalkeddb.clear();
                        tmwArrListNumberOfStepsdb.clear();
                        tmwArrListStepLengthdb.clear();
                        tmwArrListStepImpactdb.clear();
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    tmwArrListEventBatterydb.clear();
                    tmwArrListStTimestampdb.clear();
                    tmwArrListEndTimestampdb.clear();
                    tmwArrListDistanceWalkeddb.clear();
                    tmwArrListNumberOfStepsdb.clear();
                    tmwArrListStepLengthdb.clear();
                    tmwArrListStepImpactdb.clear();
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    tmwArrListEventBatterydb.clear();
                    tmwArrListStTimestampdb.clear();
                    tmwArrListEndTimestampdb.clear();
                    tmwArrListDistanceWalkeddb.clear();
                    tmwArrListNumberOfStepsdb.clear();
                    tmwArrListStepLengthdb.clear();
                    tmwArrListStepImpactdb.clear();
                    e.printStackTrace();
                }
            } else {
                tmwArrListEventBatterydb.clear();
                tmwArrListStTimestampdb.clear();
                tmwArrListEndTimestampdb.clear();
                tmwArrListDistanceWalkeddb.clear();
                tmwArrListNumberOfStepsdb.clear();
                tmwArrListStepLengthdb.clear();
                tmwArrListStepImpactdb.clear();
                //   Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKHeartRateArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < hrArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", hrArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", hrArrListStTimestamp.get(i));
                        jObjd.put("HR_MEASUREMENT", hrArrListHrMeasurement.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            //   Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        hrArrListStTimestamp.clear();
                        hrArrListStTimestamp.clear();
                        hrArrListEventBattery.clear();
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                //  Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKDoffedArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < dfArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", doffedArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", doffedArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", doffedArrListEndTimestampdb.get(i));
                        jObjd.put("POS_TOTAL_DOFFED_TIME", doffedArrListDoffedTimedb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        //WifiStateOff(context_new);
                        try {

                            for (int i = 0; i < doffedArrListEventBatterydb.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataDoffed();

                            }

                        } catch (Exception e) {

                        }
                        doffedArrListEventBatterydb.clear();
                        doffedArrListStTimestampdb.clear();
                        doffedArrListEndTimestampdb.clear();
                        doffedArrListDoffedTimedb.clear();


                    } else {
                        doffedArrListEventBatterydb.clear();
                        doffedArrListStTimestampdb.clear();
                        doffedArrListEndTimestampdb.clear();
                        doffedArrListDoffedTimedb.clear();

                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    doffedArrListEventBatterydb.clear();
                    doffedArrListStTimestampdb.clear();
                    doffedArrListEndTimestampdb.clear();
                    doffedArrListDoffedTimedb.clear();

                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    doffedArrListEventBatterydb.clear();
                    doffedArrListStTimestampdb.clear();
                    doffedArrListEndTimestampdb.clear();
                    doffedArrListDoffedTimedb.clear();

                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKTransArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tranArrListEventBatterydbMain.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tranArrListEventBatterydbMain.get(i));
                        jObjd.put("ST_TIMESTAMP", tranArrListStTimestampdbMain.get(i));
                        jObjd.put("CURRENT_POSTURE", tranArrListCurrentPosturedbMain.get(i));
                        jObjd.put("TRANSITION_MODE", tranArrListTransitionModedbMain.get(i));
                        jObjd.put("POS_TRANSITION_TIME", tranArrListPosTransitionTimedbMain.get(i));
                        jObjd.put("POS_TRANSITION_INTENSITY", tranArrListPosTransitionIntensitydbMain.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        try {

                            for (int i = 0; i < tranArrListEventBatterydbMain.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataTRANSACTION();

                            }

                        } catch (Exception e) {

                        }

                        tranArrListEventBatterydbMain.clear();
                        tranArrListStTimestampdbMain.clear();
                        tranArrListCurrentPosturedbMain.clear();
                        tranArrListTransitionModedbMain.clear();
                        tranArrListPosTransitionTimedbMain.clear();
                        tranArrListPosTransitionIntensitydbMain.clear();
                    } else {
                        tranArrListEventBatterydbMain.clear();
                        tranArrListStTimestampdbMain.clear();
                        tranArrListCurrentPosturedbMain.clear();
                        tranArrListTransitionModedbMain.clear();
                        tranArrListPosTransitionTimedbMain.clear();
                        tranArrListPosTransitionIntensitydbMain.clear();
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    tranArrListEventBatterydbMain.clear();
                    tranArrListStTimestampdbMain.clear();
                    tranArrListCurrentPosturedbMain.clear();
                    tranArrListTransitionModedbMain.clear();
                    tranArrListPosTransitionTimedbMain.clear();
                    tranArrListPosTransitionIntensitydbMain.clear();
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    tranArrListEventBatterydbMain.clear();
                    tranArrListStTimestampdbMain.clear();
                    tranArrListCurrentPosturedbMain.clear();
                    tranArrListTransitionModedbMain.clear();
                    tranArrListPosTransitionTimedbMain.clear();
                    tranArrListPosTransitionIntensitydbMain.clear();
                    e.printStackTrace();
                }
            } else {
                tranArrListEventBatterydbMain.clear();
                tranArrListStTimestampdbMain.clear();
                tranArrListCurrentPosturedbMain.clear();
                tranArrListTransitionModedbMain.clear();
                tranArrListPosTransitionTimedbMain.clear();
                tranArrListPosTransitionIntensitydbMain.clear();
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKTWalkArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arrEventBatterydbMain.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBatterydbMain.get(i));
                        jObjd.put("ST_TIMESTAMP", arrStTimestampdbMain.get(i));
                        jObjd.put("END_TIMESTAMP", arrEndTimestampdbMain.get(i));
                        jObjd.put("DISTANCE_WALKED", arrDistanceWalkeddbMain.get(i));
                        jObjd.put("NUMBER_OF_STEPS", arrNumberOfStepsdbMain.get(i));
                        jObjd.put("STEP_LENGTH", arrStepLengthdbMain.get(i));
                        jObjd.put("IMPACT", arrStepImpactdbMain.get(i));
                        jObjd.put("INACTIVITY_SCORE", arrInactivityScoredbMain.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        //WifiStateOff(context_new);
                        try {

                            for (int i = 0; i < arrEventBatterydbMain.size(); i++) {

                                new DataBaseHandler(context_new.getApplicationContext()).deleteLocalDataWalk();

                            }

                        } catch (Exception e) {

                        }
                        arrEventBatterydbMain.clear();
                        arrStTimestampdbMain.clear();
                        arrEndTimestampdbMain.clear();
                        arrDistanceWalkeddbMain.clear();
                        arrNumberOfStepsdbMain.clear();
                        arrStepLengthdbMain.clear();
                        arrStepImpactdbMain.clear();
                        arrInactivityScoredbMain.clear();

                    } else {
                        arrEventBatterydbMain.clear();
                        arrStTimestampdbMain.clear();
                        arrEndTimestampdbMain.clear();
                        arrDistanceWalkeddbMain.clear();
                        arrNumberOfStepsdbMain.clear();
                        arrStepLengthdbMain.clear();
                        arrStepImpactdbMain.clear();
                        arrInactivityScoredbMain.clear();
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    arrEventBatterydbMain.clear();
                    arrStTimestampdbMain.clear();
                    arrEndTimestampdbMain.clear();
                    arrDistanceWalkeddbMain.clear();
                    arrNumberOfStepsdbMain.clear();
                    arrStepLengthdbMain.clear();
                    arrStepImpactdbMain.clear();
                    arrInactivityScoredbMain.clear();
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    arrEventBatterydbMain.clear();
                    arrStTimestampdbMain.clear();
                    arrEndTimestampdbMain.clear();
                    arrDistanceWalkeddbMain.clear();
                    arrNumberOfStepsdbMain.clear();
                    arrStepLengthdbMain.clear();
                    arrStepImpactdbMain.clear();
                    arrInactivityScoredbMain.clear();
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
                arrEventBatterydbMain.clear();
                arrStTimestampdbMain.clear();
                arrEndTimestampdbMain.clear();
                arrDistanceWalkeddbMain.clear();
                arrNumberOfStepsdbMain.clear();
                arrStepLengthdbMain.clear();
                arrStepImpactdbMain.clear();
                arrInactivityScoredbMain.clear();
            }

        }
    }

    private void WifiStateOff(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(false);
    }

    private void WifiStateOn(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }

    private void scheduleUploadPeriodicTimer(final Context context) {

        Log.e("TIMER_STATUS", "START");
        this.timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                DateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy - HH:mm:ss");
                Calendar calendar = Calendar.getInstance();
                //dateFormat.format(calendar.getTime());
                Log.e("TIMER_STATUS", "COMPLETED:- " + dateFormat.format(calendar.getTime()));

                WifiStateOn(context);
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    public void run() {
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {

                            TransactionSQLDataRetrieve(context);
                            TugSQLDataRetrieve(context);
                            SOSSQLDataRetrieve(context);
                            WaveSQLDataRetrieve(context);
                            FallDetectionSQLDataRetrieve(context);
                            WalkSQLDataRetrieve(context);
                            SleepSQLDataRetrieve(context);
                            ThreeMinWalkSQLDataRetrieve(context);

                        } else {

                        }
                    }
                });


            }
        }, 0, timerInterval);
    }

    private void TransactionSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {
            List<TransactionGetSet> transactionGetSets = dataBaseHandler.getAllLocalDataTRANSACTION();

            ArrayList<String> tranArrListEventBatterydb = new ArrayList<>();
            ArrayList<String> tranArrListStTimestampdb = new ArrayList<>();
            ArrayList<String> tranArrListCurrentPosturedb = new ArrayList<>();
            ArrayList<String> tranArrListTransitionModedb = new ArrayList<>();
            ArrayList<String> tranArrListPosTransitionTimedb = new ArrayList<>();
            ArrayList<String> tranArrListPosTransitionIntensitydb = new ArrayList<>();

            for (TransactionGetSet walking : transactionGetSets) {

                tranArrListEventBatterydb.add(walking.getEvent_battery());
                tranArrListStTimestampdb.add(walking.getSt_timestamp());
                tranArrListCurrentPosturedb.add(walking.getCurrent_posture());
                tranArrListTransitionModedb.add(walking.getTransaction_mode());
                tranArrListPosTransitionTimedb.add(walking.getPos_transaction_time());
                tranArrListPosTransitionIntensitydb.add(walking.getPos_transaction_intansity());


            }

            for (int i = 0; i < tranArrListEventBatterydb.size(); i++) {

                ArrayList<String> tranArrListEventBatterydb1 = new ArrayList(Arrays.asList(tranArrListEventBatterydb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListStTimestampdb1 = new ArrayList(Arrays.asList(tranArrListStTimestampdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListCurrentPosturedb1 = new ArrayList(Arrays.asList(tranArrListCurrentPosturedb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListTransitionModedb1 = new ArrayList(Arrays.asList(tranArrListTransitionModedb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListPosTransitionTimedb1 = new ArrayList(Arrays.asList(tranArrListPosTransitionTimedb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListPosTransitionIntensitydb1 = new ArrayList(Arrays.asList(tranArrListPosTransitionIntensitydb.get(i).split("\\s*,\\s*")));

                for (int j = 0; j < tranArrListEventBatterydb1.size(); j++) {
                    tranArrListEventBatterydbMain.add(tranArrListEventBatterydb1.get(j));
                    tranArrListStTimestampdbMain.add(tranArrListStTimestampdb1.get(j));
                    tranArrListCurrentPosturedbMain.add(tranArrListCurrentPosturedb1.get(j));
                    tranArrListTransitionModedbMain.add(tranArrListTransitionModedb1.get(j));
                    tranArrListPosTransitionTimedbMain.add(tranArrListPosTransitionTimedb1.get(j));
                    tranArrListPosTransitionIntensitydbMain.add(tranArrListPosTransitionIntensitydb1.get(j));

                }

            }

            if (!tranArrListEventBatterydbMain.isEmpty()) {

                //  Toast.makeText(context.getApplicationContext(), "Transition SQL data uploading..." + tranArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_transition);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTransArrayTaskNew().execute(urlEncoded);

            }


        } catch (Exception e) {

            //   Toast.makeText(context.getApplicationContext(), "TRANSITION: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void TugSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {

            List<TUGGetSet> tugGetSets = dataBaseHandler.getAllLocalDataTUG();

            for (TUGGetSet tugGetSet : tugGetSets) {

                tugArrListEventBatterydb.add(tugGetSet.getEvent_battery());
                tugArrListStTimestampdb.add(tugGetSet.getSt_timestamp());
                tugArrListEndTimestampdb.add(tugGetSet.getEnd_timestamp());
                tugArrListDistanceWalkeddb.add(tugGetSet.getDistance_walked());
                tugArrListNumberOfStepsdb.add(tugGetSet.getNum_of_steps());
                tugArrListInactivityScoredb.add(tugGetSet.getInactivity_score());


            }

            if (!tugArrListEventBatterydb.isEmpty()) {
                // Toast.makeText(context.getApplicationContext(), "TUG offline data uploading!" + tugArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_tug_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTugArrayTaskNew().execute(urlEncoded);

            }
        } catch (Exception e) {
            //   Toast.makeText(context.getApplicationContext(), "TUG: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void SOSSQLDataRetrieve(Context context) {

        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {
            List<SOSGetSet> sosGetSets = dataBaseHandler.getAllLocalDataSOS();

            for (SOSGetSet walking : sosGetSets) {

                sosArrListEventBatterydb.add(walking.getEvent_battery());
                sosArrListStTimestampdb.add(walking.getSt_timestamp());
                sosArrListHrMeasurementdb.add(walking.getHr_measurement());

            }

            if (!sosArrListEventBatterydb.isEmpty()) {

                //Toast.makeText(context.getApplicationContext(), "SOS SQL data Uploading..." + sosArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_sos_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKSOSArrayTaskNew().execute(urlEncoded);
            }


        } catch (Exception e) {

            //   Toast.makeText(context.getApplicationContext(), "SOS: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void WaveSQLDataRetrieve(Context context) {

        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {
            List<WaveGetSet> waveGetSets = dataBaseHandler.getAllLocalDataWave();


            for (WaveGetSet waveGetSet : waveGetSets) {

                waveArrListEventBatterydb.add(waveGetSet.getEvent_battery());
                waveArrListStTimestampdb.add(waveGetSet.getSt_timestamp());
                waveArrListEndTimestampdb.add(waveGetSet.getEnd_timestamp());
                waveArrListHrMeasurementdb.add(waveGetSet.getHr_measurement());
                waveArrListAttentionRequestdb.add(waveGetSet.getAttention_request());
            }

            if (!waveArrListEventBatterydb.isEmpty()) {

                //  Toast.makeText(context.getApplicationContext(), "Wave SQL data Uploading..." + waveArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

                //Toast.makeText(context.getApplicationContext(), "Wave offline data uploading!" + waveArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_wave_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKWaveArrayTaskNew().execute(urlEncoded);

            }
        } catch (Exception e) {

            //   Toast.makeText(context.getApplicationContext(), "Wave: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void FallDetectionSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {

            List<FalldetectionGetSet> fallLocalDataFallDetections = dataBaseHandler.getAllLocalDataFallDetection();

            for (FalldetectionGetSet fallLocalDataFallDetection : fallLocalDataFallDetections) {

                fallArrListEventBatterydb.add(fallLocalDataFallDetection.getEvent_battery());
                fallArrListStTimestampdb.add(fallLocalDataFallDetection.getSt_timestamp());
                fallArrListEndTimestampdb.add(fallLocalDataFallDetection.getEnd_timestamp());
                fallArrListDistanceWalkeddb.add(fallLocalDataFallDetection.getDistance_walked());
                fallArrListNumberOfStepsdb.add(fallLocalDataFallDetection.getNum_of_steps());
                fallArrListStepImpactdb.add(fallLocalDataFallDetection.getStep_impact());
                fallArrListStepLengthdb.add(fallLocalDataFallDetection.getStep_length());
                fallArrListForceLeveldb.add(fallLocalDataFallDetection.getFall_gforce_level());
                fallArrListFallConfidencedb.add(fallLocalDataFallDetection.getFall_confidence());

            }
            if (!fallArrListEventBatterydb.isEmpty()) {
                //       Toast.makeText(context.getApplicationContext(), "Fall offline data uploading!" + fallArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_falldetection_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKFallArrayTaskNew().execute(urlEncoded);

            }

        } catch (Exception e) {
            //  Toast.makeText(context.getApplicationContext(), "Fall Detection: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void WalkSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {
            List<WalkGetSet> walkGetSets = dataBaseHandler.getAllLocalDataWalk();
            ArrayList<String> arrEventBatterydb = new ArrayList<>();
            ArrayList<String> arrStTimestampdb = new ArrayList<>();
            ArrayList<String> arrEndTimestampdb = new ArrayList<>();
            ArrayList<String> arrDistanceWalkeddb = new ArrayList<>();
            ArrayList<String> arrNumberOfStepsdb = new ArrayList<>();
            ArrayList<String> arrStepLengthdb = new ArrayList<>();
            ArrayList<String> arrStepImpactdb = new ArrayList<>();
            ArrayList<String> arrInactivityScoredb = new ArrayList<>();

            for (WalkGetSet walking : walkGetSets) {
                arrEventBatterydb.add(walking.getEvent_battery());
                arrStTimestampdb.add(walking.getSt_timestamp());
                arrEndTimestampdb.add(walking.getEnd_timestamp());
                arrDistanceWalkeddb.add(walking.getDistance_walked());
                arrNumberOfStepsdb.add(walking.getNum_of_steps());
                arrStepLengthdb.add(walking.getStep_length());
                arrStepImpactdb.add(walking.getImpact());
                arrInactivityScoredb.add(walking.getInactivity_score());

            }


            for (int i = 0; i < arrEventBatterydb.size(); i++) {

                ArrayList<String> arrEventBatterydb1 = new ArrayList(Arrays.asList(arrEventBatterydb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrStTimestampdb1 = new ArrayList(Arrays.asList(arrStTimestampdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrEndTimestampdb1 = new ArrayList(Arrays.asList(arrEndTimestampdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrDistanceWalkeddb1 = new ArrayList(Arrays.asList(arrDistanceWalkeddb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrNumberOfStepsdb1 = new ArrayList(Arrays.asList(arrNumberOfStepsdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrStepLengthdb1 = new ArrayList(Arrays.asList(arrStepLengthdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrStepImpactdb1 = new ArrayList(Arrays.asList(arrStepImpactdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrInactivityScoredb1 = new ArrayList(Arrays.asList(arrInactivityScoredb.get(i).split("\\s*,\\s*")));

                for (int j = 0; j < arrEventBatterydb1.size(); j++) {
                    arrEventBatterydbMain.add(arrEventBatterydb1.get(j));
                    arrStTimestampdbMain.add(arrStTimestampdb1.get(j));
                    arrEndTimestampdbMain.add(arrEndTimestampdb1.get(j));
                    arrDistanceWalkeddbMain.add(arrDistanceWalkeddb1.get(j));
                    arrNumberOfStepsdbMain.add(arrNumberOfStepsdb1.get(j));
                    arrStepLengthdbMain.add(arrStepLengthdb1.get(j));
                    arrStepImpactdbMain.add(arrStepImpactdb1.get(j));
                    arrInactivityScoredbMain.add(arrInactivityScoredb1.get(j));
                }

            }

            if (!arrEventBatterydbMain.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "Walk SQL data Uploading..." + arrEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_walk);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTWalkArrayTaskNew().execute(urlEncoded);
            }
        } catch (Exception e) {

            //  Toast.makeText(context.getApplicationContext(), "Walking: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void SleepSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);
        try {
            List<SleepGetSet> sleepGetSets = dataBaseHandler.getAllLocalDataSleep();


            for (SleepGetSet sleepGetSet : sleepGetSets) {

                sleepArrListEventBatterydb.add(sleepGetSet.getEvent_battery());
                sleepArrListStTimestampdb.add(sleepGetSet.getSt_timestamp());
                sleepArrListEndTimestampb.add(sleepGetSet.getEnd_timestamp());
                sleepArrListSolSLeepddb.add(sleepGetSet.getSlp_sol());
                sleepArrListTibSleepsdb.add(sleepGetSet.getSlp_tib());
                sleepArrListTstSleepdb.add(sleepGetSet.getSlp_tst());
                sleepArrListWasoSleepdb.add(sleepGetSet.getSlp_waso());
                sleepArrListSeSleepdb.add(sleepGetSet.getSlp_se());
                sleepArrListPositionChangeSleepdb.add(sleepGetSet.getSlp_position_changes());

            }

            if (!sleepArrListEventBatterydb.isEmpty()) {
                // Toast.makeText(context.getApplicationContext(), "Sleep SQL data uploading..." + sleepArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_sleep_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKSleepArrayTaskNew().execute(urlEncoded);

            }
        } catch (Exception e) {
            //   Toast.makeText(context.getApplicationContext(), "SLEEP: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    private void ThreeMinWalkSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {
            List<ThreeMWalkGetSet> threeMWalkGetSetLists = dataBaseHandler.getAllLocalDataTMWalk();

            for (ThreeMWalkGetSet threeMWalkGetSetList : threeMWalkGetSetLists) {

                tmwArrListEventBatterydb.add(threeMWalkGetSetList.getEvent_battery());
                tmwArrListStTimestampdb.add(threeMWalkGetSetList.getSt_timestamp());
                tmwArrListEndTimestampdb.add(threeMWalkGetSetList.getEnd_timestamp());
                tmwArrListDistanceWalkeddb.add(threeMWalkGetSetList.getDistance_walked());
                tmwArrListNumberOfStepsdb.add(threeMWalkGetSetList.getNum_of_steps());
                tmwArrListStepLengthdb.add(threeMWalkGetSetList.getStep_length());
                tmwArrListStepImpactdb.add(threeMWalkGetSetList.getImpact());

            }
            if (!tmwArrListEventBatterydb.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "3MW SQL data uploading..." + tmwArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_tmw_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTMWArrayTaskNew().execute(urlEncoded);

            }
        } catch (Exception e) {

        }
    }

    private void DoffedSQLDataRetrieve(Context context) {
        DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

        try {
            List<DoffedGetSet> doffedGetSetLists = dataBaseHandler.getAllLocalDataDoffed();

            for (DoffedGetSet threeMWalkGetSetList : doffedGetSetLists) {

                doffedArrListEventBatterydb.add(threeMWalkGetSetList.getEvent_battery());
                doffedArrListStTimestampdb.add(threeMWalkGetSetList.getSt_timestamp());
                doffedArrListEndTimestampdb.add(threeMWalkGetSetList.getEnd_timestamp());
                doffedArrListDoffedTimedb.add(threeMWalkGetSetList.getDoffed_time());

            }
            if (!doffedArrListEventBatterydb.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "3MW SQL data uploading..." + tmwArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_doffedtime_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKDoffedArrayTaskNew().execute(urlEncoded);

            }
        } catch (Exception e) {

        }
    }


    public void SaveNSDSmartWatchData(String datasrc,String sourcedata) {
        try {




            JSONObject walkjsonObj = new JSONObject();
               /* {
                    "i_imei": "362531821612104",
                        "i_data_source": "nsd.smartwatch.walking",
                        "i_source_data": "{\"data\":\"test\"}"
                }*/


            walkjsonObj.put("i_source_data", datasrc);
            walkjsonObj.put("i_data_source", sourcedata);
            walkjsonObj.put("i_imei", imei_common);



            new WatchServiceCall(context_new) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {


                        if (getStatuscode().equals("200")) {
                            String hresp = getResult();
                            Toast.makeText(context_new.getApplicationContext(), "NSDWalk_On:" + hresp, Toast.LENGTH_SHORT).show();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }


            }.execute(walkjsonObj.toString(), "SaveNSDSmartWatchData");

        } catch (Exception ex) {

        }
    }


    public void getRuntimeConfigValues() {
        try {




            JSONObject rtjsonObj = new JSONObject();


            rtjsonObj.put("i_imei", imei_common);



            new JavaServiceCall(context_new) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {


                        if (getStatuscode().equals("200")) {
                            String hresp = getResult();


                            Toast.makeText(context_new.getApplicationContext(), "NSDRT:" + hresp, Toast.LENGTH_SHORT).show();
                            //  con = context_new.createPackageContext("com.sharedpref1", 0);//first app package name is "com.sharedpref1"

                          /*  Intent sendIntent = new Intent();
                            //  sendIntent.setClassName("de.fraunhofer.igd.serversync","de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");

                            ComponentName componentName=new ComponentName("de.fraunhofer.igd.nsddemonstrator","CONFIGURATION_MESSAGE");
                            sendIntent.setComponent(componentName);
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra("ConfigValues", hresp);
                            sendIntent.setType("text/plain");
                            context_new.startActivity(sendIntent);
*/

                            final Intent sendIntent = new Intent();

                            //  sendIntent.setAction("com.nsd.ncare.beta.network.MyReceive");

                            sendIntent.setAction("de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");
                            sendIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                            sendIntent.putExtra("CONTENT_STRING",hresp);
                            context_new.sendBroadcast(sendIntent);



                            // Toast.makeText(context_new.getApplicationContext(), "NSDWalk_On:" + hresp, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }


            }.execute(rtjsonObj.toString(), "GetRuntimeConfigValues");

        } catch (Exception ex) {

        }
    }

}
