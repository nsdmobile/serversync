package de.fraunhofer.igd.serversync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import background.CommonMethods;
import background.ConnectionDetector;

/**
 * Created by mhaescher on 23.08.16.
 */

public class DemonstratorDataReceiverUpload extends BroadcastReceiver {
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    String imei_common;

    Bundle extras;

    int[] arrEventBattery;
    String[] arrStTimestamp;
    String[] arrEndTimestamp;
    int[] arrCurrentPosture;
    int[] arrTransitionMode;
    double[] arrPosTransitionTime;
    double[] arrPosTransitionIntensity;
    double[] arrDistanceWalked;
    long[] arrNumberOfSteps;
    double[] arrStepLength;
    double[] arrStepImpact;
    double[] arrInactivityScore;

    int solSleep;
    int tibSleep;
    int tstSleep;
    int wasoSleep;
    int seSleep;
    int postionChangeSleep;

    int eventBattery;
    String stTimestamp;
    String endTimestamp;
    double distanceWalked;
    long numberOfSteps;
    long doffedtime_pos_total_doffed_time;
    double inactivityScore;
    int hrMeasurement;
    int attentionRequest;
    double stepLength;
    double stepImpact;
    double fallGForceLevel;
    int fallConfidence;
    int arr_flag;
    int heartrate_hr_measurement;
    String eventMessage;
    File globalFile;

    public SaveToFile saveToFile = new SaveToFile();

    // Surya Start

    ArrayList<Integer> tugArrListEventBattery = new ArrayList<>();
    ArrayList<String> tugArrListStTimestamp = new ArrayList<>();
    ArrayList<String> tugArrListEndTimestamp = new ArrayList<>();
    ArrayList<Double> tugArrListDistanceWalked = new ArrayList<>();
    ArrayList<Long> tugArrListNumberOfSteps = new ArrayList<>();
    ArrayList<Double> tugArrListInactivityScore = new ArrayList<>();

    ArrayList<Integer> waveArrListEventBattery = new ArrayList<>();
    ArrayList<Integer> waveArrListHrMeasurement = new ArrayList<>();
    ArrayList<String> waveArrListEndTimestamp = new ArrayList<>();
    ArrayList<String> waveArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> waveArrListAttentionRequest = new ArrayList<>();


    ArrayList<Integer> fallArrListEventBattery = new ArrayList<>();
    ArrayList<String> fallArrListStTimestamp = new ArrayList<>();
    ArrayList<String> fallAListEndTimestamp = new ArrayList<>();
    ArrayList<Double> fallArrListDistanceWalked = new ArrayList<>();
    ArrayList<Long> fallArrListNumberOfSteps = new ArrayList<>();
    ArrayList<Double> fallArrListStepLength = new ArrayList<>();
    ArrayList<Double> fallArrListStepImpact = new ArrayList<>();
    ArrayList<Double> fallArrListForceLevel = new ArrayList<>();
    ArrayList<Integer> fallArrListFallConfidence = new ArrayList<>();


    ArrayList<Integer> sleepArrListEventBattery = new ArrayList<>();
    ArrayList<String> sleepArrListStTimestamp = new ArrayList<>();
    ArrayList<String> sleepArrListEndTimestamp = new ArrayList<>();
    ArrayList<Integer> sleepArrListSolSLeep = new ArrayList<>();
    ArrayList<Integer> sleepArrListTibSleep = new ArrayList<>();
    ArrayList<Integer> sleepArrListTstSleep = new ArrayList<>();
    ArrayList<Integer> sleepArrListWasoSleep = new ArrayList<>();
    ArrayList<Integer> sleepArrListSeSleep = new ArrayList<>();
    ArrayList<Integer> sleepArrListPositionChangeSleep = new ArrayList<>();

    ArrayList<Integer> tmwArrListEventBattery = new ArrayList<>();
    ArrayList<String> tmwArrListStTimestamp = new ArrayList<>();
    ArrayList<String> tmwArrListEndTimestamp = new ArrayList<>();
    ArrayList<Double> tmwArrListDistanceWalked = new ArrayList<>();
    ArrayList<Long> tmwArrListNumberOfSteps = new ArrayList<>();
    ArrayList<Double> tmwArrListStepLength = new ArrayList<>();
    ArrayList<Double> tmwArrListStepImpact = new ArrayList<>();

    ArrayList<Integer> hrArrListEventBattery = new ArrayList<>();
    ArrayList<String> hrArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> hrArrListHrMeasurement = new ArrayList<>();

    ArrayList<Integer> dfArrListEventBattery = new ArrayList<>();
    ArrayList<String> dfArrListStTimestamp = new ArrayList<>();
    ArrayList<Long> dfArrListTotalDoffedTime = new ArrayList<>();

    ArrayList<Integer> tranArrListEventBattery = new ArrayList<>();
    ArrayList<String> tranArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> tranArrListCurrentPosture = new ArrayList<>();
    ArrayList<Integer> tranArrListTransitionMode = new ArrayList<>();
    ArrayList<Double> tranArrListPosTransitionTime = new ArrayList<>();
    ArrayList<Double> tranArrListPosTransitionIntensity = new ArrayList<>();

    ArrayList<Integer> walkArrListEventBattery = new ArrayList<>();
    ArrayList<String> walkArrListStTimestamp = new ArrayList<>();
    ArrayList<String> walkArrListEndTimestamp = new ArrayList<>();
    ArrayList<Double> walkArrListDistanceWalked = new ArrayList<>();
    ArrayList<Long> walkArrListNumberOfSteps = new ArrayList<>();
    ArrayList<Double> walkArrListStepLength = new ArrayList<>();
    ArrayList<Double> walkArrListStepImpact = new ArrayList<>();
    ArrayList<Double> walkArrListInactivityScore = new ArrayList<>();

    // Surya End

    ArrayList<Integer> sosArrEVENT_BATTERY = new ArrayList<>();
    ArrayList<String> sosArrST_TIMESTAMP = new ArrayList<>();
    ArrayList<Integer> sosArrHR_MEASUREMENT = new ArrayList<>();

    private boolean isReading = false;
    private boolean isPaused = false;
    private boolean isCanceled = false;
    private long timeRemaining = 0;

    private boolean isReadingUpload = false;
    private boolean isPausedUpload = false;
    private boolean isCanceledUpload = false;
    private long timeRemainingUpload = 0;
    private long timeRemainUploadSec = 0;

    Context context_new;
    private long fileUploadInterval = 60; //Plz change this to (60 * 60)

    private String uploadUrl = "http://www.nextstepdynamics.com/riskfactor/webservices/uploadexcel";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("CLASS_NAME ", "DemonstratorDataReceiver");

        context_new = context;

        imei_common = CommonMethods.getIMEI(context.getApplicationContext());
        Log.e("IMEI", "" + imei_common);

        cd = new ConnectionDetector(context.getApplicationContext());

        // WifiStateOn(context);
       /* Toast.makeText(context.getApplicationContext(), "Number of offline records:" +
                        "Tran :" + tranArrListEventBattery.size() + "\n" +
                        "TUG :" + tugArrListEventBattery.size() + "\n" +
                        "SOS :" + sosArrEVENT_BATTERY.size() + "\n" +
                        "WAVE :" + waveArrListEventBattery.size() + "\n" +
                        "WALK :" + walkArrListEventBattery.size() + "\n" +
                        "FALL :" + fallArrListEventBattery.size() + "\n"
                , Toast.LENGTH_SHORT).show();*/

        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            CancelTimer();
            StartTimer(context);

        }
        //File Upload timer start
        CancelTimerUpload();
        StartTimerUpload(context);

        eventMessage = intent.getStringExtra("MESSAGE_TYPE");
        if (intent.getStringExtra("MESSAGE_TYPE").equals("TRANSITION")) {

            extras = intent.getExtras();
            arrEventBattery = extras.getIntArray("EVENT_BATTERY");
            arrStTimestamp = extras.getStringArray("ST_TIMESTAMP");
            arrCurrentPosture = extras.getIntArray("CURRENT_POSTURE");
            arrTransitionMode = extras.getIntArray("TRANSITION_MODE");
            arrPosTransitionTime = extras.getDoubleArray("POS_TRANSITION_TIME");
            arrPosTransitionIntensity = extras.getDoubleArray("POS_TRANSITION_INTENSITY");

            arr_flag = extras.getInt("ARR_FLAG", 0);

            /*for (int i = 0; i < extras.getInt("ARR_FLAG", 0); i++) {
                Toast.makeText(context.getApplicationContext(), "TRANSITION" + "\n" +
                        arrStTimestamp[i] + " " + "\n" +
                        arrCurrentPosture[i] + " " + "\n" +
                        arrTransitionMode[i] + " " + "\n" +
                        arrPosTransitionTime[i] + " " + "\n" +
                        arrPosTransitionIntensity[i], Toast.LENGTH_LONG).show();
            }*/

            // WifiStateOn(context);

            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                if (!tranArrListEventBattery.isEmpty()) {
                    //Toast.makeText(context.getApplicationContext(), "Transition offline data uploading!" + tranArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_transition);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKTransArrayTaskNew().execute(urlEncoded);

                }
                //Toast.makeText(context.getApplicationContext(), "Transition online data uploading!" + arr_flag, Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_transition);
                url_login = context.getResources().getString(R.string.ws_transition);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTransactionTask().execute(urlEncoded);

               /* //File Upload Code
                File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                final File myFile = new File(fileDirectory, strName);
*/
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                  //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {

                //  Toast.makeText(context.getApplicationContext(), "Saving Transition offline", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < extras.getInt("ARR_FLAG", 0); i++) {
                    tranArrListEventBattery.add(arrEventBattery[i]);
                    tranArrListStTimestamp.add(arrStTimestamp[i]);
                    tranArrListCurrentPosture.add(arrCurrentPosture[i]);
                    tranArrListTransitionMode.add(arrTransitionMode[i]);
                    tranArrListPosTransitionTime.add(arrPosTransitionTime[i]);
                    tranArrListPosTransitionIntensity.add(arrPosTransitionIntensity[i]);
                }
            }
        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("TUG")) {

            extras = intent.getExtras();
            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
            numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
            inactivityScore = extras.getDouble("INACTIVITY_SCORE", 0);

          /*  Toast.makeText(context.getApplicationContext(), "TUG" + "\n" +
                    stTimestamp + " " + "\n" +
                    endTimestamp + " " + "\n" +
                    distanceWalked + " " + "\n" +
                    numberOfSteps + " " + "\n" +
                    inactivityScore, Toast.LENGTH_LONG).show();*/

            //  WifiStateOn(context);

            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                if (!tugArrListEventBattery.isEmpty()) {

                    // Toast.makeText(context.getApplicationContext(), "TUG offline data uploading!" + tugArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_tug_array);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKTugArrayTaskNew().execute(urlEncoded);

                }
                // Toast.makeText(context.getApplicationContext(), "TUG online data uploading!", Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_tug);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTugTask().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 5) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {

                // Toast.makeText(context.getApplicationContext(), "Saving TUG offline", Toast.LENGTH_SHORT).show();
                tugArrListEventBattery.add(eventBattery);
                tugArrListStTimestamp.add(stTimestamp);
                tugArrListEndTimestamp.add(endTimestamp);
                tugArrListDistanceWalked.add(distanceWalked);
                tugArrListNumberOfSteps.add(numberOfSteps);
                tugArrListInactivityScore.add(inactivityScore);
            }


        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("SOS")) {

            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            hrMeasurement = extras.getInt("HR_MEASUREMENT", 0);


          /*  Toast.makeText(context.getApplicationContext(), "SOS" + "\n" +
                    eventBattery + " " + "\n" +
                    stTimestamp + " " + "\n" +
                    hrMeasurement, Toast.LENGTH_LONG).show();*/

            // WifiStateOn(context);

            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                // Toast.makeText(context.getApplicationContext(), "SOS online data uploading!", Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_sos);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKSOSTask().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {
                // Toast.makeText(context.getApplicationContext(), "Saving SOS offline", Toast.LENGTH_SHORT).show();
                sosArrEVENT_BATTERY.add(eventBattery);
                sosArrST_TIMESTAMP.add(stTimestamp);
                sosArrHR_MEASUREMENT.add(hrMeasurement);
            }
        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("WAVE")) {

            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            hrMeasurement = extras.getInt("HR_MEASUREMENT", 0);
            attentionRequest = extras.getInt("ATTENTION_REQUEST", 0);

          /*  Toast.makeText(context.getApplicationContext(), "WAVE" + "\n" +
                    intent.getStringExtra("ST_TIMESTAMP") + "\n" +
                    intent.getStringExtra("END_TIMESTAMP") + "\n" +
                    intent.getIntExtra("HR_MEASUREMENT", 0) + "\n" +
                    intent.getIntExtra("ATTENTION_REQUEST", 0), Toast.LENGTH_LONG).show();*/

            //  WifiStateOn(context);
            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                // Toast.makeText(context.getApplicationContext(), "Wave online data uploading!" + tugArrListEventBattery.size(), Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_wave_detected);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKWaveDetected().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }
            } else {
                // Toast.makeText(context.getApplicationContext(), "Saving Wave offline", Toast.LENGTH_SHORT).show();
                waveArrListEventBattery.add(eventBattery);
                waveArrListStTimestamp.add(stTimestamp);
                waveArrListEndTimestamp.add(endTimestamp);
                waveArrListHrMeasurement.add(hrMeasurement);
                waveArrListAttentionRequest.add(attentionRequest);
            }
        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("FALL_DETECTION")) {
            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
            numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
            stepLength = extras.getDouble("STEP_LENGTH", 0);
            stepImpact = extras.getDouble("IMPACT", 0);
            fallGForceLevel = extras.getDouble("FALL_G_FORCE_LEVEL", 0);
            fallConfidence = extras.getInt("FALL_CONFIDENCE", 0);
/*
            Toast.makeText(context.getApplicationContext(), "FALL_DETECTION" + "\n" +
                    stTimestamp + "-->" + endTimestamp + " " + "\n" +
                    "Distance: " + distanceWalked + ";" +
                    "Steps: " + numberOfSteps + " " + "\n" +
                    "Length: " + stepLength + " " + ";" +
                    "Impact: " + stepImpact + " " + "\n" +
                    "Force: " + fallGForceLevel + " " + ";" +
                    "Conf: " + fallConfidence, Toast.LENGTH_LONG).show();*/

            // WifiStateOn(context);
            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                // Toast.makeText(context.getApplicationContext(), "Saving Fall Online data uploading", Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_falldetection);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKFallDetectionTask().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {
                // Toast.makeText(context.getApplicationContext(), "Saving Fall offline", Toast.LENGTH_SHORT).show();
                fallArrListEventBattery.add(eventBattery);
                fallArrListStTimestamp.add(stTimestamp);
                fallAListEndTimestamp.add(endTimestamp);
                fallArrListDistanceWalked.add(distanceWalked);
                fallArrListNumberOfSteps.add(numberOfSteps);
                fallArrListStepImpact.add(stepImpact);
                fallArrListStepLength.add(stepLength);
                fallArrListForceLevel.add(fallGForceLevel);
                fallArrListFallConfidence.add(fallConfidence);
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("WALK")) {
            extras = intent.getExtras();

            arrEventBattery = extras.getIntArray("EVENT_BATTERY");
            arrStTimestamp = extras.getStringArray("ST_TIMESTAMP");
            arrEndTimestamp = extras.getStringArray("END_TIMESTAMP");
            arrDistanceWalked = extras.getDoubleArray("DISTANCE_WALKED");
            arrNumberOfSteps = extras.getLongArray("NUMBER_OF_STEPS");
            arrStepLength = extras.getDoubleArray("STEP_LENGTH");
            arrStepImpact = extras.getDoubleArray("IMPACT");
            arrInactivityScore = extras.getDoubleArray("INACTIVITY_SCORE");
            arr_flag = extras.getInt("ARR_FLAG", 0);

           /* for (int i = 0; i < intent.getIntExtra("ARR_FLAG", 0); i++) {
                Toast.makeText(context.getApplicationContext(), "WALK" + "\n" +
                        arrStTimestamp[i] + "-->" + arrEndTimestamp[i] + " " + "\n" +
                        "Distance: " + arrDistanceWalked[i] + ";" +
                        "Steps: " + arrNumberOfSteps[i] + " " + "\n" +
                        "Length: " + arrStepLength[i] + ";" +
                        "Impact: " + arrStepImpact[i] + " " + "\n" +
                        "Score: " + arrInactivityScore[i], Toast.LENGTH_LONG).show();
            }*/
            //   WifiStateOn(context);

            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                if (!walkArrListEventBattery.isEmpty()) {
                    //  Toast.makeText(context.getApplicationContext(), "Walk offline data uploading!" + walkArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_walk);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKTWalkArrayTaskNew().execute(urlEncoded);
                }
                //  Toast.makeText(context.getApplicationContext(), "Walk live data uploading!" + intent.getIntExtra("ARR_FLAG", 0), Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_walk);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKWalkingTask().execute(urlEncoded);


                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {
                //  Toast.makeText(context.getApplicationContext(), "Save Walk offline", Toast.LENGTH_SHORT).show();

                for (int i = 0; i < intent.getIntExtra("ARR_FLAG", 0); i++) {
                    walkArrListEventBattery.add(arrEventBattery[i]);
                    walkArrListStTimestamp.add(arrStTimestamp[i]);
                    walkArrListEndTimestamp.add(arrEndTimestamp[i]);
                    walkArrListDistanceWalked.add(arrDistanceWalked[i]);
                    walkArrListNumberOfSteps.add(arrNumberOfSteps[i]);
                    walkArrListStepLength.add(arrStepLength[i]);
                    walkArrListStepImpact.add(arrStepImpact[i]);
                    walkArrListInactivityScore.add(arrInactivityScore[i]);
                }
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("SLEEP")) {

            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            solSleep = extras.getInt("SLP_SOL", 0);
            tibSleep = extras.getInt("SLP_TIB", 0);
            tstSleep = extras.getInt("SLP_TST", 0);
            wasoSleep = extras.getInt("SLP_WASO", 0);
            seSleep = extras.getInt("SLP_SE", 0);
            postionChangeSleep = extras.getInt("SLP_POSITION_CHANGES", 0);


           /* Toast.makeText(context.getApplicationContext(), "SLEEP" + "\n" +
                    intent.getStringExtra("ST_TIMESTAMP") + "-->" +
                    intent.getStringExtra("END_TIMESTAMP") + "\n" +
                    "Sol: " + intent.getIntExtra("SLP_SOL", 0) + ";" +
                    "Tib: " + intent.getIntExtra("SLP_TIB", 0) + "\n" +
                    "Tst: " + intent.getIntExtra("SLP_TST", 0) + ";" +
                    "SE: " + intent.getIntExtra("SLP_SE", 0) + "\n" +
                    "Waso: " + intent.getIntExtra("SLP_WASO", 0) + ";" +
                    "Pos: " + intent.getIntExtra("SLP_POSITION_CHANGES", 0), Toast.LENGTH_LONG).show();*/

            // WifiStateOn(context);
            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);

            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                if (!sleepArrListEventBattery.isEmpty()) {
                    // Toast.makeText(context.getApplicationContext(), "Sleep offline data uploading!" + sleepArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_sleep_array);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKSleepArrayTaskNew().execute(urlEncoded);

                }
                // Toast.makeText(context.getApplicationContext(), "Sleep LIve data uploading!", Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_sleeping);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKSleepingTask().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {
                //  Toast.makeText(context.getApplicationContext(), "Save Sleep offline", Toast.LENGTH_SHORT).show();

                sleepArrListEventBattery.add(eventBattery);
                sleepArrListStTimestamp.add(stTimestamp);
                sleepArrListEndTimestamp.add(endTimestamp);
                sleepArrListPositionChangeSleep.add(postionChangeSleep);
                sleepArrListSeSleep.add(seSleep);
                sleepArrListSolSLeep.add(solSleep);
                sleepArrListTstSleep.add(tstSleep);
                sleepArrListTibSleep.add(tibSleep);
                sleepArrListWasoSleep.add(wasoSleep);
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("THREE_MIN_WALK")) {

            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            endTimestamp = extras.getString("END_TIMESTAMP");
            distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
            numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
            stepLength = extras.getDouble("STEP_LENGTH", 0);
            stepImpact = extras.getDouble("IMPACT", 0);


          /*  Toast.makeText(context.getApplicationContext(), "THREE_MIN_WALK" + "\n" +
                    intent.getStringExtra("ST_TIMESTAMP") + "\n" +
                    intent.getStringExtra("END_TIMESTAMP") + "\n" +
                    intent.getDoubleExtra("DISTANCE_WALKED", 0) + "\n" +
                    intent.getLongExtra("NUMBER_OF_STEPS", 0) + "\n" +
                    intent.getDoubleExtra("STEP_LENGTH", 0) + "\n" +
                    intent.getDoubleExtra("IMPACT", 0), Toast.LENGTH_LONG).show();*/

            //  WifiStateOn(context);

            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                if (!tmwArrListEventBattery.isEmpty()) {
                    //   Toast.makeText(context.getApplicationContext(), "3MW offline data uploading!" + tmwArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_tmw_array);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKTMWArrayTaskNew().execute(urlEncoded);

                }
                // Toast.makeText(context.getApplicationContext(), "3MW Live data uploading!", Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_tmw);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTMWTask().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {
                //Toast.makeText(context.getApplicationContext(), "Save 3MW offline", Toast.LENGTH_SHORT).show();

                tmwArrListEventBattery.add(eventBattery);
                tmwArrListStTimestamp.add(stTimestamp);
                tmwArrListEndTimestamp.add(endTimestamp);
                tmwArrListDistanceWalked.add(distanceWalked);
                tmwArrListNumberOfSteps.add(numberOfSteps);
                tmwArrListStepLength.add(stepLength);
                tmwArrListStepImpact.add(stepImpact);
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("HEART_RATE")) {

            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            heartrate_hr_measurement = extras.getInt("HR_MEASUREMENT", 0);

           /* Toast.makeText(context.getApplicationContext(), "HEART_RATE" + "\n" +
                    intent.getStringExtra("EVENT_BATTERY") + "\n" +
                    intent.getStringExtra("ST_TIMESTAMP") + "\n" +
                    intent.getDoubleExtra("HR_MEASUREMENT", 0), Toast.LENGTH_LONG).show();*/

            // WifiStateOn(context);
            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                if (!hrArrListEventBattery.isEmpty()) {
                    //     Toast.makeText(context.getApplicationContext(), "HR offline data uploading!" + hrArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                    String url_login = context.getResources().getString(R.string.ws_heartrate_array);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKHeartRateArrayTaskNew().execute(urlEncoded);

                }
                // Toast.makeText(context.getApplicationContext(), "HR online data uploading!" + hrArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_heartrate);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKHeartRateTask().execute(urlEncoded);


                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }


            } else {
                // Toast.makeText(context.getApplicationContext(), "Save Heart Rate offline", Toast.LENGTH_SHORT).show();
                hrArrListEventBattery.add(eventBattery);
                hrArrListStTimestamp.add(stTimestamp);
                hrArrListHrMeasurement.add(heartrate_hr_measurement);
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("DOFFED")) {
            extras = intent.getExtras();

            eventBattery = extras.getInt("EVENT_BATTERY", 0);
            stTimestamp = extras.getString("ST_TIMESTAMP");
            doffedtime_pos_total_doffed_time = extras.getLong("POS_TOTAL_DOFFED_TIME", 0);

           /* Toast.makeText(context.getApplicationContext(), "Doffed" + "\n" +
                    intent.getStringExtra("EVENT_BATTERY") + "\n" +
                    intent.getStringExtra("ST_TIMESTAMP") + "\n" +
                    intent.getLongExtra("POS_TOTAL_DOFFED_TIME", 0), Toast.LENGTH_LONG).show();*/

            // WifiStateOn(context);
            //File Save to local
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_" + eventMessage + imei_common + ".txt";
            saveToFile.dumpToFile(strName, eventMessage, intent, imei_common);


            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {

                if (!dfArrListEventBattery.isEmpty()) {
                    //  Toast.makeText(context.getApplicationContext(), "Doffed Time offline data uploading!" + dfArrListEventBattery.size(), Toast.LENGTH_SHORT).show();

                    String url_login = context.getResources().getString(R.string.ws_doffedtime_array);
                    String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                    new BKDoffedArrayTaskNew().execute(urlEncoded);

                }

                // Toast.makeText(context.getApplicationContext(), "Doffed online data uploading!" + dfArrListEventBattery.size(), Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_doffedtime);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKDoffedTask().execute(urlEncoded);

                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);

                if (myFile.exists()) {

                    myFile.getPath();
                    //  Log.e("File_Path:-", "" + myFile.getPath());
                    if (timeRemainUploadSec == fileUploadInterval - 1) {

                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Status:", " " + myFile.getAbsolutePath());
                                    //creating new thread to handle Http Operations
                                    uploadFileServer(myFile.getAbsolutePath());
                                }
                            }).start();

                        }
                    }

                } else {

                }

            } else {
                // Toast.makeText(context.getApplicationContext(), "Save Doffed Time offline", Toast.LENGTH_SHORT).show();

                dfArrListEventBattery.add(eventBattery);
                dfArrListStTimestamp.add(stTimestamp);
                dfArrListTotalDoffedTime.add(doffedtime_pos_total_doffed_time);
            }

        } else if (intent.getStringExtra("MESSAGE_TYPE").equals("UNREGISTER_BROADCAST_RECEIVER")) {

            DataService.unregisterReceiver();

        }

       /* try {
            Thread.sleep(120000);
            WifiStateOff(context);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            WifiStateOff(context);
        }else {

        }*/
    }

    private class BKHeartRateTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* dialog_login.show();
            dialog_login.setCancelable(false);
            dialog_login.setMessage("Data Uploading......");*/

        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("HR_MEASUREMENT", heartrate_hr_measurement);

                    Log.e("URL_PARAMETERS", "" + jsonObj.toString());
                    writer.write(jsonObj.toString());
                    writer.flush();
                    //Log.e("Test", jsonObj.toString());
                } catch (JSONException ex) {
                    Log.e("REQUEST_EXCEPTION", "" + ex);
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                    Log.e("code", "" + code);
                    // String message = new String(jsonObject.getString("message").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        //   WifiStateOff(context);


                        //  tvhint_msg.setText("Data Upload Success......");
                        Log.e("ERROR ", "Data Upload Success");


                    } else if (Integer.parseInt(code) == 1) {
                        // tvhint_msg.setText("Data Upload Failure......");
                        Log.e("ERROR ", "Data Upload failure");


                    } else {
                        //  tvhint_msg.setText("Data Failure......");
                        Log.e("ERROR ", "Data Upload failure");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    private class BKDoffedTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* dialog_login.show();
            dialog_login.setCancelable(false);
            dialog_login.setMessage("Data Uploading......");*/

        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("POS_TOTAL_DOFFED_TIME", doffedtime_pos_total_doffed_time);

                    Log.e("URL_PARAMETERS", "" + jsonObj.toString());
                    writer.write(jsonObj.toString());
                    writer.flush();
                    //Log.e("Test", jsonObj.toString());
                } catch (JSONException ex) {
                    Log.e("REQUEST_EXCEPTION", "" + ex);
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                    Log.e("code", "" + code);
                    // String message = new String(jsonObject.getString("message").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        //  tvhint_msg.setText("Data Upload Success......");
                        Log.e("ERROR ", "Data Upload Success");


                    } else if (Integer.parseInt(code) == 1) {
                        // tvhint_msg.setText("Data Upload Failure......");
                        Log.e("ERROR ", "Data Upload failure");


                    } else {
                        //  tvhint_msg.setText("Data Failure......");
                        Log.e("ERROR ", "Data Upload failure");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    // Start of Three minute walk Task
    private class BKTMWTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("STEP_LENGTH", stepLength);
                    jsonObj.put("IMPACT", stepImpact);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Three minute walk Task

    // Start of Sleep Task
    private class BKSleepingTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("SLP_SOL", solSleep);
                    jsonObj.put("SLP_TIB", tibSleep);
                    jsonObj.put("SLP_TST", tstSleep);
                    jsonObj.put("SLP_WASO", wasoSleep);
                    jsonObj.put("SLP_SE", seSleep);
                    jsonObj.put("SLP_POSITION_CHANGES", postionChangeSleep);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Sleep Task

    // Start of Walk Task
    private class BKWalkingTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arr_flag; i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBattery[i]);
                        jObjd.put("DISTANCE_WALKED", arrDistanceWalked[i]);
                        jObjd.put("NUMBER_OF_STEPS", arrNumberOfSteps[i]);
                        jObjd.put("ST_TIMESTAMP", arrStTimestamp[i]);
                        jObjd.put("END_TIMESTAMP", arrEndTimestamp[i]);
                        jObjd.put("STEP_LENGTH", arrStepLength[i]);
                        jObjd.put("IMPACT", arrStepImpact[i]);
                        jObjd.put("INACTIVITY_SCORE", arrInactivityScore[i]);

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);
                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Walk Task

    // End of Fall Detection Task
    private class BKFallDetectionTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();


                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("FALL_CONFIDENCE", fallConfidence);
                    jsonObj.put("FALL_G_FORCE_LEVEL", fallGForceLevel);
                    jsonObj.put("STEP_LENGTH", stepLength);
                    jsonObj.put("IMPACT", stepImpact);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Fall Detection Task

    // Start  of Wave Detection
    private class BKWaveDetected extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("HR_MEASUREMENT", hrMeasurement);
                    jsonObj.put("ATTENTION_REQUEST", attentionRequest);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }
        }
    }
    // End of Wave Detection Task

    // Start of SOS Task
    private class BKSOSTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {


                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("HR_MEASUREMENT", hrMeasurement);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    // End of SOS Task
    // Start of Tug Task
    private class BKTugTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("IMEI", imei_common);
                    jsonObj.put("EVENT_BATTERY", eventBattery);
                    jsonObj.put("ST_TIMESTAMP", stTimestamp);
                    jsonObj.put("END_TIMESTAMP", endTimestamp);
                    jsonObj.put("DISTANCE_WALKED", distanceWalked);
                    jsonObj.put("NUMBER_OF_STEPS", numberOfSteps);
                    jsonObj.put("INACTIVITY_SCORE", inactivityScore);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    private class BKTransactionTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post

                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arr_flag; i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBattery[i]);
                        jObjd.put("CURRENT_POSTURE", arrCurrentPosture[i]);
                        jObjd.put("TRANSITION_MODE", arrTransitionMode[i]);
                        jObjd.put("ST_TIMESTAMP", arrStTimestamp[i]);
                        jObjd.put("POS_TRANSITION_TIME", arrPosTransitionTime[i]);
                        jObjd.put("POS_TRANSITION_INTENSITY", arrPosTransitionIntensity[i]);

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                    //Log.e("Test", jsonObj.toString());
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            // dialog_login.dismiss();
            // msg.setText(output);
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {

                ///  tvhint_msg.setText("Data Upload Failure......");
                Log.e("ERROR ", "Data Upload failure");


            }

        }
    }

    private void WifiStateOn(Context context) {
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }

    private void WifiStateOff(Context context) {
        WifiManager wifi = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(false);
    }


    private class BKSOSArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {
                    //Toast.makeText(context_new.getApplicationContext(), "ARRAY_LENGTH_1:-" + sosArrEVENT_BATTERY.size(), Toast.LENGTH_SHORT).show();

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < sosArrEVENT_BATTERY.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", sosArrEVENT_BATTERY.get(i));
                        jObjd.put("HR_MEASUREMENT", sosArrHR_MEASUREMENT.get(i));
                        jObjd.put("ST_TIMESTAMP", sosArrST_TIMESTAMP.get(i));

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                //  Toast.makeText(context_new.getApplicationContext(), "SOS_URL_PAR:- Exception1", Toast.LENGTH_SHORT).show();

                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        Log.e("ERROR ", "Data Upload Success");
                        sosArrEVENT_BATTERY.clear();
                        sosArrHR_MEASUREMENT.clear();
                        sosArrST_TIMESTAMP.clear();

                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKTugArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                //Toast.makeText(context_new.getApplicationContext(), "ARRAY_LENGTH:-" + sosArrEVENT_BATTERY.size(), Toast.LENGTH_SHORT).show();


                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tugArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tugArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", tugArrListStTimestamp.get(i));
                        jObjd.put("END_TIMESTAMP", tugArrListEndTimestamp.get(i));
                        jObjd.put("DISTANCE_WALKED", tugArrListDistanceWalked.get(i));
                        jObjd.put("NUMBER_OF_STEPS", tugArrListNumberOfSteps.get(i));
                        jObjd.put("INACTIVITY_SCORE", tugArrListInactivityScore.get(i));

                        jsonArr.put(jObjd);
                    }
                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);
                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        tugArrListInactivityScore.clear();
                        tugArrListStTimestamp.clear();
                        tugArrListNumberOfSteps.clear();
                        tugArrListDistanceWalked.clear();
                        tugArrListEndTimestamp.clear();
                        tugArrListEventBattery.clear();

                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKWaveArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < waveArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", waveArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", waveArrListStTimestamp.get(i));
                        jObjd.put("END_TIMESTAMP", waveArrListEndTimestamp.get(i));
                        jObjd.put("HR_MEASUREMENT", waveArrListHrMeasurement.get(i));
                        jObjd.put("ATTENTION_REQUEST", waveArrListAttentionRequest.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        waveArrListEndTimestamp.clear();
                        waveArrListAttentionRequest.clear();
                        waveArrListHrMeasurement.clear();
                        waveArrListEventBattery.clear();
                        waveArrListStTimestamp.clear();

                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKFallArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < fallArrListEventBattery.size(); i++) {


                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", fallArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", fallArrListStTimestamp.get(i));
                        jObjd.put("END_TIMESTAMP", fallAListEndTimestamp.get(i));
                        jObjd.put("DISTANCE_WALKED", fallArrListDistanceWalked.get(i));
                        jObjd.put("NUMBER_OF_STEPS", fallArrListNumberOfSteps.get(i));
                        jObjd.put("STEP_LENGTH", fallArrListStepLength.get(i));
                        jObjd.put("IMPACT", fallArrListStepImpact.get(i));
                        jObjd.put("FALL_G_FORCE_LEVEL", fallArrListForceLevel.get(i));
                        jObjd.put("FALL_CONFIDENCE", fallArrListFallConfidence.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {
                        fallArrListEventBattery.clear();
                        fallArrListStTimestamp.clear();
                        fallAListEndTimestamp.clear();
                        fallArrListDistanceWalked.clear();
                        fallArrListNumberOfSteps.clear();
                        fallArrListStepLength.clear();
                        fallArrListStepImpact.clear();
                        fallArrListForceLevel.clear();
                        fallArrListFallConfidence.clear();

                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKSleepArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < sleepArrListEventBattery.size(); i++) {


                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", sleepArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", sleepArrListStTimestamp.get(i));
                        jObjd.put("END_TIMESTAMP", sleepArrListEndTimestamp.get(i));
                        jObjd.put("SLP_SOL", sleepArrListSolSLeep.get(i));
                        jObjd.put("SLP_TIB", sleepArrListTibSleep.get(i));
                        jObjd.put("SLP_TST", sleepArrListTstSleep.get(i));
                        jObjd.put("SLP_WASO", sleepArrListWasoSleep.get(i));
                        jObjd.put("SLP_SE", sleepArrListSeSleep.get(i));
                        jObjd.put("SLP_POSITION_CHANGES", sleepArrListPositionChangeSleep.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        sleepArrListEventBattery.clear();
                        sleepArrListStTimestamp.clear();
                        sleepArrListEndTimestamp.clear();
                        sleepArrListSolSLeep.clear();
                        sleepArrListTibSleep.clear();
                        sleepArrListTstSleep.clear();
                        sleepArrListWasoSleep.clear();
                        sleepArrListSeSleep.clear();
                        sleepArrListPositionChangeSleep.clear();

                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKTMWArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tmwArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tmwArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", tmwArrListStTimestamp.get(i));
                        jObjd.put("END_TIMESTAMP", tmwArrListEndTimestamp.get(i));
                        jObjd.put("DISTANCE_WALKED", tmwArrListDistanceWalked.get(i));
                        jObjd.put("NUMBER_OF_STEPS", tmwArrListNumberOfSteps.get(i));
                        jObjd.put("STEP_LENGTH", tmwArrListStepLength.get(i));
                        jObjd.put("IMPACT", tmwArrListStepImpact.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        tmwArrListEventBattery.clear();
                        tmwArrListStepImpact.clear();
                        tmwArrListStepLength.clear();
                        tmwArrListNumberOfSteps.clear();
                        tmwArrListDistanceWalked.clear();
                        tmwArrListEndTimestamp.clear();
                        tmwArrListStTimestamp.clear();

                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKHeartRateArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < hrArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", hrArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", hrArrListStTimestamp.get(i));
                        jObjd.put("HR_MEASUREMENT", hrArrListHrMeasurement.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        hrArrListStTimestamp.clear();
                        hrArrListStTimestamp.clear();
                        hrArrListEventBattery.clear();
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }


    private class BKDoffedArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < dfArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", dfArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", dfArrListStTimestamp.get(i));
                        jObjd.put("POS_TOTAL_DOFFED_TIME", dfArrListTotalDoffedTime.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        dfArrListTotalDoffedTime.clear();
                        dfArrListEventBattery.clear();
                        dfArrListStTimestamp.clear();
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKTransArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tranArrListEventBattery.size(); i++) {


                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tranArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", tranArrListStTimestamp.get(i));
                        jObjd.put("CURRENT_POSTURE", tranArrListCurrentPosture.get(i));
                        jObjd.put("TRANSITION_MODE", tranArrListTransitionMode.get(i));
                        jObjd.put("POS_TRANSITION_TIME", tranArrListPosTransitionTime.get(i));
                        jObjd.put("POS_TRANSITION_INTENSITY", tranArrListPosTransitionIntensity.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        tranArrListPosTransitionTime.clear();
                        tranArrListPosTransitionTime.clear();
                        tranArrListTransitionMode.clear();
                        tranArrListCurrentPosture.clear();
                        tranArrListEventBattery.clear();
                        tranArrListStTimestamp.clear();
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private class BKTWalkArrayTaskNew extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }

        private String getOutputFromUrl(String url) {

            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = getHttpConnection(url);

                String result = "";
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            stream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    stream.close();
                    result = sb.toString();
                    output.append(result);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return output.toString();
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();

            try {
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpConnection.setRequestMethod("POST");
                httpConnection.setDoOutput(true);
                httpConnection.connect();
                //post
                OutputStreamWriter writer = new OutputStreamWriter(httpConnection.getOutputStream());
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < walkArrListEventBattery.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", walkArrListEventBattery.get(i));
                        jObjd.put("ST_TIMESTAMP", walkArrListStTimestamp.get(i));
                        jObjd.put("END_TIMESTAMP", walkArrListEndTimestamp.get(i));
                        jObjd.put("DISTANCE_WALKED", walkArrListDistanceWalked.get(i));
                        jObjd.put("NUMBER_OF_STEPS", walkArrListNumberOfSteps.get(i));
                        jObjd.put("STEP_LENGTH", walkArrListStepLength.get(i));
                        jObjd.put("IMPACT", walkArrListStepImpact.get(i));
                        jObjd.put("INACTIVITY_SCORE", walkArrListInactivityScore.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    writer.write(jsonObj.toString());
                    writer.flush();
                } catch (JSONException ex) {
                    saveToFile.uploadFailed(eventMessage + " JSON Exception" + ex.getMessage());
                }


                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return stream;
        }

        @Override
        protected void onPostExecute(String output) {
            Log.e("JSON_RES_LOGIN", "" + output);
            if (output != null) {

                try {
                    JSONObject jsonObject = new JSONObject(output);
                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                    if (Integer.parseInt(code) == 0) {

                        walkArrListInactivityScore.clear();
                        walkArrListStepImpact.clear();
                        walkArrListStepLength.clear();
                        walkArrListNumberOfSteps.clear();
                        walkArrListDistanceWalked.clear();
                        walkArrListEndTimestamp.clear();
                        walkArrListEventBattery.clear();
                        walkArrListStTimestamp.clear();
                    } else {
                        saveToFile.uploadFailed(eventMessage + "Post Execution ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ERROR ", "Data Upload failure");
            }

        }
    }

    private void StartTimer(final Context context) {

        isReading = true;

        isPaused = false;
        isCanceled = false;

        CountDownTimer timer;
        long millisInFuture = 180000; //180 seconds
        long countDownInterval = 1000; //1 second

        //Initialize a new CountDownTimer instance
        timer = new CountDownTimer(millisInFuture, countDownInterval) {
            public void onTick(long millisUntilFinished) {
                //do something in every tick
                if (isPaused || isCanceled) {
                    //If the user request to cancel or paused the
                    cancel();
                } else {
                    //Display the remaining seconds to app interface
                    long pValue = millisUntilFinished / 1000;
                    timeRemaining = millisUntilFinished;
                    //Log.e("pValue", "" + pValue);

                    if (pValue == 120) {

                        //   Toast.makeText(context.getApplicationContext(), "Wifi Prog on call...", Toast.LENGTH_SHORT).show();


                        // WifiStateOn(context);
                    }

                }
            }

            public void onFinish() {
                //Do something when count down finished
                //  Log.e("DONE", "DONE");
                dataUpload(context);

            }
        }.start();
    }

    private void StartTimerUpload(final Context context) {

        isReadingUpload = true;

        isPausedUpload = false;
        isCanceledUpload = false;

        CountDownTimer timer;
        long millisInFuture = 60000; //60 seconds
        long countDownInterval = 1000; //1 second

        //Initialize a new CountDownTimer instance
        timer = new CountDownTimer(millisInFuture, countDownInterval) {
            public void onTick(long millisUntilFinished) {
                //do something in every tick
                if (isPausedUpload || isCanceledUpload) {
                    //If the user request to cancel or paused the
                    cancel();
                } else {
                    //Display the remaining seconds to app interface
                    long pValue = millisUntilFinished / 1000;
                    timeRemainingUpload = millisUntilFinished;
                    //Log.e("pValue", "" + pValue);

                    timeRemainUploadSec = pValue;

                }
            }

            public void onFinish() {
                //Do something when count down finished
                //  Log.e("DONE", "DONE");
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                }

            }
        }.start();
    }

    private void dataUpload(Context context) {

        // Toast.makeText(context.getApplicationContext(), "Timer data call", Toast.LENGTH_SHORT).show();

        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            //Toast.makeText(context.getApplicationContext(), "Internet is ON ", Toast.LENGTH_SHORT).show();

            if (!sosArrEVENT_BATTERY.isEmpty()) {
                //   Toast.makeText(context.getApplicationContext(), "SOS offline data uploading!", Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_sos_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKSOSArrayTaskNew().execute(urlEncoded);
            }

            if (!waveArrListEventBattery.isEmpty()) {
                // Toast.makeText(context.getApplicationContext(), "WAVE offline data uploading!", Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_wave_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKWaveArrayTaskNew().execute(urlEncoded);

            }

            if (!fallArrListEventBattery.isEmpty()) {
                //   Toast.makeText(context.getApplicationContext(), "Fall Detection offline data uploading!", Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_falldetection_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKFallArrayTaskNew().execute(urlEncoded);

            }

            if (!tranArrListEventBattery.isEmpty()) {
                // Toast.makeText(context.getApplicationContext(), "Transition offline data uploading!" + tranArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_transition);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTransArrayTaskNew().execute(urlEncoded);

            }

            if (!tugArrListEventBattery.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "TUG offline data uploading!" + tugArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_tug_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTugArrayTaskNew().execute(urlEncoded);

            }

            if (!walkArrListEventBattery.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "Walk offline data uploading!" + walkArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_walk);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTWalkArrayTaskNew().execute(urlEncoded);
            }

            if (!tmwArrListEventBattery.isEmpty()) {
                // Toast.makeText(context.getApplicationContext(), "3MW offline data uploading!" + tmwArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_tmw_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKTMWArrayTaskNew().execute(urlEncoded);

            }

            if (!hrArrListEventBattery.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "HR offline data uploading!" + hrArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_heartrate_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKHeartRateArrayTaskNew().execute(urlEncoded);

            }

            if (!dfArrListEventBattery.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "Doffed Time offline data uploading!" + dfArrListEventBattery.size(), Toast.LENGTH_SHORT).show();

                String url_login = context.getResources().getString(R.string.ws_doffedtime_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                new BKDoffedArrayTaskNew().execute(urlEncoded);

            }


        } else {
            //  Toast.makeText(context.getApplicationContext(), "Internet is OFF ", Toast.LENGTH_SHORT).show();
        }
    }

    private void CancelTimer() {
        //When user request to cancel the CountDownTimer
        isCanceled = true;
        isReading = false;

        Log.e("CANCEL", "CANCEL");
        Log.e("Canceled/stopped.", "Canceled/stopped.");


    }

    private void CancelTimerUpload() {
        //When user request to cancel the CountDownTimer
        isCanceledUpload = true;
        isReadingUpload = false;

        Log.e("CANCEL", "CANCEL");
        Log.e("Canceled/stopped.", "Canceled/stopped.");


    }

    public int uploadFileServer(final String selectedFilePath) {

        int serverResponseCode = 0;

        final HttpURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead, bytesAvailable, bufferSize;
        final byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length - 1];

        if (!selectedFile.isFile()) {
            //  progressDialog.dismiss();
            Log.e("NO_File_Sel", "NO_File_Sel");
            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(uploadUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("file", selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0) {
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                //   Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);
                Log.e("STATUS", "2");

                //response code of 200 indicates the server status OK
                if (serverResponseCode == 200) {

                    Log.e("STATUS", "3");
                    InputStream in = null;
                    try {
                        in = connection.getInputStream();
                        // byte[] buffer1 = new byte[1024];
                        int read;
                        while ((read = in.read(buffer)) > 0) {
                            // System.out.println(new String(buffer, 0, read, "utf-8"));
                            String JSONResp = new String(buffer, 0, read, "utf-8");
                            Log.e("RESPONSE_JSON : - ", "" + JSONResp);
                            JSONObject jsonObject = new JSONObject(JSONResp);
                            String code = jsonObject.getString("code");
                            Log.e("CODE_UPLOAD : - ", "" + code);

                        }
                    } catch (UnsupportedEncodingException e) {

                        e.printStackTrace();
                        Log.e("USEE", "UnsupportedEncodingException");

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("IOException", "IOException");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.e("IOException", "IOException1");

                        }
                    }
                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("FileNotFoundException", "FileNotFoundException");

            } catch (MalformedURLException e) {

                e.printStackTrace();
                Log.e("MalformedURLException", "MalformedURLException");

            } catch (IOException e) {

                e.printStackTrace();
                Log.e("IOException", "IOException");
            }
            //progressDialog.dismiss();
            return serverResponseCode;
        }

    }

}
