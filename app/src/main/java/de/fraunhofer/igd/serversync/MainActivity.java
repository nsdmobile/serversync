package de.fraunhofer.igd.serversync;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;


import java.io.IOException;

import java.util.ArrayList;

import background.CommonMethods;
import background.JavaServiceCall;
import background.WatchServiceCall;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MainActivity extends Activity {



    ArrayList<String> permissionsArray = new ArrayList<>();
    int resultCode = 0;
    private final static int ALL_PERMISSIONS_RESULT = 107;
    private SharedPreferences sharedPreferences;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;



    String imei_common;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        permissionsArray.add(READ_PHONE_STATE);
        permissionsArray.add(WRITE_EXTERNAL_STORAGE);
        permissionsArray.add(READ_EXTERNAL_STORAGE);


       /* final Intent sendIntent = new Intent();

        sendIntent.setAction("de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");
        sendIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendIntent.putExtra("CONTENT_STRING","Testing Service");
        context.sendBroadcast(sendIntent);
*/



        AgainAskForPermission();

       imei_common = CommonMethods.getIMEI(context.getApplicationContext());



      //  getRuntimeConfigValues(imei_common);


    }



    public void getRuntimeConfigValues(String imei) {
        try {


         //   Toast.makeText(context,imei_common,Toast.LENGTH_SHORT).show();

            JSONObject rtjsonObj = new JSONObject();
          //  imei_common="362531821710684";

           // imei_common="355857070102185";
            rtjsonObj.put("i_imei", imei);



            new JavaServiceCall(context) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {



                        if (getStatuscode().equals("200")) {
                            String hresp = getResult();

                            Log.e("hresp",hresp);

                           // Toast.makeText(context,imei_common,Toast.LENGTH_SHORT).show();
                            //  con = context_new.createPackageContext("com.sharedpref1", 0);//first app package name is "com.sharedpref1"

                         /*   Intent sendIntent = new Intent();
                           //  sendIntent.setClassName("com.nsd.ncare.beta","com.nsd.ncare.beta.ui.HomeActivity");

                            sendIntent.setClassName("de.fraunhofer.igd.nsddemonstrator","de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");

                          //  ComponentName componentName=new ComponentName("de.fraunhofer.igd.nsddemonstrator","CONFIGURATION_MESSAGE");
                           // ComponentName componentName=new ComponentName("com.nsd.ncare.beta.ui","HomeActivity");
                           // sendIntent.setComponent(componentName);
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra("CONTENT_STRING", hresp);
                            sendIntent.setType("text/plain");
                            context.startActivity(sendIntent);*/


                            final Intent sendIntent = new Intent();
//
                       //    sendIntent.setAction("com.nsd.ncare.beta.network.MyReceive");

                          sendIntent.setAction("de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");
                            sendIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                            sendIntent.putExtra("CONTENT_STRING",hresp);
                            context.sendBroadcast(sendIntent);


                             Toast.makeText(context.getApplicationContext(), "NSD:" + hresp, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {

                        Log.e("Exception","::"+e.getMessage());
                        e.printStackTrace();

                    }


                }


            }.execute(rtjsonObj.toString(), "GetRuntimeConfigValues");

        } catch (Exception ex) {

        }
    }


 


    /*@Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if(demonstratorReceiver!= null)
            unregisterReceiver(demonstratorReceiver);
      //  System.out.println("Service stopped!");
        Log.e("Service stopped!","Service stopped!");
    }*/

    public static void killApp() {
        System.exit(0);
    }


    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private ArrayList<String> findRejectedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private boolean shouldWeAsk(String permission) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    private void markAsAsked(String permission) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    private void clearMarkAsAsked(String permission) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }

    private void makePostRequestSnack() {
        for (String perm : permissionsRejected) {
            clearMarkAsAsked(perm);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void AgainAskForPermission() {

        resultCode = ALL_PERMISSIONS_RESULT;
        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissionsArray);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissionsArray);

        if (permissionsToRequest.size() > 0) {//we need to ask for permissions
            //but have we already asked for them?
            requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
            //mark all these as asked..
            for (String perm : permissionsToRequest) {
                markAsAsked(perm);
            }
        } else {
            //show the success banner
            if (permissionsRejected.size() < permissionsArray.size()) {
                //this means we can show success because some were already accepted.
                // permissionSuccess.setVisibility(View.VISIBLE);
            }

            if (permissionsRejected.size() > 0) {

                Log.e("REJ_PER_1 : ", "" + permissionsRejected.size());
                //we have none to request but some previously rejected..tell the user.
                //It may be better to show a dialog here in a prod application
                for (String perm : permissionsRejected) {

                    clearMarkAsAsked(perm);

                }

            } else {

                try {
                    StartRecording();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


        }
    }

    private void StartRecording() throws IOException {

        imei_common = CommonMethods.getIMEI(getApplicationContext());
        Log.e("IMEI", "" + imei_common);

      //  getRuntimeConfigValues();



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                boolean someAccepted = false;
                boolean someRejected = false;
                for (String perms : permissionsToRequest) {
                    if (hasPermission(perms)) {
                        someAccepted = true;
                    } else {
                        someRejected = true;
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    someRejected = true;
                }

                if (someAccepted) {
                    // permissionSuccess.setVisibility(View.VISIBLE);
                }
                if (someRejected) {
                    makePostRequestSnack();
                }

                if (permissionsRejected.size() > 0) {

                    Log.e("REJ_PER_2 : ", "" + permissionsRejected.size());

                    AgainAskForPermission();

                } else {

                    try {
                        StartRecording();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    /*  @Override
      public void onBackPressed() {
          super.onBackPressed();
      }*/











}
