package de.fraunhofer.igd.serversync;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mhaescher on 27.07.16.
 */

public class SaveToFile {
    Bundle extras;

    int[] arrEventBattery;
    String[] arrStTimestamp;
    String[] arrEndTimestamp;
    int[] arrCurrentPosture;
    int[] arrTransitionMode;
    double[] arrPosTransitionTime;
    double[] arrPosTransitionIntensity;
    double[] arrDistanceWalked;
    long[] arrNumberOfSteps;
    double[] arrStepLength;
    double[] arrStepImpact;
    double[] arrInactivityScore;

    int solSleep;
    int tibSleep;
    int tstSleep;
    int wasoSleep;
    int seSleep;
    int postionChangeSleep;

    int eventBattery, heartrate_hr_measurement;
    String stTimestamp;
    String endTimestamp;
    double distanceWalked;
    long numberOfSteps, doffedtime_pos_total_doffed_time;
    double inactivityScore;
    int hrMeasurement;
    int attentionRequest;
    double stepLength;
    double stepImpact;
    double fallGForceLevel;
    int fallConfidence;
    int arr_flag;

    public void dumpToFile(String FileName, String msg, Intent intent, String imei_common) {
        //File dump

        if (msg.equals("WALK")) {
            try {
                //TRANSITION MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

              /*  File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                 //End

                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "END_TIMESTAMP" + ";" +
                                            "DISTANCE_WALKED" + ";" +
                                            "NUMBER_OF_STEPS" + ";" +
                                            "STEP_LENGTH" + ";" +
                                            "IMPACT" + ";" +
                                            "INACTIVITY_SCORE" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter =
                        new OutputStreamWriter(fOut);

                extras = intent.getExtras();

                arrEventBattery = intent.getIntArrayExtra("EVENT_BATTERY");
                arrStTimestamp = intent.getStringArrayExtra("ST_TIMESTAMP");
                arrEndTimestamp = intent.getStringArrayExtra("END_TIMESTAMP");
                arrDistanceWalked = intent.getDoubleArrayExtra("DISTANCE_WALKED");
                arrNumberOfSteps = intent.getLongArrayExtra("NUMBER_OF_STEPS");
                arrStepLength = intent.getDoubleArrayExtra("STEP_LENGTH");
                arrStepImpact = intent.getDoubleArrayExtra("IMPACT");
                arrInactivityScore = intent.getDoubleArrayExtra("INACTIVITY_SCORE");
                arr_flag = intent.getIntExtra("ARR_FLAG", 0) - 1;


                for (int i = 0; i < (extras.getInt("ARR_FLAG", 0) - 1); i++) {
                    myOutWriter.append(
                            arrEventBattery[i] + ";" +
                                    arrStTimestamp[i] + ";" +
                                    arrEndTimestamp[i] + ";" +
                                    arrDistanceWalked[i] + ";" +
                                    arrNumberOfSteps[i] + ";" +
                                    arrStepLength[i] + ";" +
                                    arrStepImpact[i] + ";" +
                                    arrInactivityScore[i]);
                }
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("TRANSITION")) {
            try {
                //TRANSITION MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

               /* File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "CURRENT_POSTURE" + ";" +
                                            "TRANSITION_MODE" + ";" +
                                            "POS_TRANSITION_TIME" + ";" +
                                            "POS_TRANSITION_INTENSITY" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();
                arrEventBattery = extras.getIntArray("EVENT_BATTERY");
                arrStTimestamp = extras.getStringArray("ST_TIMESTAMP");
                arrCurrentPosture = extras.getIntArray("CURRENT_POSTURE");
                arrTransitionMode = extras.getIntArray("TRANSITION_MODE");
                arrPosTransitionTime = extras.getDoubleArray("POS_TRANSITION_TIME");
                arrPosTransitionIntensity = extras.getDoubleArray("POS_TRANSITION_INTENSITY");

                arr_flag = extras.getInt("ARR_FLAG", 0) - 1;

                for (int i = 0; i < (extras.getInt("ARR_FLAG", 0) - 1); i++) {
                    myOutWriter.append(
                            arrEventBattery[i] + ";" +
                                    arrStTimestamp[i] + ";" +
                                    arrCurrentPosture[i] + ";" +
                                    arrTransitionMode[i] + ";" +
                                    arrPosTransitionTime[i] + ";" +
                                    arrPosTransitionIntensity[i] + "\n");
                }
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("SLEEP")) {
            try {
                //SLEEPING MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

              /*  File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "END_TIMESTAMP" + ";" +
                                            "SLP_SOL" + ";" +
                                            "SLP_TIB" + ";" +
                                            "SLP_TST" + ";" +
                                            "SLP_WASO" + ";" +
                                            "SLP_SE" + ";" +
                                            "SLP_POSITION_CHANGES" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();

                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                endTimestamp = extras.getString("END_TIMESTAMP");
                solSleep = extras.getInt("SLP_SOL", 0);
                tibSleep = extras.getInt("SLP_TIB", 0);
                tstSleep = extras.getInt("SLP_TST", 0);
                wasoSleep = extras.getInt("SLP_WASO", 0);
                seSleep = extras.getInt("SLP_SE", 0);
                postionChangeSleep = extras.getInt("SLP_POSITION_CHANGES", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        endTimestamp + ";" +
                                        solSleep + ";" +
                                        tibSleep + ";" +
                                        tstSleep + ";" +
                                        wasoSleep + ";" +
                                        seSleep + ";" +
                                        postionChangeSleep + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("TUG")) {
            try {
                //TUG MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

             /*   File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "END_TIMESTAMP" + ";" +
                                            "DISTANCE_WALKED" + ";" +
                                            "NUMBER_OF_STEPS" + ";" +
                                            "INACTIVITY_SCORE" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter =
                        new OutputStreamWriter(fOut);

                extras = intent.getExtras();
                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                endTimestamp = extras.getString("END_TIMESTAMP");
                distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
                numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
                inactivityScore = extras.getDouble("INACTIVITY_SCORE", 0);


                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        endTimestamp + ";" +
                                        distanceWalked + ";" +
                                        numberOfSteps + ";" +
                                        inactivityScore + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("FALL_DETECTION")) {
            try {
                //FALL_DETECTION MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

              /*  File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "END_TIMESTAMP" + ";" +
                                            "DISTANCE_WALKED" + ";" +
                                            "NUMBER_OF_STEPS" + ";" +
                                            "STEP_LENGTH" + ";" +
                                            "IMPACT" + ";" +
                                            "FALL_G_FORCE_LEVEL" + ";" +
                                            "FALL_CONFIDENCE" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();
                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                endTimestamp = extras.getString("END_TIMESTAMP");
                distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
                numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
                stepLength = extras.getDouble("STEP_LENGTH", 0);
                stepImpact = extras.getDouble("IMPACT", 0);
                fallGForceLevel = extras.getDouble("FALL_G_FORCE_LEVEL", 0);
                fallConfidence = extras.getInt("FALL_CONFIDENCE", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        endTimestamp + ";" +
                                        distanceWalked + ";" +
                                        numberOfSteps + ";" +
                                        stepLength + ";" +
                                        stepImpact + ";" +
                                        fallGForceLevel + ";" +
                                        fallConfidence + ";" + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("SOS")) {
            try {
                //SOS MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

              /*  File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
               // myFile.setExecutable(true);
               // myFile.setReadable(true);
               // myFile.setWritable(true);

                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "HR_MEASUREMENT" + ";" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();
                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                hrMeasurement = extras.getInt("HR_MEASUREMENT", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        hrMeasurement + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("WAVE")) {
            try {
                //SOS MSG

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

              /*  File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +

                                            "END_TIMESTAMP" + ";" +
                                            "HR_MEASUREMENT" + ";" +
                                            "ATTENTION_REQUEST" + ";" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();

                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                endTimestamp = extras.getString("END_TIMESTAMP");
                hrMeasurement = extras.getInt("HR_MEASUREMENT", 0);
                attentionRequest = extras.getInt("ATTENTION_REQUEST", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        endTimestamp + ";" +
                                        hrMeasurement + ";" +
                                        attentionRequest + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("THREE_MIN_WALK")) {
            try {

                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";
/*

                File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/
                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +

                                            "END_TIMESTAMP" + ";" +
                                            "DISTANCE_WALKED" + ";" +
                                            "NUMBER_OF_STEPS" + ";" +
                                            "STEP_LENGTH" + ";" +
                                            "IMPACT" + ";" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();

                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                endTimestamp = extras.getString("END_TIMESTAMP");
                distanceWalked = extras.getDouble("DISTANCE_WALKED", 0);
                numberOfSteps = extras.getLong("NUMBER_OF_STEPS", 0);
                stepLength = extras.getDouble("STEP_LENGTH", 0);
                stepImpact = extras.getDouble("IMPACT", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        endTimestamp + ";" +
                                        hrMeasurement + ";" +
                                        distanceWalked + ";" +
                                        numberOfSteps + ";" +
                                        stepLength + ";" +
                                        stepImpact + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("DOFFED")) {
            try {
                //SOS MSG
                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

              /*  File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/
                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "POS_TOTAL_DOFFED_TIME" + ";" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();

                eventBattery = intent.getIntExtra("EVENT_BATTERY", 0);
                stTimestamp = intent.getStringExtra("ST_TIMESTAMP");
                doffedtime_pos_total_doffed_time = intent.getLongExtra("POS_TOTAL_DOFFED_TIME", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        doffedtime_pos_total_doffed_time + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (msg.equals("HEART_RATE")) {
            try {
                //SOS MSG
                String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
                String strName = timeStamp + "_" + FileName + imei_common + ".txt";

               /* File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
                fileDirectory.mkdirs();
                File myFile = new File(fileDirectory, strName);
*/

                //Start
                String root = Environment.getExternalStorageDirectory().toString();
                File fileDirectory = new File(root + "/NSD_File");

                if (!fileDirectory.exists()) {

                    fileDirectory.mkdirs();
                }
                final File myFile = new File(fileDirectory, strName);
                //End
                if (!myFile.exists()) {
                    myFile.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(myFile, true);
                    OutputStreamWriter myOutWriter =
                            new OutputStreamWriter(fOut);
                    myOutWriter.append
                            (
                                    "EVENT_BATTERY" + ";" +
                                            "ST_TIMESTAMP" + ";" +
                                            "HR_MEASUREMENT" + ";" + "\n"
                            );
                    myOutWriter.flush();
                    myOutWriter.close();
                    fOut.close();
                }
                FileOutputStream fOut = new FileOutputStream(myFile, true);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

                extras = intent.getExtras();

                eventBattery = extras.getInt("EVENT_BATTERY", 0);
                stTimestamp = extras.getString("ST_TIMESTAMP");
                heartrate_hr_measurement = extras.getInt("HR_MEASUREMENT", 0);

                myOutWriter.append
                        (
                                eventBattery + ";" +
                                        stTimestamp + ";" +
                                        doffedtime_pos_total_doffed_time + "\n"
                        );
                myOutWriter.flush();
                myOutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void uploadFailed(String str) {
        try {
            String timeStamp = new SimpleDateFormat("dd_MM_yyyy").format(Calendar.getInstance().getTime());
            String strName = timeStamp + "_error.log";

            File fileDirectory = new File("/sdcard/de.fraunhofer.igd.nsddemonstrator/files/");
            fileDirectory.mkdirs();
            File myFile = new File(fileDirectory, strName);

            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(myFile, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

            myOutWriter.append
                    (
                            str + " data failed to upload to the server at " + timeStamp + "\n"
                    );
            myOutWriter.flush();
            myOutWriter.close();
            fOut.close();
        } catch (Exception e) {
        }
    }
}

