package de.fraunhofer.igd.serversync;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by apple on 17/08/16.
 */
public class AppController extends Application {
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
