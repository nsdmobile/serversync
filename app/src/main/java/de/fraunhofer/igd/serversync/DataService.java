package de.fraunhofer.igd.serversync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import background.BatteryLevelReceiver;

import static de.fraunhofer.igd.serversync.MainActivity.killApp;

public class DataService extends Service {
    public static IntentFilter intentFilter = new IntentFilter("de.fraunhofer.igd.serversync.SEND_DATA_TO_SERVER");
    public static DemonstratorDataReceiver demonstratorReceiver = new DemonstratorDataReceiver();
    public static Context context;


  //  public static IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
   // public static BatteryLevelReceiver mBatInfoReceiver = new BatteryLevelReceiver();



    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        // Receive broadcast from an external app
        if(intentFilter!=null)
        {
            registerReceiver(demonstratorReceiver,intentFilter);
        }

     /*   if(ifilter!=null) {
            registerReceiver(mBatInfoReceiver, ifilter);
        }*/
      //  System.out.println("Service started!");
        Log.e("Service started!","Service started!");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Receive broadcast from an external app
        if(intentFilter!=null)
        {
            registerReceiver(demonstratorReceiver,intentFilter);
        }
       /* if(ifilter!=null) {
            registerReceiver(mBatInfoReceiver, ifilter);
        }*/

      //  System.out.println("Service started!");
        Log.e("Service started!","Service started!");

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if(demonstratorReceiver!= null)
            unregisterReceiver(demonstratorReceiver);

      /*  if(mBatInfoReceiver!= null)
            context.unregisterReceiver(mBatInfoReceiver);*/

        //System.out.println("Service stopped!");
        Log.e("Service stopped!","Service stopped!");

    }

    public static void unregisterReceiver(){
        if(demonstratorReceiver!= null)
            context.unregisterReceiver(demonstratorReceiver);
      //  System.out.println("Service stopped!");
        Log.e("Service stopped!","Service stopped!");
        Log.e("Broadcasting stopped!","Broadcasting stopped!");

        //Toast.makeText(context.getApplicationContext(), "Broadcasting stopped!", Toast.LENGTH_LONG).show();
        killApp();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
