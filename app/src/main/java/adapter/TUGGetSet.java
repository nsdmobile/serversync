package adapter;

/**
 * Created by BKC_A on 11/14/16.
 */


public class TUGGetSet {

    // private variables
    int id;
    public String event_battery;
    public String st_timestamp;
    public String end_timestamp;
    public String distance_walked;
    public String num_of_steps;
    public String inactivity_score;

    public TUGGetSet() {
    }

    public TUGGetSet(int id, String event_battery, String st_timestamp, String end_timestamp, String distance_walked, String num_of_steps, String inactivity_score) {
        this.id = id;
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.distance_walked = distance_walked;
        this.num_of_steps = num_of_steps;
        this.inactivity_score = inactivity_score;
    }

    public TUGGetSet(String event_battery, String st_timestamp, String end_timestamp, String distance_walked, String num_of_steps, String inactivity_score) {
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.distance_walked = distance_walked;
        this.num_of_steps = num_of_steps;
        this.inactivity_score = inactivity_score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_battery() {
        return event_battery;
    }

    public void setEvent_battery(String event_battery) {
        this.event_battery = event_battery;
    }

    public String getSt_timestamp() {
        return st_timestamp;
    }

    public void setSt_timestamp(String st_timestamp) {
        this.st_timestamp = st_timestamp;
    }

    public String getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(String end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public String getDistance_walked() {
        return distance_walked;
    }

    public void setDistance_walked(String distance_walked) {
        this.distance_walked = distance_walked;
    }

    public String getNum_of_steps() {
        return num_of_steps;
    }

    public void setNum_of_steps(String num_of_steps) {
        this.num_of_steps = num_of_steps;
    }

    public String getInactivity_score() {
        return inactivity_score;
    }

    public void setInactivity_score(String inactivity_score) {
        this.inactivity_score = inactivity_score;
    }
}
