package adapter;

/**
 * Created by BKC_A on 11/14/16.
 */


public class TransactionGetSet {

    // private variables
    int id;
    public String event_battery;
    public String st_timestamp;
    public String current_posture;
    public String transaction_mode;
    public String pos_transaction_time;
    public String pos_transaction_intansity;

    public TransactionGetSet() {
    }

    public TransactionGetSet(int id, String event_battery, String st_timestamp, String current_posture, String transaction_mode, String pos_transaction_time, String pos_transaction_intansity) {
        this.id = id;
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.current_posture = current_posture;
        this.transaction_mode = transaction_mode;
        this.pos_transaction_time = pos_transaction_time;
        this.pos_transaction_intansity = pos_transaction_intansity;
    }

    public TransactionGetSet(String event_battery, String st_timestamp, String current_posture, String transaction_mode, String pos_transaction_time, String pos_transaction_intansity) {
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.current_posture = current_posture;
        this.transaction_mode = transaction_mode;
        this.pos_transaction_time = pos_transaction_time;
        this.pos_transaction_intansity = pos_transaction_intansity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_battery() {
        return event_battery;
    }

    public void setEvent_battery(String event_battery) {
        this.event_battery = event_battery;
    }

    public String getSt_timestamp() {
        return st_timestamp;
    }

    public void setSt_timestamp(String st_timestamp) {
        this.st_timestamp = st_timestamp;
    }

    public String getCurrent_posture() {
        return current_posture;
    }

    public void setCurrent_posture(String current_posture) {
        this.current_posture = current_posture;
    }

    public String getTransaction_mode() {
        return transaction_mode;
    }

    public void setTransaction_mode(String transaction_mode) {
        this.transaction_mode = transaction_mode;
    }

    public String getPos_transaction_time() {
        return pos_transaction_time;
    }

    public void setPos_transaction_time(String pos_transaction_time) {
        this.pos_transaction_time = pos_transaction_time;
    }

    public String getPos_transaction_intansity() {
        return pos_transaction_intansity;
    }

    public void setPos_transaction_intansity(String pos_transaction_intansity) {
        this.pos_transaction_intansity = pos_transaction_intansity;
    }
}
