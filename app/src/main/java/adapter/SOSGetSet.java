package adapter;

/**
 * Created by BKC_A on 11/14/16.
 */


public class SOSGetSet {

    // private variables
    int id;
    public String event_battery;
    public String st_timestamp;
    public String hr_measurement;

    public SOSGetSet() {
    }

    public SOSGetSet(int id, String event_battery, String st_timestamp, String hr_measurement) {
        this.id = id;
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.hr_measurement = hr_measurement;
    }

    public SOSGetSet(String event_battery, String st_timestamp, String hr_measurement) {
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.hr_measurement = hr_measurement;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_battery() {
        return event_battery;
    }

    public void setEvent_battery(String event_battery) {
        this.event_battery = event_battery;
    }

    public String getSt_timestamp() {
        return st_timestamp;
    }

    public void setSt_timestamp(String st_timestamp) {
        this.st_timestamp = st_timestamp;
    }

    public String getHr_measurement() {
        return hr_measurement;
    }

    public void setHr_measurement(String hr_measurement) {
        this.hr_measurement = hr_measurement;
    }
}
