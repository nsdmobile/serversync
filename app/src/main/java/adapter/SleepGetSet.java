package adapter;

/**
 * Created by BKC_A on 11/14/16.
 */


public class SleepGetSet {

    // private variables
    int id;
    public String event_battery;
    public String st_timestamp;
    public String end_timestamp;
    public String slp_sol;
    public String slp_tib;
    public String slp_tst;
    public String slp_waso;
    public String slp_se;
    public String slp_position_changes;

    public SleepGetSet() {
    }

    public SleepGetSet(int id, String event_battery, String st_timestamp, String end_timestamp, String slp_sol, String slp_tib, String slp_tst, String slp_waso, String slp_se, String slp_position_changes) {
        this.id = id;
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.slp_sol = slp_sol;
        this.slp_tib = slp_tib;
        this.slp_tst = slp_tst;
        this.slp_waso = slp_waso;
        this.slp_se = slp_se;
        this.slp_position_changes = slp_position_changes;
    }

    public SleepGetSet(String event_battery, String st_timestamp, String end_timestamp, String slp_sol, String slp_tib, String slp_tst, String slp_waso, String slp_se, String slp_position_changes) {
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.slp_sol = slp_sol;
        this.slp_tib = slp_tib;
        this.slp_tst = slp_tst;
        this.slp_waso = slp_waso;
        this.slp_se = slp_se;
        this.slp_position_changes = slp_position_changes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_battery() {
        return event_battery;
    }

    public void setEvent_battery(String event_battery) {
        this.event_battery = event_battery;
    }

    public String getSt_timestamp() {
        return st_timestamp;
    }

    public void setSt_timestamp(String st_timestamp) {
        this.st_timestamp = st_timestamp;
    }

    public String getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(String end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public String getSlp_sol() {
        return slp_sol;
    }

    public void setSlp_sol(String slp_sol) {
        this.slp_sol = slp_sol;
    }

    public String getSlp_tib() {
        return slp_tib;
    }

    public void setSlp_tib(String slp_tib) {
        this.slp_tib = slp_tib;
    }

    public String getSlp_tst() {
        return slp_tst;
    }

    public void setSlp_tst(String slp_tst) {
        this.slp_tst = slp_tst;
    }

    public String getSlp_waso() {
        return slp_waso;
    }

    public void setSlp_waso(String slp_waso) {
        this.slp_waso = slp_waso;
    }

    public String getSlp_se() {
        return slp_se;
    }

    public void setSlp_se(String slp_se) {
        this.slp_se = slp_se;
    }

    public String getSlp_position_changes() {
        return slp_position_changes;
    }

    public void setSlp_position_changes(String slp_position_changes) {
        this.slp_position_changes = slp_position_changes;
    }
}
