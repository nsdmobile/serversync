package adapter;

/**
 * Created by BKC_A on 12/22/16.
 */

public class DoffedGetSet {

    int id;
    public String event_battery;
    public String st_timestamp;
    public String end_timestamp;
    public String doffed_time;

    public DoffedGetSet() {
    }

    public DoffedGetSet(int id, String event_battery, String st_timestamp, String end_timestamp, String doffed_time) {
        this.id = id;
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.doffed_time = doffed_time;
    }

    public DoffedGetSet(String event_battery, String st_timestamp, String end_timestamp, String doffed_time) {
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.doffed_time = doffed_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_battery() {
        return event_battery;
    }

    public void setEvent_battery(String event_battery) {
        this.event_battery = event_battery;
    }

    public String getSt_timestamp() {
        return st_timestamp;
    }

    public void setSt_timestamp(String st_timestamp) {
        this.st_timestamp = st_timestamp;
    }

    public String getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(String end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public String getDoffed_time() {
        return doffed_time;
    }

    public void setDoffed_time(String doffed_time) {
        this.doffed_time = doffed_time;
    }
}
