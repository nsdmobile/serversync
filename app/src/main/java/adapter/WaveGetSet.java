package adapter;

/**
 * Created by BKC_A on 11/14/16.
 */


public class WaveGetSet {

    // private variables
    int id;
    public String event_battery;
    public String st_timestamp;
    public String end_timestamp;
    public String hr_measurement;
    public String attention_request;

    public WaveGetSet() {
    }

    public WaveGetSet(int id, String event_battery, String st_timestamp, String end_timestamp, String hr_measurement, String attention_request) {
        this.id = id;
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.hr_measurement = hr_measurement;
        this.attention_request = attention_request;
    }

    public WaveGetSet(String event_battery, String st_timestamp, String end_timestamp, String hr_measurement, String attention_request) {
        this.event_battery = event_battery;
        this.st_timestamp = st_timestamp;
        this.end_timestamp = end_timestamp;
        this.hr_measurement = hr_measurement;
        this.attention_request = attention_request;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_battery() {
        return event_battery;
    }

    public void setEvent_battery(String event_battery) {
        this.event_battery = event_battery;
    }

    public String getSt_timestamp() {
        return st_timestamp;
    }

    public void setSt_timestamp(String st_timestamp) {
        this.st_timestamp = st_timestamp;
    }

    public String getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(String end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public String getHr_measurement() {
        return hr_measurement;
    }

    public void setHr_measurement(String hr_measurement) {
        this.hr_measurement = hr_measurement;
    }

    public String getAttention_request() {
        return attention_request;
    }

    public void setAttention_request(String attention_request) {
        this.attention_request = attention_request;
    }
}
