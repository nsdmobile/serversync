package background;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import adapter.DoffedGetSet;
import adapter.FalldetectionGetSet;
import adapter.SOSGetSet;
import adapter.SleepGetSet;
import adapter.TUGGetSet;
import adapter.ThreeMWalkGetSet;
import adapter.TransactionGetSet;
import adapter.WalkGetSet;
import adapter.WaveGetSet;

/**
 * Created by BKC_A on 11/14/16.
 */

public class DataBaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "nsdserversyncdb";

    // Walk table name
    private static final String TABLE_WALK = "nsdserversyncwalk";
    // Walk Table Columns names
    private static final String KEY_ID = "idwalk";
    private static final String KEY_EVENT_BATTERY = "keyeventbattery";
    private static final String KEY_ST_TIMESTAMP = "keysttimestamp";
    private static final String KEY_END_TIMESTAMP = "keyendstamp";
    private static final String KEY_DISTANCE_WALKED = "keydistancewalked";
    private static final String KEY_NO_STEPS = "keynoofsteps";
    private static final String KEY_STEP_LENGTH = "keysteplength";
    private static final String KEY_IMPACT = "keyimpact";
    private static final String KEY_INACTIVITY_SCORE = "keyinactivityscore";

    // SOS table name
    private static final String TABLE_SOS = "nsdserversyncsos";
    // SOS Table Columns names
    private static final String KEY_ID_SOS = "idsos";
    private static final String KEY_EVENT_BATTERY_SOS = "keyeventbatterysos";
    private static final String KEY_ST_TIMESTAMP_SOS = "keysttimestampsos";
    private static final String KEY_HR_MEASUREMENT = "keyhrmeasurementsos";

    // Transaction table name
    private static final String TABLE_TRANSACTION = "nsdserversynctransaction";
    // Transaction Table Columns names
    private static final String KEY_ID_TRANS = "idtrans";
    private static final String KEY_EVENT_BATTERY_TRANS = "keyeventbatterytrans";
    private static final String KEY_ST_TIMESTAMP_TRANS = "keysttimestamptrans";
    private static final String KEY_CURRENT_POSTURE_TRANS = "keycurrentpostrans";
    private static final String KEY_TRANS_MODE_TRANS = "keymodetrans";
    private static final String KEY_POS_TRANS_TIME_TRANS = "keypostimetrans";
    private static final String KEY_POS_TRANS_INTANCITY_TRANS = "keyposintancitytrans";

    // Tug table name
    private static final String TABLE_TUG = "nsdserversynctug";
    // Tug Table Columns names
    private static final String KEY_ID_TUG = "idtug";
    private static final String KEY_EVENT_BATTERY_TUG = "keyeventbatterytug";
    private static final String KEY_ST_TIMESTAMP_TUG = "keysttimestamptug";
    private static final String KEY_END_TIMESTAMP_TUG = "keyendtimestamptug";
    private static final String KEY_DISTANCE_WALKED_TUG = "keydistancewalkedtug";
    private static final String KEY_NUM_OF_STEPS_TUG = "keynumofstepstug";
    private static final String KEY_INACTIVITY_SCORE_TUG = "keyinactivityscoretug";

    // Sleep table name
    private static final String TABLE_SLEEP = "nsdserversyncsleep";
    // Sleep Table Columns names
    private static final String KEY_ID_SLEEP = "idtug";
    private static final String KEY_EVENT_BATTERY_SLEEP = "keyeventbatterysleep";
    private static final String KEY_ST_TIMESTAMP_SLEEP = "keysttimestampsleep";
    private static final String KEY_END_TIMESTAMP_SLEEP = "keyendtimestampsleep";
    private static final String KEY_SOL_SLEEP = "keysolsleep";
    private static final String KEY_TIB_SLEEP = "keytibsleep";
    private static final String KEY_TST_SLEEP = "keytstsleep";
    private static final String KEY_WASO_SLEEP = "keywasosleep";
    private static final String KEY_SE_SLEEP = "keysesleep";
    private static final String KEY_POSITION_CHANGES = "keyslppositionchanges";

    // Wave table name
    private static final String TABLE_WAVE = "nsdserversyncwave";
    // Wave Table Columns names
    private static final String KEY_ID_WAVE = "idwave";
    private static final String KEY_EVENT_BATTERY_WAVE = "keyeventbatterywave";
    private static final String KEY_ST_TIMESTAMP_WAVE = "keysttimestampwave";
    private static final String KEY_END_TIMESTAMP_WAVE = "keyendtimestampwave";
    private static final String KEY_HR_MEASUREMENT_WAVE = "keyhrmeasurementwave";
    private static final String KEY_ATTENTION_REQUEST_WAVE = "keyattentionrequestwave";

    // FallDetection table name
    private static final String TABLE_FALLDETECTION = "nsdserversyncfalldetection";
    // FallDetection Table Columns names
    private static final String KEY_ID_FALLDETECTION = "idwave";
    private static final String KEY_EVENT_BATTERY_FALLDETECTION = "keyeventbatteryfalldetection";
    private static final String KEY_ST_TIMESTAMP_FALLDETECTION = "keysttimestampfalldetection";
    private static final String KEY_END_TIMESTAMP_FALLDETECTION = "keyendtimestampfalldetection";
    private static final String KEY_DISTANCE_WALKED_FALLDETECTION = "keydistancewalkedfalldetection";
    private static final String KEY_NUM_OF_STEPS_FALLDETECTION = "keynumstepsfalldetection";
    private static final String KEY_STEP_LENGTH_FALLDETECTION = "keyststeplengthfalldetection";
    private static final String KEY_STEP_IMPACT_FALLDETECTION = "keystepimpactfalldetection";
    private static final String KEY_GFORCE_FALLDETECTION = "keygforcefalldetection";
    private static final String KEY_CONFIDENCE_FALLDETECTION = "keyconfidencefalldetection";

    // 3MWalk table name
    private static final String TABLE_TMWALK = "nsdserversynctmwalk";
    // 3MWalk Table Columns names
    private static final String KEY_ID_TMWALK = "idtmwalk";
    private static final String KEY_EVENT_BATTERY_TMWALK = "keyeventbatterytmwalk";
    private static final String KEY_ST_TIMESTAMP_TMWALK = "keysttimestamptmwalk";
    private static final String KEY_END_TIMESTAMP_TMWALK = "keyendstamptmwalk";
    private static final String KEY_DISTANCE_WALKED_TMWALK = "keydistancewalkedtmwalk";
    private static final String KEY_NO_STEPS_TMWALK = "keynoofstepstmwalk";
    private static final String KEY_STEP_LENGTH_TMWALK = "keysteplengthtmwalk";
    private static final String KEY_IMPACT_TMWALK = "keyimpacttmwalk";

    // Doffed table name
    private static final String TABLE_DOFFED = "nsdserversyncdoffed";
    // Doffed Table Columns names
    private static final String KEY_ID_DOFFED = "iddoffed";
    private static final String KEY_EVENT_BATTERY_DOFFED = "keyeventbatterydoffed";
    private static final String KEY_ST_TIMESTAMP_DOFFED = "keysttimestampdoffed";
    private static final String KEY_END_TIMESTAMP_DOFFED = "keyendstampdoffed";
    private static final String KEY_TIME_DOFFED = "keytimedoffed";




    // Table for battery status update

    // battery table name
    private static final String TABLE_BATTERY = "nsdserversyncbattery";
    // battery Table Columns names
    private static final String KEY_ID_BATTERYIMEI = "idbatteryimei";
    private static final String KEY_BATTERY_STATUS = "keybatterystatus";
    private static final String KEY_BATTERY_LASTUPDATE = "keybatterylastupdate";



    // 09-Feb-2018 added heart rate table

    // HEARTRATE table name
    private static final String TABLE_HEARTRATE = "nsdserversyncheartrate";
    // HEARTRATE Table Columns names
    private static final String KEY_ID_HEARTRATE = "idheartrate";
    private static final String KEY_EVENT_BATTERY_HEARTRATE = "keyeventbatteryheartrate";
    private static final String KEY_ST_TIMESTAMP_HEARTRATE = "keysttimestampheartrate";
    private static final String KEY_HR_MEASUREMENT_HEARTRATE = "keyhrmeasurementheartrate";



    // 16-Feb-2018 added respiration rate table

    // RESPIRATIONRATE table name
    private static final String TABLE_RESPIRATIONRATE = "nsdserversyncrespirationrate";
    // HEARTRATE Table Columns names
    private static final String KEY_ID_RESPIRATIONRATE= "idresprate";
    private static final String KEY_EVENT_BATTERY_RESPIRATIONRATE = "keyeventbatteryrespirationrate";
    private static final String KEY_ST_TIMESTAMP_RESPIRATIONRATE = "keysttimestamprespirationrate";
    private static final String KEY_RR_MEASUREMENT_RESPIRATIONRATE = "keyrrmeasurementrespirationrate";






    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_WALK_TABLE = "CREATE TABLE " + TABLE_WALK + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY + " TEXT,"
                + KEY_ST_TIMESTAMP + " TEXT,"
                + KEY_END_TIMESTAMP + " TEXT,"
                + KEY_DISTANCE_WALKED + " TEXT,"
                + KEY_NO_STEPS + " TEXT,"
                + KEY_STEP_LENGTH + " TEXT,"
                + KEY_IMPACT + " TEXT,"
                + KEY_INACTIVITY_SCORE + " TEXT" + ")";

        String CREATE_SOS_TABLE = "CREATE TABLE " + TABLE_SOS + "("
                + KEY_ID_SOS + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_SOS + " TEXT,"
                + KEY_ST_TIMESTAMP_SOS + " TEXT,"
                + KEY_HR_MEASUREMENT + " TEXT" + ")";

        String CREATE_TRANSACTION_TABLE = "CREATE TABLE " + TABLE_TRANSACTION + "("
                + KEY_ID_TRANS + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_TRANS + " TEXT,"
                + KEY_ST_TIMESTAMP_TRANS + " TEXT,"
                + KEY_CURRENT_POSTURE_TRANS + " TEXT,"
                + KEY_TRANS_MODE_TRANS + " TEXT,"
                + KEY_POS_TRANS_TIME_TRANS + " TEXT,"
                + KEY_POS_TRANS_INTANCITY_TRANS + " TEXT" + ")";


        String CREATE_TUG_TABLE = "CREATE TABLE " + TABLE_TUG + "("
                + KEY_ID_TUG + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_TUG + " TEXT,"
                + KEY_ST_TIMESTAMP_TUG + " TEXT,"
                + KEY_END_TIMESTAMP_TUG + " TEXT,"
                + KEY_DISTANCE_WALKED_TUG + " TEXT,"
                + KEY_NUM_OF_STEPS_TUG + " TEXT,"
                + KEY_INACTIVITY_SCORE_TUG + " TEXT" + ")";

        String CREATE_SLEEP_TABLE = "CREATE TABLE " + TABLE_SLEEP + "("
                + KEY_ID_SLEEP + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_SLEEP + " TEXT,"
                + KEY_ST_TIMESTAMP_SLEEP + " TEXT,"
                + KEY_END_TIMESTAMP_SLEEP + " TEXT,"
                + KEY_SOL_SLEEP + " TEXT,"
                + KEY_TIB_SLEEP + " TEXT,"
                + KEY_TST_SLEEP + " TEXT,"
                + KEY_WASO_SLEEP + " TEXT,"
                + KEY_SE_SLEEP + " TEXT,"
                + KEY_POSITION_CHANGES + " TEXT" + ")";

        String CREATE_WAVE_TABLE = "CREATE TABLE " + TABLE_WAVE + "("
                + KEY_ID_WAVE + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_WAVE + " TEXT,"
                + KEY_ST_TIMESTAMP_WAVE + " TEXT,"
                + KEY_END_TIMESTAMP_WAVE + " TEXT,"
                + KEY_HR_MEASUREMENT_WAVE + " TEXT,"
                + KEY_ATTENTION_REQUEST_WAVE + " TEXT" + ")";

        String CREATE_FALLDETECTION_TABLE = "CREATE TABLE " + TABLE_FALLDETECTION + "("
                + KEY_ID_FALLDETECTION + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_FALLDETECTION + " TEXT,"
                + KEY_ST_TIMESTAMP_FALLDETECTION + " TEXT,"
                + KEY_END_TIMESTAMP_FALLDETECTION + " TEXT,"
                + KEY_DISTANCE_WALKED_FALLDETECTION + " TEXT,"
                + KEY_NUM_OF_STEPS_FALLDETECTION + " TEXT,"
                + KEY_STEP_LENGTH_FALLDETECTION + " TEXT,"
                + KEY_STEP_IMPACT_FALLDETECTION + " TEXT,"
                + KEY_GFORCE_FALLDETECTION + " TEXT,"
                + KEY_CONFIDENCE_FALLDETECTION + " TEXT" + ")";

        String CREATE_TMWALK_TABLE = "CREATE TABLE " + TABLE_TMWALK + "("
                + KEY_ID_TMWALK + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_TMWALK + " TEXT,"
                + KEY_ST_TIMESTAMP_TMWALK + " TEXT,"
                + KEY_END_TIMESTAMP_TMWALK + " TEXT,"
                + KEY_DISTANCE_WALKED_TMWALK + " TEXT,"
                + KEY_NO_STEPS_TMWALK + " TEXT,"
                + KEY_STEP_LENGTH_TMWALK + " TEXT,"
                + KEY_IMPACT_TMWALK + " TEXT" + ")";

        String CREATE_TMWALK_DOFFED = "CREATE TABLE " + TABLE_DOFFED + "("
                + KEY_ID_DOFFED + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_DOFFED + " TEXT,"
                + KEY_ST_TIMESTAMP_DOFFED + " TEXT,"
                + KEY_END_TIMESTAMP_DOFFED + " TEXT,"
                + KEY_TIME_DOFFED + " TEXT" + ")";



        // CREATE HEARTRATE table name


        String CREATE_TABLE_HEARTRATE = "CREATE TABLE " + TABLE_HEARTRATE + "("
                + KEY_ID_HEARTRATE + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_HEARTRATE + " TEXT,"
                + KEY_ST_TIMESTAMP_HEARTRATE + " TEXT,"
                + KEY_HR_MEASUREMENT_HEARTRATE + " TEXT" + ")";


        // CREATE RESPIRATIONRATE table name


        String CREATE_TABLE_RESPIRATIONRATE = "CREATE TABLE " + TABLE_RESPIRATIONRATE + "("
                + KEY_ID_RESPIRATIONRATE + " INTEGER PRIMARY KEY,"
                + KEY_EVENT_BATTERY_RESPIRATIONRATE + " TEXT,"
                + KEY_ST_TIMESTAMP_RESPIRATIONRATE + " TEXT,"
                + KEY_RR_MEASUREMENT_RESPIRATIONRATE + " TEXT" + ")";



        db.execSQL(CREATE_WALK_TABLE);
        db.execSQL(CREATE_SOS_TABLE);
        db.execSQL(CREATE_TRANSACTION_TABLE);
        db.execSQL(CREATE_TUG_TABLE);
        db.execSQL(CREATE_SLEEP_TABLE);
        db.execSQL(CREATE_WAVE_TABLE);
        db.execSQL(CREATE_FALLDETECTION_TABLE);
        db.execSQL(CREATE_TMWALK_TABLE);
        db.execSQL(CREATE_TMWALK_DOFFED);

        db.execSQL(CREATE_TABLE_HEARTRATE);

        db.execSQL(CREATE_TABLE_RESPIRATIONRATE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WALK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TUG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SLEEP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WAVE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FALLDETECTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TMWALK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOFFED);

        //ADDED ON 09-FEB-2018
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HEARTRATE);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESPIRATIONRATE);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public void addLocalDataWalk(WalkGetSet walkGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY, walkGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP, walkGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP, walkGetSet.end_timestamp);
        values.put(KEY_DISTANCE_WALKED, walkGetSet.distance_walked);
        values.put(KEY_NO_STEPS, walkGetSet.num_of_steps);
        values.put(KEY_STEP_LENGTH, walkGetSet.step_length);
        values.put(KEY_IMPACT, walkGetSet.impact);
        values.put(KEY_INACTIVITY_SCORE, walkGetSet.inactivity_score);
        //values.put(KEY_ARR_FLAG, walkGetSet.arr_flag);


        // Inserting Row
        db.insert(TABLE_WALK, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    WalkGetSet getLocalDataWalk(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_WALK, new String[]{KEY_ID,
                        KEY_EVENT_BATTERY, KEY_ST_TIMESTAMP, KEY_END_TIMESTAMP, KEY_DISTANCE_WALKED, KEY_NO_STEPS, KEY_STEP_LENGTH, KEY_IMPACT, KEY_INACTIVITY_SCORE}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        WalkGetSet walkGetSet = new WalkGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        // return contact
        return walkGetSet;

    }

    // Getting All trips
    public List<WalkGetSet> getAllLocalDataWalk() {
        List<WalkGetSet> contactList = new ArrayList<WalkGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversyncwalk ORDER BY keysttimestamp";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                WalkGetSet walkGetSet = new WalkGetSet();
                walkGetSet.setId(Integer.parseInt(cursor.getString(0)));
                walkGetSet.setEvent_battery(cursor.getString(1));
                walkGetSet.setSt_timestamp(cursor.getString(2));
                walkGetSet.setEnd_timestamp(cursor.getString(3));
                walkGetSet.setDistance_walked(cursor.getString(4));
                walkGetSet.setNum_of_steps(cursor.getString(5));
                walkGetSet.setStep_length(cursor.getString(6));
                walkGetSet.setImpact(cursor.getString(7));
                walkGetSet.setInactivity_score(cursor.getString(8));
                //  walkGetSet.setArr_flag(cursor.getString(7));

                // Adding trip to list
                contactList.add(walkGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return contactList;

    }

    // Updating single trip
    public int updateLocalDataWalk(WalkGetSet walkGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY, walkGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP, walkGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP, walkGetSet.getEnd_timestamp());
        values.put(KEY_DISTANCE_WALKED, walkGetSet.getDistance_walked());
        values.put(KEY_NO_STEPS, walkGetSet.getNum_of_steps());
        values.put(KEY_STEP_LENGTH, walkGetSet.getStep_length());
        values.put(KEY_IMPACT, walkGetSet.getImpact());
        values.put(KEY_INACTIVITY_SCORE, walkGetSet.getInactivity_score());
        // values.put(KEY_ARR_FLAG, walkGetSet.getArr_flag());

        // updating row
        return db.update(TABLE_WALK, values, KEY_ID + " = ?",
                new String[]{String.valueOf(walkGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataWalk() {
        SQLiteDatabase db = this.getWritableDatabase();
        //  db.delete(TABLE_WALK, KEY_ID + " = ?", new String[]{String.valueOf(walkGetSet.getId())});
        db.execSQL("delete from " + TABLE_WALK);

        db.close();
    }

    // Getting trips Count
    public int getLocalDataWalkCount() {
        String countQuery = "SELECT  * FROM " + TABLE_WALK;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

//SOS Table Entry Start

    public void addLocalDataSOS(SOSGetSet sosGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_SOS, sosGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_SOS, sosGetSet.st_timestamp);
        values.put(KEY_HR_MEASUREMENT, sosGetSet.hr_measurement);


        // Inserting Row
        db.insert(TABLE_SOS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    SOSGetSet getLocalDataSOS(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SOS, new String[]{KEY_ID_SOS,
                        KEY_EVENT_BATTERY_SOS, KEY_ST_TIMESTAMP_SOS, KEY_HR_MEASUREMENT}, KEY_ID_SOS + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        SOSGetSet tripperGetSet = new SOSGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1));

        // return contact
        return tripperGetSet;

    }

    // Getting All trips
    public List<SOSGetSet> getAllLocalDataSOS() {
        List<SOSGetSet> sosList = new ArrayList<SOSGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversyncsos ORDER BY keysttimestampsos";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SOSGetSet walkGetSet = new SOSGetSet();
                walkGetSet.setId(Integer.parseInt(cursor.getString(0)));
                walkGetSet.setEvent_battery(cursor.getString(1));
                walkGetSet.setSt_timestamp(cursor.getString(2));
                walkGetSet.setHr_measurement(cursor.getString(3));

                // Adding trip to list
                sosList.add(walkGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return sosList;

    }

    // Updating single trip
    public int updateLocalDataSOS(SOSGetSet walkGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_SOS, walkGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_SOS, walkGetSet.getSt_timestamp());
        values.put(KEY_HR_MEASUREMENT, walkGetSet.getHr_measurement());

        // updating row
        return db.update(TABLE_SOS, values, KEY_ID_SOS + " = ?",
                new String[]{String.valueOf(walkGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataSOS() {
        SQLiteDatabase db = this.getWritableDatabase();
        // db.delete(TABLE_SOS, KEY_ID_SOS + " = ?", new String[]{String.valueOf(sosGetSet.getId())});
        db.execSQL("delete from " + TABLE_SOS);

        db.close();
    }


    // Getting trips Count
    public int getCountLocalDataSOS() {
        String countQuery = "SELECT  * FROM " + TABLE_SOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

//Transaction Table Entry Start

    public void addLocalDataTRANSACTION(TransactionGetSet transactionGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TRANS, transactionGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_TRANS, transactionGetSet.st_timestamp);
        values.put(KEY_CURRENT_POSTURE_TRANS, transactionGetSet.current_posture);
        values.put(KEY_TRANS_MODE_TRANS, transactionGetSet.transaction_mode);
        values.put(KEY_POS_TRANS_TIME_TRANS, transactionGetSet.pos_transaction_time);
        values.put(KEY_POS_TRANS_INTANCITY_TRANS, transactionGetSet.pos_transaction_intansity);

        // Inserting Row
        db.insert(TABLE_TRANSACTION, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    TransactionGetSet getLocalDataTRANSACTION(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TRANSACTION, new String[]{KEY_ID_TRANS,
                        KEY_EVENT_BATTERY_TRANS, KEY_ST_TIMESTAMP_TRANS, KEY_CURRENT_POSTURE_TRANS, KEY_TRANS_MODE_TRANS, KEY_POS_TRANS_TIME_TRANS, KEY_POS_TRANS_INTANCITY_TRANS}, KEY_ID_TRANS + "=?",

                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        TransactionGetSet transactionGetSet = new TransactionGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        return transactionGetSet;

    }

    // Getting All trips
    public List<TransactionGetSet> getAllLocalDataTRANSACTION() {
        List<TransactionGetSet> transactionList = new ArrayList<TransactionGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversynctransaction ORDER BY keysttimestamptrans";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TransactionGetSet transactionGetSet = new TransactionGetSet();
                transactionGetSet.setId(Integer.parseInt(cursor.getString(0)));
                transactionGetSet.setEvent_battery(cursor.getString(1));
                transactionGetSet.setSt_timestamp(cursor.getString(2));
                transactionGetSet.setCurrent_posture(cursor.getString(3));
                transactionGetSet.setTransaction_mode(cursor.getString(4));
                transactionGetSet.setPos_transaction_time(cursor.getString(5));
                transactionGetSet.setPos_transaction_intansity(cursor.getString(6));

                // Adding trip to list
                transactionList.add(transactionGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return transactionList;

    }

    // Updating single trip
    public int updateLocalDataTRANSACTION(TransactionGetSet transactionGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TRANS, transactionGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_TRANS, transactionGetSet.getSt_timestamp());
        values.put(KEY_CURRENT_POSTURE_TRANS, transactionGetSet.getCurrent_posture());
        values.put(KEY_TRANS_MODE_TRANS, transactionGetSet.getTransaction_mode());
        values.put(KEY_POS_TRANS_TIME_TRANS, transactionGetSet.getPos_transaction_time());
        values.put(KEY_POS_TRANS_INTANCITY_TRANS, transactionGetSet.getPos_transaction_intansity());

        // updating row
        return db.update(TABLE_TRANSACTION, values, KEY_ID_TRANS + " = ?",
                new String[]{String.valueOf(transactionGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataTRANSACTION() {
        SQLiteDatabase db = this.getWritableDatabase();
        // db.delete(TABLE_TRANSACTION, KEY_ID_TRANS + " = ?", new String[]{String.valueOf(transactionGetSet.getId())});
        db.execSQL("delete from " + TABLE_TRANSACTION);

        db.close();
    }

    // Getting trips Count
    public int getCountLocalDataTRANSACTION() {
        String countQuery = "SELECT  * FROM " + TABLE_TRANSACTION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    //TUG Table Entry Start

    // Tug table name

    public void addLocalDataTUG(TUGGetSet tugGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TUG, tugGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_TUG, tugGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP_TUG, tugGetSet.end_timestamp);
        values.put(KEY_DISTANCE_WALKED_TUG, tugGetSet.distance_walked);
        values.put(KEY_NUM_OF_STEPS_TUG, tugGetSet.num_of_steps);
        values.put(KEY_INACTIVITY_SCORE_TUG, tugGetSet.inactivity_score);

        // Inserting Row
        db.insert(TABLE_TUG, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    TUGGetSet getLocalDataTUG(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TUG, new String[]{KEY_ID_TUG,
                        KEY_EVENT_BATTERY_TUG, KEY_ST_TIMESTAMP_TUG, KEY_END_TIMESTAMP_TUG, KEY_DISTANCE_WALKED_TUG, KEY_NUM_OF_STEPS_TUG, KEY_INACTIVITY_SCORE_TUG}, KEY_ID_TUG + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        TUGGetSet transactionGetSet = new TUGGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        return transactionGetSet;

    }

    // Getting All trips
    public List<TUGGetSet> getAllLocalDataTUG() {
        List<TUGGetSet> tugList = new ArrayList<TUGGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversynctug ORDER BY keysttimestamptug";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TUGGetSet tugGetSet = new TUGGetSet();
                tugGetSet.setId(Integer.parseInt(cursor.getString(0)));
                tugGetSet.setEvent_battery(cursor.getString(1));
                tugGetSet.setSt_timestamp(cursor.getString(2));
                tugGetSet.setEnd_timestamp(cursor.getString(3));
                tugGetSet.setDistance_walked(cursor.getString(4));
                tugGetSet.setNum_of_steps(cursor.getString(5));
                tugGetSet.setInactivity_score(cursor.getString(6));

                // Adding trip to list
                tugList.add(tugGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return tugList;

    }

    // Updating single trip
    public int updateLocalDataTUG(TUGGetSet tugGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TUG, tugGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_TUG, tugGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP_TUG, tugGetSet.getEnd_timestamp());
        values.put(KEY_DISTANCE_WALKED_TUG, tugGetSet.getDistance_walked());
        values.put(KEY_NUM_OF_STEPS_TUG, tugGetSet.getNum_of_steps());
        values.put(KEY_INACTIVITY_SCORE_TUG, tugGetSet.getInactivity_score());

        // updating row
        return db.update(TABLE_TUG, values, KEY_ID_TUG + " = ?",
                new String[]{String.valueOf(tugGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataTUG() {
        SQLiteDatabase db = this.getWritableDatabase();
        // db.delete(TABLE_TUG, KEY_ID_TUG + " = ?", new String[]{String.valueOf(tugGetSet.getId())});
        db.execSQL("delete from " + TABLE_TUG);
        db.close();
    }

    // Getting trips Count
    public int getCountLocalDataTUG() {
        String countQuery = "SELECT  * FROM " + TABLE_TUG;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

//Sleep Entry Start here

    // Sleep table name

    public void addLocalDataSleep(SleepGetSet sleepGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_SLEEP, sleepGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_SLEEP, sleepGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP_SLEEP, sleepGetSet.end_timestamp);
        values.put(KEY_SOL_SLEEP, sleepGetSet.slp_sol);
        values.put(KEY_TIB_SLEEP, sleepGetSet.slp_tib);
        values.put(KEY_TST_SLEEP, sleepGetSet.slp_tst);
        values.put(KEY_WASO_SLEEP, sleepGetSet.slp_waso);
        values.put(KEY_SE_SLEEP, sleepGetSet.slp_se);
        values.put(KEY_POSITION_CHANGES, sleepGetSet.slp_position_changes);

        // Inserting Row
        db.insert(TABLE_SLEEP, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    SleepGetSet getLocalDataSleep(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SLEEP, new String[]{KEY_ID_SLEEP,
                        KEY_EVENT_BATTERY_SLEEP, KEY_ST_TIMESTAMP_SLEEP, KEY_END_TIMESTAMP_SLEEP, KEY_SOL_SLEEP, KEY_TIB_SLEEP, KEY_TST_SLEEP, KEY_WASO_SLEEP, KEY_SE_SLEEP, KEY_POSITION_CHANGES}, KEY_ID_SLEEP + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        SleepGetSet sleepGetSet = new SleepGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        return sleepGetSet;

    }

    // Getting All trips
    public List<SleepGetSet> getAllLocalDataSleep() {
        List<SleepGetSet> sleepList = new ArrayList<SleepGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversyncsleep ORDER BY keysttimestampsleep";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SleepGetSet sleepGetSet = new SleepGetSet();
                sleepGetSet.setId(Integer.parseInt(cursor.getString(0)));
                sleepGetSet.setEvent_battery(cursor.getString(1));
                sleepGetSet.setSt_timestamp(cursor.getString(2));
                sleepGetSet.setEnd_timestamp(cursor.getString(3));
                sleepGetSet.setSlp_sol(cursor.getString(4));
                sleepGetSet.setSlp_tib(cursor.getString(5));
                sleepGetSet.setSlp_tst(cursor.getString(6));
                sleepGetSet.setSlp_waso(cursor.getString(7));
                sleepGetSet.setSlp_se(cursor.getString(8));
                sleepGetSet.setSlp_position_changes(cursor.getString(9));

                // Adding trip to list
                sleepList.add(sleepGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return sleepList;

    }

    // Updating single trip
    public int updateLocalDataSleep(SleepGetSet sleepGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TUG, sleepGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_TUG, sleepGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP_TUG, sleepGetSet.getEnd_timestamp());
        values.put(KEY_DISTANCE_WALKED_TUG, sleepGetSet.getSlp_sol());
        values.put(KEY_TIB_SLEEP, sleepGetSet.getSlp_tib());
        values.put(KEY_TST_SLEEP, sleepGetSet.getSlp_tst());
        values.put(KEY_WASO_SLEEP, sleepGetSet.getSlp_waso());
        values.put(KEY_SE_SLEEP, sleepGetSet.getSlp_se());
        values.put(KEY_POSITION_CHANGES, sleepGetSet.getSlp_position_changes());

        // updating row
        return db.update(TABLE_SLEEP, values, KEY_ID_SLEEP + " = ?",
                new String[]{String.valueOf(sleepGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataSleep() {
        SQLiteDatabase db = this.getWritableDatabase();
        // db.delete(TABLE_SLEEP, KEY_ID_SLEEP + " = ?", new String[]{String.valueOf(sleepGetSet.getId())});
        db.execSQL("delete from " + TABLE_SLEEP);
        db.close();
    }

    // Getting trips Count
    public int getCountLocalDataSleep() {
        String countQuery = "SELECT  * FROM " + TABLE_SLEEP;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }

    //Wave Table Entry Start

    public void addLocalDataWave(WaveGetSet waveGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_WAVE, waveGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_WAVE, waveGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP_WAVE, waveGetSet.end_timestamp);
        values.put(KEY_HR_MEASUREMENT_WAVE, waveGetSet.hr_measurement);
        values.put(KEY_ATTENTION_REQUEST_WAVE, waveGetSet.attention_request);

        // Inserting Row
        db.insert(TABLE_WAVE, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    WaveGetSet getLocalDataWave(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_WAVE, new String[]{KEY_ID_WAVE,
                        KEY_EVENT_BATTERY_WAVE, KEY_ST_TIMESTAMP_WAVE, KEY_END_TIMESTAMP_WAVE, KEY_HR_MEASUREMENT_WAVE, KEY_ATTENTION_REQUEST_WAVE}, KEY_ID_WAVE + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        WaveGetSet waveGetSet = new WaveGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        // return contact
        return waveGetSet;

    }

    // Getting All trips
    public List<WaveGetSet> getAllLocalDataWave() {
        List<WaveGetSet> sosList = new ArrayList<WaveGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversyncwave ORDER BY keysttimestampwave";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                WaveGetSet waveGetSet = new WaveGetSet();
                waveGetSet.setId(Integer.parseInt(cursor.getString(0)));
                waveGetSet.setEvent_battery(cursor.getString(1));
                waveGetSet.setSt_timestamp(cursor.getString(2));
                waveGetSet.setEnd_timestamp(cursor.getString(3));
                waveGetSet.setHr_measurement(cursor.getString(4));
                waveGetSet.setAttention_request(cursor.getString(5));

                // Adding trip to list
                sosList.add(waveGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return sosList;

    }

    // Updating single trip
    public int updateLocalDataWave(WaveGetSet waveGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_WAVE, waveGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_WAVE, waveGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP_WAVE, waveGetSet.getEnd_timestamp());
        values.put(KEY_HR_MEASUREMENT_WAVE, waveGetSet.getHr_measurement());
        values.put(KEY_ATTENTION_REQUEST_WAVE, waveGetSet.getAttention_request());

        // updating row
        return db.update(TABLE_WAVE, values, KEY_ID_WAVE + " = ?",
                new String[]{String.valueOf(waveGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataWave() {
        SQLiteDatabase db = this.getWritableDatabase();
        // db.delete(TABLE_WAVE, KEY_ID_WAVE + " = ?", new String[]{String.valueOf(waveGetSet.getId())});
        db.execSQL("delete from " + TABLE_WAVE);

        db.close();
    }

    // Getting trips Count
    public int getCountLocalDataWave() {
        String countQuery = "SELECT  * FROM " + TABLE_WAVE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
    //

    //FallDetection Table Entry Start

    public void addLocalDataFallDetection(FalldetectionGetSet falldetectionGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_FALLDETECTION, falldetectionGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_FALLDETECTION, falldetectionGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP_FALLDETECTION, falldetectionGetSet.end_timestamp);
        values.put(KEY_DISTANCE_WALKED_FALLDETECTION, falldetectionGetSet.distance_walked);
        values.put(KEY_NUM_OF_STEPS_FALLDETECTION, falldetectionGetSet.num_of_steps);
        values.put(KEY_STEP_LENGTH_FALLDETECTION, falldetectionGetSet.step_length);
        values.put(KEY_STEP_IMPACT_FALLDETECTION, falldetectionGetSet.step_impact);
        values.put(KEY_GFORCE_FALLDETECTION, falldetectionGetSet.fall_gforce_level);
        values.put(KEY_CONFIDENCE_FALLDETECTION, falldetectionGetSet.fall_confidence);

        // Inserting Row
        db.insert(TABLE_FALLDETECTION, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    FalldetectionGetSet getLocalDataFallDetection(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_FALLDETECTION, new String[]{KEY_ID_FALLDETECTION,
                        KEY_EVENT_BATTERY_FALLDETECTION, KEY_ST_TIMESTAMP_FALLDETECTION, KEY_END_TIMESTAMP_FALLDETECTION, KEY_DISTANCE_WALKED_FALLDETECTION, KEY_NUM_OF_STEPS_FALLDETECTION, KEY_STEP_LENGTH_FALLDETECTION, KEY_STEP_IMPACT_FALLDETECTION, KEY_GFORCE_FALLDETECTION, KEY_CONFIDENCE_FALLDETECTION}, KEY_ID_FALLDETECTION + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        FalldetectionGetSet falldetectionGetSet = new FalldetectionGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        // return contact
        return falldetectionGetSet;

    }

    // Getting All trips
    public List<FalldetectionGetSet> getAllLocalDataFallDetection() {
        List<FalldetectionGetSet> falldetectionList = new ArrayList<FalldetectionGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversyncfalldetection ORDER BY keysttimestampfalldetection";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                FalldetectionGetSet falldetectionGetSet = new FalldetectionGetSet();
                falldetectionGetSet.setId(Integer.parseInt(cursor.getString(0)));
                falldetectionGetSet.setEvent_battery(cursor.getString(1));
                falldetectionGetSet.setSt_timestamp(cursor.getString(2));
                falldetectionGetSet.setEnd_timestamp(cursor.getString(3));
                falldetectionGetSet.setDistance_walked(cursor.getString(4));
                falldetectionGetSet.setNum_of_steps(cursor.getString(5));
                falldetectionGetSet.setStep_length(cursor.getString(6));
                falldetectionGetSet.setStep_impact(cursor.getString(7));
                falldetectionGetSet.setFall_gforce_level(cursor.getString(8));
                falldetectionGetSet.setFall_confidence(cursor.getString(9));
                // Adding trip to list
                falldetectionList.add(falldetectionGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return falldetectionList;

    }

    // Updating single trip
    public int updateLocalDataFallDetection(FalldetectionGetSet falldetectionGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_FALLDETECTION, falldetectionGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_FALLDETECTION, falldetectionGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP_FALLDETECTION, falldetectionGetSet.getEnd_timestamp());
        values.put(KEY_DISTANCE_WALKED_FALLDETECTION, falldetectionGetSet.getDistance_walked());
        values.put(KEY_NUM_OF_STEPS_FALLDETECTION, falldetectionGetSet.getNum_of_steps());
        values.put(KEY_STEP_LENGTH_FALLDETECTION, falldetectionGetSet.getStep_length());
        values.put(KEY_STEP_IMPACT_FALLDETECTION, falldetectionGetSet.getStep_impact());
        values.put(KEY_GFORCE_FALLDETECTION, falldetectionGetSet.getFall_gforce_level());
        values.put(KEY_CONFIDENCE_FALLDETECTION, falldetectionGetSet.getFall_confidence());

        // updating row
        return db.update(TABLE_FALLDETECTION, values, KEY_ID_FALLDETECTION + " = ?",
                new String[]{String.valueOf(falldetectionGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataFallDetection() {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_FALLDETECTION, KEY_ID_FALLDETECTION + " = ?", new String[]{String.valueOf(waveGetSet.getId())});
        db.execSQL("delete from " + TABLE_FALLDETECTION);
        db.close();
    }

    // Getting trips Count
    public int getCountLocalDataFallDetection() {
        String countQuery = "SELECT  * FROM " + TABLE_FALLDETECTION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }
//Three Min Walk Start Here

    public void addLocalDataTMWalk(ThreeMWalkGetSet walkGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TMWALK, walkGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_TMWALK, walkGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP_TMWALK, walkGetSet.end_timestamp);
        values.put(KEY_DISTANCE_WALKED_TMWALK, walkGetSet.distance_walked);
        values.put(KEY_NO_STEPS_TMWALK, walkGetSet.num_of_steps);
        values.put(KEY_STEP_LENGTH_TMWALK, walkGetSet.step_length);
        values.put(KEY_IMPACT_TMWALK, walkGetSet.impact);


        // Inserting Row
        db.insert(TABLE_TMWALK, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    ThreeMWalkGetSet getLocalDataTMWalk(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TMWALK, new String[]{KEY_ID_TMWALK,
                        KEY_EVENT_BATTERY_TMWALK, KEY_ST_TIMESTAMP_TMWALK, KEY_END_TIMESTAMP_TMWALK, KEY_DISTANCE_WALKED_TMWALK, KEY_NO_STEPS_TMWALK, KEY_STEP_LENGTH_TMWALK, KEY_IMPACT_TMWALK}, KEY_ID_TMWALK + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ThreeMWalkGetSet walkGetSet = new ThreeMWalkGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        // return contact
        return walkGetSet;

    }

    // Getting All trips
    public List<ThreeMWalkGetSet> getAllLocalDataTMWalk() {
        List<ThreeMWalkGetSet> tmwalkList = new ArrayList<ThreeMWalkGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversynctmwalk ORDER BY keyeventbatterytmwalk";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ThreeMWalkGetSet threeMWalkGetSet = new ThreeMWalkGetSet();
                threeMWalkGetSet.setId(Integer.parseInt(cursor.getString(0)));
                threeMWalkGetSet.setEvent_battery(cursor.getString(1));
                threeMWalkGetSet.setSt_timestamp(cursor.getString(2));
                threeMWalkGetSet.setEnd_timestamp(cursor.getString(3));
                threeMWalkGetSet.setDistance_walked(cursor.getString(4));
                threeMWalkGetSet.setNum_of_steps(cursor.getString(5));
                threeMWalkGetSet.setStep_length(cursor.getString(6));
                threeMWalkGetSet.setImpact(cursor.getString(7));

                // Adding trip to list
                tmwalkList.add(threeMWalkGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return tmwalkList;

    }

    // Updating single trip
    public int updateLocalDataTMWalk(ThreeMWalkGetSet threeMWalkGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_TMWALK, threeMWalkGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_TMWALK, threeMWalkGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP_TMWALK, threeMWalkGetSet.getEnd_timestamp());
        values.put(KEY_DISTANCE_WALKED_TMWALK, threeMWalkGetSet.getDistance_walked());
        values.put(KEY_NO_STEPS_TMWALK, threeMWalkGetSet.getNum_of_steps());
        values.put(KEY_STEP_LENGTH_TMWALK, threeMWalkGetSet.getStep_length());
        values.put(KEY_IMPACT_TMWALK, threeMWalkGetSet.getImpact());

        // updating row
        return db.update(TABLE_TMWALK, values, KEY_ID_TMWALK + " = ?",
                new String[]{String.valueOf(threeMWalkGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataTMWalk() {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_TMWALK, KEY_ID_TMWALK + " = ?", new String[]{String.valueOf(threeMWalkGetSet.getId())});
        db.execSQL("delete from " + TABLE_TMWALK);

        db.close();
    }

    // Getting trips Count
    public int getLocalDataTMWalkCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TMWALK;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    //Doffed Data

    public void addLocalDataDoffed(DoffedGetSet doffedGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_DOFFED, doffedGetSet.event_battery);
        values.put(KEY_ST_TIMESTAMP_DOFFED, doffedGetSet.st_timestamp);
        values.put(KEY_END_TIMESTAMP_DOFFED, doffedGetSet.end_timestamp);
        values.put(KEY_TIME_DOFFED, doffedGetSet.doffed_time);

        // Inserting Row
        db.insert(TABLE_DOFFED, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    DoffedGetSet getLocalDataDoffed(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_DOFFED, new String[]{KEY_ID_DOFFED,
                        KEY_EVENT_BATTERY_DOFFED, KEY_ST_TIMESTAMP_DOFFED, KEY_END_TIMESTAMP_DOFFED, KEY_TIME_DOFFED}, KEY_ID_DOFFED + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        DoffedGetSet walkGetSet = new DoffedGetSet(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(1), cursor.getString(1), cursor.getString(1));

        // return contact
        return walkGetSet;

    }

    // Getting All trips
    public List<DoffedGetSet> getAllLocalDataDoffed() {
        List<DoffedGetSet> doffedGetSets = new ArrayList<DoffedGetSet>();
        // Select All Query
        String selectQuery = "SELECT  * FROM nsdserversyncdoffed ORDER BY keysttimestampdoffed";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DoffedGetSet doffedGetSet = new DoffedGetSet();
                doffedGetSet.setId(Integer.parseInt(cursor.getString(0)));
                doffedGetSet.setEvent_battery(cursor.getString(1));
                doffedGetSet.setSt_timestamp(cursor.getString(2));
                doffedGetSet.setEnd_timestamp(cursor.getString(3));
                doffedGetSet.setDoffed_time(cursor.getString(4));

                // Adding trip to list
                doffedGetSets.add(doffedGetSet);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return doffedGetSets;

    }


    // Updating single trip
    public int updateLocalDataDoffed(DoffedGetSet doffedGetSet) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_DOFFED, doffedGetSet.getEvent_battery());
        values.put(KEY_ST_TIMESTAMP_DOFFED, doffedGetSet.getSt_timestamp());
        values.put(KEY_END_TIMESTAMP_DOFFED, doffedGetSet.getEnd_timestamp());
        values.put(KEY_TIME_DOFFED, doffedGetSet.getDoffed_time());

        // updating row
        return db.update(TABLE_DOFFED, values, KEY_ID_DOFFED + " = ?",
                new String[]{String.valueOf(doffedGetSet.getId())});

    }

    // Deleting single trip
    public void deleteLocalDataDoffed() {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_TMWALK, KEY_ID_TMWALK + " = ?", new String[]{String.valueOf(threeMWalkGetSet.getId())});
        db.execSQL("delete from " + TABLE_DOFFED);

        db.close();
    }

    // Getting trips Count
    public int getLocalDataDoffedCount() {
        String countQuery = "SELECT  * FROM " + TABLE_DOFFED;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    //ADDED ON 09-FEB-2018

    public void addLocalDataHEARTRATE(String ht_battery,String ht_timestamp,String ht_measurement ) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_HEARTRATE, ht_battery);
        values.put(KEY_ST_TIMESTAMP_HEARTRATE, ht_timestamp);
        values.put(KEY_HR_MEASUREMENT_HEARTRATE, ht_measurement);


        // Inserting Row
        db.insert(TABLE_HEARTRATE, null, values);
        db.close(); // Closing database connection
    }
    public List<HashMap> getLocalDataHeartRate() {
        List<HashMap> sosList = new ArrayList<HashMap>();
        // Select All Query




        String selectQuery = "SELECT  * FROM nsdserversyncheartrate ORDER BY keysttimestampheartrate";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
               HashMap<String,String> hp=new HashMap();

                hp.put("Event_battery",cursor.getString(1));
                hp.put("St_timestamp",cursor.getString(2));
                hp.put("Hr_measurement",cursor.getString(3));

                // Adding trip to list
                sosList.add(hp);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return trip list
        return sosList;

    }

    public void deleteLocalDataHeartRate() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_HEARTRATE);

        db.close();
    }



    //ADDED ON 16-FEB-2018

    public void addLocalDataRESPIRATIONRATE(String rr_battery,String rr_timestamp,String rr_measurement ) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_EVENT_BATTERY_RESPIRATIONRATE, rr_battery);
        values.put(KEY_ST_TIMESTAMP_RESPIRATIONRATE, rr_timestamp);
        values.put(KEY_RR_MEASUREMENT_RESPIRATIONRATE, rr_measurement);


        // Inserting Row
        db.insert(TABLE_RESPIRATIONRATE, null, values);
        db.close(); // Closing database connection
    }
    public List<HashMap> getLocalDataRespirationRate() {
        List<HashMap> rrList = new ArrayList<HashMap>();
        // Select All Query

        String selectQuery = "SELECT  * FROM nsdserversyncrespirationrate ORDER BY keysttimestamprespirationrate";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list


        if (cursor.moveToFirst()) {
            do {
                HashMap<String,String> hp=new HashMap();

                hp.put("Event_battery",cursor.getString(1));
                hp.put("rr_timestamp",cursor.getString(2));
                hp.put("rr_measurement",cursor.getString(3));

                // Adding trip to list
                rrList.add(hp);
            } while (cursor.moveToNext());
        }

       /* if(cursor != null)
        {
            if (cursor.moveToFirst()) {
                HashMap<String,String> hp=new HashMap();

                hp.put("Event_battery",cursor.getString(1));
                hp.put("rr_timestamp",cursor.getString(2));
                hp.put("rr_measurement",cursor.getString(3));

                rrList.add(hp);
            }
        }
        cursor.close();*/

        // close inserting data from database
        db.close();
        // return trip list
        return rrList;

    }

    public void deleteLocalDataRespirationRate() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_RESPIRATIONRATE);

        db.close();
    }



}
