package background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    public BatteryLevelReceiver mBatInfoReceiver = null;


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        Toast.makeText(context,"MyReceiver",Toast.LENGTH_SHORT).show();

        mBatInfoReceiver = new BatteryLevelReceiver();

        IntentFilter ifilter = new IntentFilter();
        ifilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        context.getApplicationContext().registerReceiver(mBatInfoReceiver,ifilter);

    }
}
