package background;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import adapter.DoffedGetSet;
import adapter.FalldetectionGetSet;
import adapter.SOSGetSet;
import adapter.SleepGetSet;
import adapter.TUGGetSet;
import adapter.ThreeMWalkGetSet;
import adapter.TransactionGetSet;
import adapter.WalkGetSet;
import adapter.WaveGetSet;
import de.fraunhofer.igd.serversync.R;


/**
 * Created by Snigdha on 22-11-2017.
 */

public class SyncOffline {

    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    ArrayList<Integer> hrArrListEventBattery = new ArrayList<>();
    ArrayList<String> hrArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> hrArrListHrMeasurement = new ArrayList<>();

    ArrayList<Integer> dfArrListEventBattery = new ArrayList<>();
    ArrayList<String> dfArrListStTimestamp = new ArrayList<>();
    ArrayList<Long> dfArrListTotalDoffedTime = new ArrayList<>();

    ArrayList<Integer> tranArrListEventBattery = new ArrayList<>();
    ArrayList<String> tranArrListStTimestamp = new ArrayList<>();
    ArrayList<Integer> tranArrListCurrentPosture = new ArrayList<>();
    ArrayList<Integer> tranArrListTransitionMode = new ArrayList<>();
    ArrayList<Double> tranArrListPosTransitionTime = new ArrayList<>();
    ArrayList<Double> tranArrListPosTransitionIntensity = new ArrayList<>();

    ArrayList<Integer> walkArrListEventBattery = new ArrayList<>();
    ArrayList<String> walkArrListStTimestamp = new ArrayList<>();
    ArrayList<String> walkArrListEndTimestamp = new ArrayList<>();
    ArrayList<Double> walkArrListDistanceWalked = new ArrayList<>();
    ArrayList<Long> walkArrListNumberOfSteps = new ArrayList<>();
    ArrayList<Double> walkArrListStepLength = new ArrayList<>();
    ArrayList<Double> walkArrListStepImpact = new ArrayList<>();
    ArrayList<Double> walkArrListInactivityScore = new ArrayList<>();

    Context context_new;

    private static Timer timer = null;
    // private final int timerInterval = 2 * 60 * 60 * 1000;
    private final int timerInterval = 1 * 30 * 60 * 1000;
    //SOS  SQL Array
    ArrayList<String> sosArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> sosArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> sosArrListHrMeasurementdb = new ArrayList<>();

    // Walk SQL Array
    ArrayList<String> arrEventBatterydbMain = new ArrayList<>();
    ArrayList<String> arrStTimestampdbMain = new ArrayList<>();
    ArrayList<String> arrEndTimestampdbMain = new ArrayList<>();
    ArrayList<String> arrDistanceWalkeddbMain = new ArrayList<>();
    ArrayList<String> arrNumberOfStepsdbMain = new ArrayList<>();
    ArrayList<String> arrStepLengthdbMain = new ArrayList<>();
    ArrayList<String> arrStepImpactdbMain = new ArrayList<>();
    ArrayList<String> arrInactivityScoredbMain = new ArrayList<>();

    //TUG SQL Array
    ArrayList<String> tugArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> tugArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> tugArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> tugArrListDistanceWalkeddb = new ArrayList<>();
    ArrayList<String> tugArrListNumberOfStepsdb = new ArrayList<>();
    ArrayList<String> tugArrListInactivityScoredb = new ArrayList<>();

    //Wave SQL Array
    ArrayList<String> waveArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> waveArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> waveArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> waveArrListHrMeasurementdb = new ArrayList<>();
    ArrayList<String> waveArrListAttentionRequestdb = new ArrayList<>();

    //Fall Detection SQL Array
    ArrayList<String> fallArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> fallArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> fallArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> fallArrListDistanceWalkeddb = new ArrayList<>();
    ArrayList<String> fallArrListNumberOfStepsdb = new ArrayList<>();
    ArrayList<String> fallArrListStepImpactdb = new ArrayList<>();
    ArrayList<String> fallArrListStepLengthdb = new ArrayList<>();
    ArrayList<String> fallArrListForceLeveldb = new ArrayList<>();
    ArrayList<String> fallArrListFallConfidencedb = new ArrayList<>();

    //Sleep SQL Array
    ArrayList<String> sleepArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> sleepArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> sleepArrListEndTimestampb = new ArrayList<>();
    ArrayList<String> sleepArrListSolSLeepddb = new ArrayList<>();
    ArrayList<String> sleepArrListTibSleepsdb = new ArrayList<>();
    ArrayList<String> sleepArrListTstSleepdb = new ArrayList<>();
    ArrayList<String> sleepArrListWasoSleepdb = new ArrayList<>();
    ArrayList<String> sleepArrListSeSleepdb = new ArrayList<>();
    ArrayList<String> sleepArrListPositionChangeSleepdb = new ArrayList<>();

    //3 Min Walk SQL Array
    ArrayList<String> tmwArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> tmwArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> tmwArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> tmwArrListDistanceWalkeddb = new ArrayList<>();
    ArrayList<String> tmwArrListNumberOfStepsdb = new ArrayList<>();
    ArrayList<String> tmwArrListStepLengthdb = new ArrayList<>();
    ArrayList<String> tmwArrListStepImpactdb = new ArrayList<>();

    //Transaction SQL Array
    ArrayList<String> tranArrListEventBatterydbMain = new ArrayList<>();
    ArrayList<String> tranArrListStTimestampdbMain = new ArrayList<>();
    ArrayList<String> tranArrListCurrentPosturedbMain = new ArrayList<>();
    ArrayList<String> tranArrListTransitionModedbMain = new ArrayList<>();
    ArrayList<String> tranArrListPosTransitionTimedbMain = new ArrayList<>();
    ArrayList<String> tranArrListPosTransitionIntensitydbMain = new ArrayList<>();

    //Doffed SQL Array
    ArrayList<String> doffedArrListEventBatterydb = new ArrayList<>();
    ArrayList<String> doffedArrListStTimestampdb = new ArrayList<>();
    ArrayList<String> doffedArrListEndTimestampdb = new ArrayList<>();
    ArrayList<String> doffedArrListDoffedTimedb = new ArrayList<>();

    String imei_common;
    DataBaseHandler dataBaseHandler;


    public SyncOffline(Context context)
    {
        context_new=context;
        dataBaseHandler = new DataBaseHandler(context);
        imei_common = CommonMethods.getIMEI(context);
        clearAll();
    }

    public void TransactionSQLDataRetrieve(Context context) {


        try {
            List<TransactionGetSet> transactionGetSets = dataBaseHandler.getAllLocalDataTRANSACTION();

            ArrayList<String> tranArrListEventBatterydb = new ArrayList<>();
            ArrayList<String> tranArrListStTimestampdb = new ArrayList<>();
            ArrayList<String> tranArrListCurrentPosturedb = new ArrayList<>();
            ArrayList<String> tranArrListTransitionModedb = new ArrayList<>();
            ArrayList<String> tranArrListPosTransitionTimedb = new ArrayList<>();
            ArrayList<String> tranArrListPosTransitionIntensitydb = new ArrayList<>();

            for (TransactionGetSet walking : transactionGetSets) {

                tranArrListEventBatterydb.add(walking.getEvent_battery());
                tranArrListStTimestampdb.add(walking.getSt_timestamp());
                tranArrListCurrentPosturedb.add(walking.getCurrent_posture());
                tranArrListTransitionModedb.add(walking.getTransaction_mode());
                tranArrListPosTransitionTimedb.add(walking.getPos_transaction_time());
                tranArrListPosTransitionIntensitydb.add(walking.getPos_transaction_intansity());


            }

            for (int i = 0; i < tranArrListEventBatterydb.size(); i++) {

                ArrayList<String> tranArrListEventBatterydb1 = new ArrayList(Arrays.asList(tranArrListEventBatterydb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListStTimestampdb1 = new ArrayList(Arrays.asList(tranArrListStTimestampdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListCurrentPosturedb1 = new ArrayList(Arrays.asList(tranArrListCurrentPosturedb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListTransitionModedb1 = new ArrayList(Arrays.asList(tranArrListTransitionModedb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListPosTransitionTimedb1 = new ArrayList(Arrays.asList(tranArrListPosTransitionTimedb.get(i).split("\\s*,\\s*")));
                ArrayList<String> tranArrListPosTransitionIntensitydb1 = new ArrayList(Arrays.asList(tranArrListPosTransitionIntensitydb.get(i).split("\\s*,\\s*")));

                for (int j = 0; j < tranArrListEventBatterydb1.size(); j++) {
                    tranArrListEventBatterydbMain.add(tranArrListEventBatterydb1.get(j));
                    tranArrListStTimestampdbMain.add(tranArrListStTimestampdb1.get(j));
                    tranArrListCurrentPosturedbMain.add(tranArrListCurrentPosturedb1.get(j));
                    tranArrListTransitionModedbMain.add(tranArrListTransitionModedb1.get(j));
                    tranArrListPosTransitionTimedbMain.add(tranArrListPosTransitionTimedb1.get(j));
                    tranArrListPosTransitionIntensitydbMain.add(tranArrListPosTransitionIntensitydb1.get(j));

                }

            }
            //  Toast.makeText(context.getApplicationContext(), "Transition SQL data uploading..." + tranArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

            if (!tranArrListEventBatterydbMain.isEmpty()) {

                String url_login = context.getResources().getString(R.string.ws_transition);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);


                JSONObject jsonObj = new JSONObject();
                JSONArray jsonArr = new JSONArray();
                for (int i = 0; i < tranArrListEventBatterydbMain.size(); i++) {

                    JSONObject jObjd = new JSONObject();
                    jObjd.put("EVENT_BATTERY", tranArrListEventBatterydbMain.get(i));
                    jObjd.put("ST_TIMESTAMP", tranArrListStTimestampdbMain.get(i));
                    jObjd.put("CURRENT_POSTURE", tranArrListCurrentPosturedbMain.get(i));
                    jObjd.put("TRANSITION_MODE", tranArrListTransitionModedbMain.get(i));
                    jObjd.put("POS_TRANSITION_TIME", tranArrListPosTransitionTimedbMain.get(i));
                    jObjd.put("POS_TRANSITION_INTENSITY", tranArrListPosTransitionIntensitydbMain.get(i));
                    jsonArr.put(jObjd);

                }

                jsonObj.put("data", jsonArr);
                jsonObj.put("IMEI", imei_common);



                new ServiceCall(context_new) {
                    @Override
                    protected void onPostExecute(Void unused) {
                        try {




                            if(getcode().equals("200")) {
                                String result = getResult();

                                JSONObject jsonObject = new JSONObject(result);
                                String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                                if (Integer.parseInt(code) == 0) {
                                    try {

                                        dataBaseHandler.deleteLocalDataTRANSACTION();
                                          /*  for (int i = 0; i < tranArrListEventBatterydbMain.size(); i++) {

                                                dataBaseHandler.deleteLocalDataTRANSACTION();

                                            }*/

                                    } catch (Exception e) {

                                    }


                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }


                    }


                }.execute(urlEncoded, jsonObj.toString());

                JSONObject sleepjsonObj = new JSONObject();
                JSONObject walkdata = new JSONObject();
                walkdata.put("data", jsonArr);

                sleepjsonObj.put("i_source_data", walkdata.toString());
                sleepjsonObj.put("i_data_source", "nsd.smartwatch.transaction");
                sleepjsonObj.put("i_imei", imei_common);


                new WatchServiceCall(context_new) {
                    @Override
                    protected void onPostExecute(Void unused) {
                        try {


                            if (getStatuscode().equals("200")) {
                                String hresp = getResult();
                                //Toast.makeText(context_new.getApplicationContext(), "NSDTrans_Off:" + hresp, Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }


                }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");





            }


        } catch (Exception e) {

            //   Toast.makeText(context.getApplicationContext(), "TRANSITION: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void TugSQLDataRetrieve(Context context) {


        try {

            List<TUGGetSet> tugGetSets = dataBaseHandler.getAllLocalDataTUG();

            for (TUGGetSet tugGetSet : tugGetSets) {

                tugArrListEventBatterydb.add(tugGetSet.getEvent_battery());
                tugArrListStTimestampdb.add(tugGetSet.getSt_timestamp());
                tugArrListEndTimestampdb.add(tugGetSet.getEnd_timestamp());
                tugArrListDistanceWalkeddb.add(tugGetSet.getDistance_walked());
                tugArrListNumberOfStepsdb.add(tugGetSet.getNum_of_steps());
                tugArrListInactivityScoredb.add(tugGetSet.getInactivity_score());


            }
            // Toast.makeText(context.getApplicationContext(), "TUG offline data uploading!" + tugArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
            if (!tugArrListEventBatterydb.isEmpty()) {

                String url_login = context.getResources().getString(R.string.ws_tug_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //   new BKTugArrayTaskNew().execute(urlEncoded);




                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tugArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tugArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", tugArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", tugArrListEndTimestampdb.get(i));
                        jObjd.put("DISTANCE_WALKED", tugArrListDistanceWalkeddb.get(i));
                        jObjd.put("NUMBER_OF_STEPS", tugArrListNumberOfStepsdb.get(i));
                        jObjd.put("INACTIVITY_SCORE", tugArrListInactivityScoredb.get(i));

                        jsonArr.put(jObjd);
                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();

                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                                    if (Integer.parseInt(code) == 0) {



                                        dataBaseHandler.deleteLocalDataTUG();
/*
                                                for (int i = 0; i < tugArrListEventBatterydb.size(); i++) {

                                                    dataBaseHandler.deleteLocalDataTUG();

                                                }*/



                                    }

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());

                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.tug");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    Toast.makeText(context_new.getApplicationContext(), "NSDTUG_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");


                } catch (JSONException ex) {

                }



            }
        } catch (Exception e) {
            //   Toast.makeText(context.getApplicationContext(), "TUG: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void SOSSQLDataRetrieve(Context context) {



        try {
            List<SOSGetSet> sosGetSets = dataBaseHandler.getAllLocalDataSOS();

            for (SOSGetSet walking : sosGetSets) {

                sosArrListEventBatterydb.add(walking.getEvent_battery());
                sosArrListStTimestampdb.add(walking.getSt_timestamp());
                sosArrListHrMeasurementdb.add(walking.getHr_measurement());

            }
            //  Toast.makeText(context.getApplicationContext(), "SOS SQL data Uploading..." + sosArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

            if (!sosArrListEventBatterydb.isEmpty()) {



                String url_login = context.getResources().getString(R.string.ws_sos_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);

                try {
                    //Toast.makeText(context_new.getApplicationContext(), "ARRAY_LENGTH_1:-" + sosArrEVENT_BATTERY.size(), Toast.LENGTH_SHORT).show();

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < sosArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", sosArrListEventBatterydb.get(i));
                        jObjd.put("HR_MEASUREMENT", sosArrListHrMeasurementdb.get(i));
                        jObjd.put("ST_TIMESTAMP", sosArrListStTimestampdb.get(i));

                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);
                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();


                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                                    dataBaseHandler.deleteLocalDataSOS();

                                      /*  if (Integer.parseInt(code) == 0) {
                                                for (int i = 0; i < sosArrListEventBatterydb.size(); i++) {
                                                    dataBaseHandler.deleteLocalDataSOS();

                                                }
                                        }*/

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());


                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.sos");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    // Toast.makeText(context_new.getApplicationContext(), "NSDsos_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");



                } catch (JSONException ex) {

                }


            }


        } catch (Exception e) {

            //   Toast.makeText(context.getApplicationContext(), "SOS: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void WaveSQLDataRetrieve(Context context) {



        try {
            List<WaveGetSet> waveGetSets = dataBaseHandler.getAllLocalDataWave();


            for (WaveGetSet waveGetSet : waveGetSets) {

                waveArrListEventBatterydb.add(waveGetSet.getEvent_battery());
                waveArrListStTimestampdb.add(waveGetSet.getSt_timestamp());
                waveArrListEndTimestampdb.add(waveGetSet.getEnd_timestamp());
                waveArrListHrMeasurementdb.add(waveGetSet.getHr_measurement());
                waveArrListAttentionRequestdb.add(waveGetSet.getAttention_request());
            }

            // Toast.makeText(context.getApplicationContext(), "Wave SQL data Uploading..." + waveArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

            if (!waveArrListEventBatterydb.isEmpty()) {


                //Toast.makeText(context.getApplicationContext(), "Wave offline data uploading!" + waveArrListEventBattery.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_wave_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //  new BKWaveArrayTaskNew().execute(urlEncoded);

                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < waveArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", waveArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", waveArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", waveArrListEndTimestampdb.get(i));
                        jObjd.put("HR_MEASUREMENT", waveArrListHrMeasurementdb.get(i));
                        jObjd.put("ATTENTION_REQUEST", waveArrListAttentionRequestdb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();
                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                                    if (Integer.parseInt(code) == 0) {
                                        try {
                                            dataBaseHandler.deleteLocalDataWave();
                                              /*  for (int i = 0; i < waveArrListEventBatterydb.size(); i++) {
                                                    dataBaseHandler.deleteLocalDataWave();
                                                }*/
                                        } catch (Exception e) {

                                        }


                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());


                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.wavedetection");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    //Toast.makeText(context_new.getApplicationContext(), "NSDWave_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");


                } catch (JSONException ex) {

                }

            }
        } catch (Exception e) {

            //   Toast.makeText(context.getApplicationContext(), "Wave: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void FallDetectionSQLDataRetrieve(Context context) {


        try {

            List<FalldetectionGetSet> fallLocalDataFallDetections = dataBaseHandler.getAllLocalDataFallDetection();

            for (FalldetectionGetSet fallLocalDataFallDetection : fallLocalDataFallDetections) {

                fallArrListEventBatterydb.add(fallLocalDataFallDetection.getEvent_battery());
                fallArrListStTimestampdb.add(fallLocalDataFallDetection.getSt_timestamp());
                fallArrListEndTimestampdb.add(fallLocalDataFallDetection.getEnd_timestamp());
                fallArrListDistanceWalkeddb.add(fallLocalDataFallDetection.getDistance_walked());
                fallArrListNumberOfStepsdb.add(fallLocalDataFallDetection.getNum_of_steps());
                fallArrListStepImpactdb.add(fallLocalDataFallDetection.getStep_impact());
                fallArrListStepLengthdb.add(fallLocalDataFallDetection.getStep_length());
                fallArrListForceLeveldb.add(fallLocalDataFallDetection.getFall_gforce_level());
                fallArrListFallConfidencedb.add(fallLocalDataFallDetection.getFall_confidence());

            }

            // Toast.makeText(context.getApplicationContext(), "Fall offline data uploading!" + fallArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

            if (!fallArrListEventBatterydb.isEmpty()) {
                String url_login = context.getResources().getString(R.string.ws_falldetection_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //  new BKFallArrayTaskNew().execute(urlEncoded);
                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < fallArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", fallArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", fallArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", fallArrListEndTimestampdb.get(i));
                        jObjd.put("DISTANCE_WALKED", fallArrListDistanceWalkeddb.get(i));
                        jObjd.put("NUMBER_OF_STEPS", fallArrListNumberOfStepsdb.get(i));
                        jObjd.put("STEP_LENGTH", fallArrListStepLengthdb.get(i));
                        jObjd.put("IMPACT", fallArrListStepImpactdb.get(i));
                        jObjd.put("FALL_G_FORCE_LEVEL", fallArrListForceLeveldb.get(i));
                        jObjd.put("FALL_CONFIDENCE", fallArrListFallConfidencedb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();
                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");


                                    if (Integer.parseInt(code) == 0) {


                                        try {
                                            dataBaseHandler.deleteLocalDataFallDetection();
                                               /* for (int i = 0; i < fallArrListEventBatterydb.size(); i++) {

                                                   dataBaseHandler.deleteLocalDataFallDetection();

                                                }*/
                                        } catch (Exception e) {

                                        }

                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());


                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.falldetection");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    //Toast.makeText(context_new.getApplicationContext(), "NSDfall_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");

                } catch (JSONException ex) {

                }
            }

        } catch (Exception e) {
            //  Toast.makeText(context.getApplicationContext(), "Fall Detection: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void WalkSQLDataRetrieve(Context context) {


        try {
            List<WalkGetSet> walkGetSets = dataBaseHandler.getAllLocalDataWalk();
            ArrayList<String> arrEventBatterydb = new ArrayList<>();
            ArrayList<String> arrStTimestampdb = new ArrayList<>();
            ArrayList<String> arrEndTimestampdb = new ArrayList<>();
            ArrayList<String> arrDistanceWalkeddb = new ArrayList<>();
            ArrayList<String> arrNumberOfStepsdb = new ArrayList<>();
            ArrayList<String> arrStepLengthdb = new ArrayList<>();
            ArrayList<String> arrStepImpactdb = new ArrayList<>();
            ArrayList<String> arrInactivityScoredb = new ArrayList<>();

            for (WalkGetSet walking : walkGetSets) {
                arrEventBatterydb.add(walking.getEvent_battery());
                arrStTimestampdb.add(walking.getSt_timestamp());
                arrEndTimestampdb.add(walking.getEnd_timestamp());
                arrDistanceWalkeddb.add(walking.getDistance_walked());
                arrNumberOfStepsdb.add(walking.getNum_of_steps());
                arrStepLengthdb.add(walking.getStep_length());
                arrStepImpactdb.add(walking.getImpact());
                arrInactivityScoredb.add(walking.getInactivity_score());

            }


            for (int i = 0; i < arrEventBatterydb.size(); i++) {

                ArrayList<String> arrEventBatterydb1 = new ArrayList(Arrays.asList(arrEventBatterydb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrStTimestampdb1 = new ArrayList(Arrays.asList(arrStTimestampdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrEndTimestampdb1 = new ArrayList(Arrays.asList(arrEndTimestampdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrDistanceWalkeddb1 = new ArrayList(Arrays.asList(arrDistanceWalkeddb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrNumberOfStepsdb1 = new ArrayList(Arrays.asList(arrNumberOfStepsdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrStepLengthdb1 = new ArrayList(Arrays.asList(arrStepLengthdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrStepImpactdb1 = new ArrayList(Arrays.asList(arrStepImpactdb.get(i).split("\\s*,\\s*")));
                ArrayList<String> arrInactivityScoredb1 = new ArrayList(Arrays.asList(arrInactivityScoredb.get(i).split("\\s*,\\s*")));

                for (int j = 0; j < arrEventBatterydb1.size(); j++) {
                    arrEventBatterydbMain.add(arrEventBatterydb1.get(j));
                    arrStTimestampdbMain.add(arrStTimestampdb1.get(j));
                    arrEndTimestampdbMain.add(arrEndTimestampdb1.get(j));
                    arrDistanceWalkeddbMain.add(arrDistanceWalkeddb1.get(j));
                    arrNumberOfStepsdbMain.add(arrNumberOfStepsdb1.get(j));
                    arrStepLengthdbMain.add(arrStepLengthdb1.get(j));
                    arrStepImpactdbMain.add(arrStepImpactdb1.get(j));
                    arrInactivityScoredbMain.add(arrInactivityScoredb1.get(j));
                }

            }

            if (!arrEventBatterydbMain.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "Walk SQL data Uploading..." + arrEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_walk);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //    new BKTWalkArrayTaskNew().execute(urlEncoded);

                try {

                    // JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < arrEventBatterydbMain.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", arrEventBatterydbMain.get(i));
                        jObjd.put("ST_TIMESTAMP", arrStTimestampdbMain.get(i));
                        jObjd.put("END_TIMESTAMP", arrEndTimestampdbMain.get(i));
                        jObjd.put("DISTANCE_WALKED", arrDistanceWalkeddbMain.get(i));
                        jObjd.put("NUMBER_OF_STEPS", arrNumberOfStepsdbMain.get(i));
                        jObjd.put("STEP_LENGTH", arrStepLengthdbMain.get(i));
                        jObjd.put("IMPACT", arrStepImpactdbMain.get(i));
                        jObjd.put("INACTIVITY_SCORE", arrInactivityScoredbMain.get(i));
                        jsonArr.put(jObjd);

                    }

                    //jsonObj.put("data", jsonArr);
                    //  jsonObj.put("IMEI", imei_common);

                    JSONObject walkjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    walkjsonObj.put("i_source_data", walkdata.toString());
                    walkjsonObj.put("i_data_source", "nsd.smartwatch.walking");
                    walkjsonObj.put("i_imei", imei_common);


                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {

                                if(getcode().equals("200")) {
                                    String result = getResult();
                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");

                                    if (Integer.parseInt(code) == 0) {
                                        try {
                                            dataBaseHandler.deleteLocalDataWalk();
                                        } catch (Exception e) {

                                        }


                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, walkjsonObj.toString());


                    new WatchServiceCall(context_new) {

                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    Toast.makeText(context_new.getApplicationContext(), "NSDWalk_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(walkjsonObj.toString(), "SaveNSDSmartWatchData");

                } catch (JSONException ex) {

                }


            }
        } catch (Exception e) {

            //  Toast.makeText(context.getApplicationContext(), "Walking: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void SleepSQLDataRetrieve(Context context) {

        try {
            List<SleepGetSet> sleepGetSets = dataBaseHandler.getAllLocalDataSleep();


            for (SleepGetSet sleepGetSet : sleepGetSets) {

                sleepArrListEventBatterydb.add(sleepGetSet.getEvent_battery());
                sleepArrListStTimestampdb.add(sleepGetSet.getSt_timestamp());
                sleepArrListEndTimestampb.add(sleepGetSet.getEnd_timestamp());
                sleepArrListSolSLeepddb.add(sleepGetSet.getSlp_sol());
                sleepArrListTibSleepsdb.add(sleepGetSet.getSlp_tib());
                sleepArrListTstSleepdb.add(sleepGetSet.getSlp_tst());
                sleepArrListWasoSleepdb.add(sleepGetSet.getSlp_waso());
                sleepArrListSeSleepdb.add(sleepGetSet.getSlp_se());
                sleepArrListPositionChangeSleepdb.add(sleepGetSet.getSlp_position_changes());

            }
            // Toast.makeText(context.getApplicationContext(), "Sleep SQL data uploading..." + sleepArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();

            if (!sleepArrListEventBatterydb.isEmpty()) {
                // Toast.makeText(context.getApplicationContext(), "Sleep SQL data uploading..." + sleepArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_sleep_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //   new BKSleepArrayTaskNew().execute(urlEncoded);


                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < sleepArrListEventBatterydb.size(); i++) {


                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", sleepArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", sleepArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", sleepArrListEndTimestampb.get(i));
                        jObjd.put("SLP_SOL", sleepArrListSolSLeepddb.get(i));
                        jObjd.put("SLP_TIB", sleepArrListTibSleepsdb.get(i));
                        jObjd.put("SLP_TST", sleepArrListTstSleepdb.get(i));
                        jObjd.put("SLP_WASO", sleepArrListWasoSleepdb.get(i));
                        jObjd.put("SLP_SE", sleepArrListSeSleepdb.get(i));
                        jObjd.put("SLP_POSITION_CHANGES", sleepArrListPositionChangeSleepdb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();
                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");
                                    // Toast.makeText(context_new.getApplicationContext(), "SleepOff::"+result, Toast.LENGTH_SHORT).show();

                                    if (Integer.parseInt(code) == 0) {

                                        try {

                                            dataBaseHandler.deleteLocalDataSleep();
                                           /* for (int i = 0; i < tmwArrListEventBatterydb.size(); i++) {

                                                dataBaseHandler.deleteLocalDataTMWalk();



                                            }*/

                                        } catch (Exception e) {

                                        }



                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());


                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.sleep");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    //Toast.makeText(context_new.getApplicationContext(), "NSDWalk_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");


                } catch (JSONException ex) {

                }

            }
        } catch (Exception e) {
            //   Toast.makeText(context.getApplicationContext(), "SLEEP: Exception", Toast.LENGTH_SHORT).show();

        }
    }

    public void ThreeMinWalkSQLDataRetrieve(Context context) {


        try {
            List<ThreeMWalkGetSet> threeMWalkGetSetLists = dataBaseHandler.getAllLocalDataTMWalk();

            for (ThreeMWalkGetSet threeMWalkGetSetList : threeMWalkGetSetLists) {

                tmwArrListEventBatterydb.add(threeMWalkGetSetList.getEvent_battery());
                tmwArrListStTimestampdb.add(threeMWalkGetSetList.getSt_timestamp());
                tmwArrListEndTimestampdb.add(threeMWalkGetSetList.getEnd_timestamp());
                tmwArrListDistanceWalkeddb.add(threeMWalkGetSetList.getDistance_walked());
                tmwArrListNumberOfStepsdb.add(threeMWalkGetSetList.getNum_of_steps());
                tmwArrListStepLengthdb.add(threeMWalkGetSetList.getStep_length());
                tmwArrListStepImpactdb.add(threeMWalkGetSetList.getImpact());

            }
            //  Toast.makeText(context.getApplicationContext(), "3MW SQL data uploading..." + tmwArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
            if (!tmwArrListEventBatterydb.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "3MW SQL data uploading..." + tmwArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_tmw_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //  new BKTMWArrayTaskNew().execute(urlEncoded);

                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < tmwArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", tmwArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", tmwArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", tmwArrListEndTimestampdb.get(i));
                        jObjd.put("DISTANCE_WALKED", tmwArrListDistanceWalkeddb.get(i));
                        jObjd.put("NUMBER_OF_STEPS", tmwArrListNumberOfStepsdb.get(i));
                        jObjd.put("STEP_LENGTH", tmwArrListStepLengthdb.get(i));
                        jObjd.put("IMPACT", tmwArrListStepImpactdb.get(i));
                        jsonArr.put(jObjd);

                    }
                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();
                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");


                                    if (Integer.parseInt(code) == 0) {

                                        try {

                                            dataBaseHandler.deleteLocalDataTMWalk();

                                            /*for (int i = 0; i < tmwArrListEventBatterydb.size(); i++) {

                                               dataBaseHandler.deleteLocalDataTMWalk();

                                            }*/

                                        } catch (Exception e) {

                                        }



                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());



                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.threeminwalk");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    Toast.makeText(context_new.getApplicationContext(), "NSD3minWalk_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");

                } catch (JSONException ex) {

                }

            }
        } catch (Exception e) {

        }
    }

    public void DoffedSQLDataRetrieve(Context context) {


        try {
            List<DoffedGetSet> doffedGetSetLists = dataBaseHandler.getAllLocalDataDoffed();

            for (DoffedGetSet threeMWalkGetSetList : doffedGetSetLists) {

                doffedArrListEventBatterydb.add(threeMWalkGetSetList.getEvent_battery());
                doffedArrListStTimestampdb.add(threeMWalkGetSetList.getSt_timestamp());
                doffedArrListEndTimestampdb.add(threeMWalkGetSetList.getEnd_timestamp());
                doffedArrListDoffedTimedb.add(threeMWalkGetSetList.getDoffed_time());

            }
            if (!doffedArrListEventBatterydb.isEmpty()) {
                //  Toast.makeText(context.getApplicationContext(), "3MW SQL data uploading..." + tmwArrListEventBatterydb.size(), Toast.LENGTH_SHORT).show();
                String url_login = context.getResources().getString(R.string.ws_doffedtime_array);
                String urlEncoded = Uri.encode(url_login, ALLOWED_URI_CHARS);
                //  new BKDoffedArrayTaskNew().execute(urlEncoded);

                try {

                    JSONObject jsonObj = new JSONObject();
                    JSONArray jsonArr = new JSONArray();
                    for (int i = 0; i < doffedArrListEventBatterydb.size(); i++) {

                        JSONObject jObjd = new JSONObject();
                        jObjd.put("EVENT_BATTERY", doffedArrListEventBatterydb.get(i));
                        jObjd.put("ST_TIMESTAMP", doffedArrListStTimestampdb.get(i));
                        jObjd.put("END_TIMESTAMP", doffedArrListEndTimestampdb.get(i));
                        jObjd.put("POS_TOTAL_DOFFED_TIME", doffedArrListDoffedTimedb.get(i));
                        jsonArr.put(jObjd);

                    }

                    jsonObj.put("data", jsonArr);
                    jsonObj.put("IMEI", imei_common);

                    new ServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {




                                if(getcode().equals("200")) {
                                    String result = getResult();
                                    JSONObject jsonObject = new JSONObject(result);
                                    String code = new String(jsonObject.getString("code").getBytes("ISO-8859-1"), "UTF-8");


                                    if (Integer.parseInt(code) == 0) {
                                        try {
                                            dataBaseHandler.deleteLocalDataDoffed();
                                           /* for (int i = 0; i < doffedArrListEventBatterydb.size(); i++) {
                                                dataBaseHandler.deleteLocalDataDoffed();

                                            }*/

                                        } catch (Exception e) {

                                        }

                                    }


                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }


                    }.execute(urlEncoded, jsonObj.toString());


                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonArr);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.doffed");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    // Toast.makeText(context_new.getApplicationContext(), "NSDdoff_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");




                } catch (JSONException ex) {

                }

            }
        } catch (Exception e) {

        }
    }


    //added on 09-feb-2018


    public void HeartRateSQLDataRetrieve(Context context) {
        try {
            List<HashMap> htList = dataBaseHandler.getLocalDataHeartRate();
            JSONObject jsonObj=null;

            if (htList.size()>0) {


                try {

                    HashMap<String, String> hp = htList.get(0);

                    jsonObj = new JSONObject();
                    jsonObj.put("i_imei", imei_common);
                    jsonObj.put("i_event_battery", hp.get("Event_battery"));
                    jsonObj.put("i_st_timestamp", hp.get("St_timestamp"));
                    jsonObj.put("i_hr_measurement", hp.get("Hr_measurement"));

                    // Toast.makeText(context_new.getApplicationContext(), "HROffline::"+jsonObj.toString(), Toast.LENGTH_SHORT).show();


                } catch (JSONException ex) {
                    //    Log.e("REQUEST_EXCEPTION", "" + ex);
                }

            }

            new JavaServiceCall(context_new) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {




                        if(getStatuscode().equals("200")) {
                            String hresp=  getResult();
                            // Toast.makeText(context_new.getApplicationContext(), "HROffline::"+hresp, Toast.LENGTH_SHORT).show();

                            dataBaseHandler.deleteLocalDataHeartRate();


                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }


            }.execute(jsonObj.toString(),"SaveHeartRate");


            JSONObject sleepjsonObj = new JSONObject();
            JSONObject walkdata = new JSONObject();
            walkdata.put("data", jsonObj);

            sleepjsonObj.put("i_source_data", walkdata.toString());
            sleepjsonObj.put("i_data_source", "nsd.smartwatch.heartrate");
            sleepjsonObj.put("i_imei", imei_common);


            new WatchServiceCall(context_new) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {


                        if (getStatuscode().equals("200")) {
                            String hresp = getResult();
                            // Toast.makeText(context_new.getApplicationContext(), "NSDHR_Off:" + hresp, Toast.LENGTH_SHORT).show();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }


            }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");




        } catch (Exception ex) {

        }
    }


    public void respirationRateSQLDataRetrieve(Context context) {



        try {
            List<HashMap> htList = dataBaseHandler.getLocalDataRespirationRate();
            JSONObject jsonObj = null;
            if (htList.size()>0) {




                try {


                    try {
                        HashMap<String,String> hp=htList.get(0);

                        jsonObj = new JSONObject();
                        jsonObj.put("i_imei", imei_common);
                        jsonObj.put("i_event_battery", hp.get("Event_battery"));
                        jsonObj.put("i_st_timestamp", hp.get("rr_timestamp"));
                        jsonObj.put("i_rr_measurement", hp.get("rr_measurement"));


                        //Toast.makeText(context_new.getApplicationContext(), "RROffline::"+jsonObj.toString(), Toast.LENGTH_SHORT).show();

                    } catch (JSONException ex) {

                    }

                    new JavaServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {
                                if(getStatuscode().equals("200")) {
                                    String rres=getResult();
                                    //Toast.makeText(context_new.getApplicationContext(), "RROffline::"+rres, Toast.LENGTH_SHORT).show();
                                    dataBaseHandler.deleteLocalDataRespirationRate();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.execute(jsonObj.toString(),"SaveRespirationRate");


                    JSONObject sleepjsonObj = new JSONObject();
                    JSONObject walkdata = new JSONObject();
                    walkdata.put("data", jsonObj);

                    sleepjsonObj.put("i_source_data", walkdata.toString());
                    sleepjsonObj.put("i_data_source", "nsd.smartwatch.respirationrate");
                    sleepjsonObj.put("i_imei", imei_common);


                    new WatchServiceCall(context_new) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {


                                if (getStatuscode().equals("200")) {
                                    String hresp = getResult();
                                    //Toast.makeText(context_new.getApplicationContext(), "NSDRR_Off:" + hresp, Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        }


                    }.execute(sleepjsonObj.toString(), "SaveNSDSmartWatchData");



                } catch (Exception ex) {

                }
            }


        } catch (Exception e) {



        }
    }

    public void SaveNSDSmartWatchData(String datasrc,String sourcedata) {
        try {




            JSONObject walkjsonObj = new JSONObject();
               /* {
                    "i_imei": "362531821612104",
                        "i_data_source": "nsd.smartwatch.walking",
                        "i_source_data": "{\"data\":\"test\"}"
                }*/


            walkjsonObj.put("i_source_data", datasrc);
            walkjsonObj.put("i_data_source", sourcedata);
            walkjsonObj.put("i_imei", imei_common);



            new JavaServiceCall(context_new) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {


                        if (getStatuscode().equals("200")) {
                            String hresp = getResult();
                            // Toast.makeText(context_new.getApplicationContext(), "NSDWalk_On:" + hresp, Toast.LENGTH_SHORT).show();




                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }


            }.execute(walkjsonObj.toString(), "SaveNSDSmartWatchData");

        } catch (Exception ex) {

        }
    }


    public void getRuntimeConfigValues(Context context) {
        try {




            JSONObject rtjsonObj = new JSONObject();


            rtjsonObj.put("i_imei", imei_common);

            Toast.makeText(context_new.getApplicationContext(), "Config:" + imei_common, Toast.LENGTH_SHORT).show();


            new JavaServiceCall(context_new) {
                @Override
                protected void onPostExecute(Void unused) {
                    try {


                        if (getStatuscode().equals("200")) {
                            String hresp = getResult();


                          //  con = context_new.createPackageContext("com.sharedpref1", 0);//first app package name is "com.sharedpref1"

                          /*  Intent sendIntent = new Intent();
                          //  sendIntent.setClassName("de.fraunhofer.igd.serversync","de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");

                            ComponentName componentName=new ComponentName("de.fraunhofer.igd.nsddemonstrator","CONFIGURATION_MESSAGE");
                            sendIntent.setComponent(componentName);
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra("ConfigValues", hresp);
                            sendIntent.setType("text/plain");
                            context_new.startActivity(sendIntent);*/


                            final Intent sendIntent = new Intent();

                            //  sendIntent.setAction("com.nsd.ncare.beta.network.MyReceive");

                            sendIntent.setAction("de.fraunhofer.igd.nsddemonstrator.CONFIGURATION_MESSAGE");
                            sendIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                            sendIntent.putExtra("CONTENT_STRING",hresp);
                            context_new.sendBroadcast(sendIntent);



                            // Toast.makeText(context_new.getApplicationContext(), "NSDWalk_On:" + hresp, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }


            }.execute(rtjsonObj.toString(), "GetRuntimeConfigValues");

        } catch (Exception ex) {

        }
    }

    public void clearAll()
    {
        hrArrListEventBattery = new ArrayList<>();
        hrArrListStTimestamp = new ArrayList<>();
        hrArrListHrMeasurement = new ArrayList<>();

        dfArrListEventBattery = new ArrayList<>();
        dfArrListStTimestamp = new ArrayList<>();
        dfArrListTotalDoffedTime = new ArrayList<>();

        tranArrListEventBattery = new ArrayList<>();
        tranArrListStTimestamp = new ArrayList<>();
        tranArrListCurrentPosture = new ArrayList<>();
        tranArrListTransitionMode = new ArrayList<>();
        tranArrListPosTransitionTime = new ArrayList<>();
        tranArrListPosTransitionIntensity = new ArrayList<>();

        walkArrListEventBattery = new ArrayList<>();
        walkArrListStTimestamp = new ArrayList<>();
        walkArrListEndTimestamp = new ArrayList<>();
        walkArrListDistanceWalked = new ArrayList<>();
        walkArrListNumberOfSteps = new ArrayList<>();
        walkArrListStepLength = new ArrayList<>();
        walkArrListStepImpact = new ArrayList<>();
        walkArrListInactivityScore = new ArrayList<>();




        sosArrListEventBatterydb = new ArrayList<>();
        sosArrListStTimestampdb = new ArrayList<>();
        sosArrListHrMeasurementdb = new ArrayList<>();

        // Walk SQL Array
        arrEventBatterydbMain = new ArrayList<>();
        arrStTimestampdbMain = new ArrayList<>();
        arrEndTimestampdbMain = new ArrayList<>();
        arrDistanceWalkeddbMain = new ArrayList<>();
        arrNumberOfStepsdbMain = new ArrayList<>();
        arrStepLengthdbMain = new ArrayList<>();
        arrStepImpactdbMain = new ArrayList<>();
        arrInactivityScoredbMain = new ArrayList<>();

        //TUG SQL Array
        tugArrListEventBatterydb = new ArrayList<>();
        tugArrListStTimestampdb = new ArrayList<>();
        tugArrListEndTimestampdb = new ArrayList<>();
        tugArrListDistanceWalkeddb = new ArrayList<>();
        tugArrListNumberOfStepsdb = new ArrayList<>();
        tugArrListInactivityScoredb = new ArrayList<>();

        //Wave SQL Array
        waveArrListEventBatterydb = new ArrayList<>();
        waveArrListStTimestampdb = new ArrayList<>();
        waveArrListEndTimestampdb = new ArrayList<>();
        waveArrListHrMeasurementdb = new ArrayList<>();
        waveArrListAttentionRequestdb = new ArrayList<>();

        //Fall Detection SQL Array
        fallArrListEventBatterydb = new ArrayList<>();
        fallArrListStTimestampdb = new ArrayList<>();
        fallArrListEndTimestampdb = new ArrayList<>();
        fallArrListDistanceWalkeddb = new ArrayList<>();
        fallArrListNumberOfStepsdb = new ArrayList<>();
        fallArrListStepImpactdb = new ArrayList<>();
        fallArrListStepLengthdb = new ArrayList<>();
        fallArrListForceLeveldb = new ArrayList<>();
        fallArrListFallConfidencedb = new ArrayList<>();

        //Sleep SQL Array
        sleepArrListEventBatterydb = new ArrayList<>();
        sleepArrListStTimestampdb = new ArrayList<>();
        sleepArrListEndTimestampb = new ArrayList<>();
        sleepArrListSolSLeepddb = new ArrayList<>();
        sleepArrListTibSleepsdb = new ArrayList<>();
        sleepArrListTstSleepdb = new ArrayList<>();
        sleepArrListWasoSleepdb = new ArrayList<>();
        sleepArrListSeSleepdb = new ArrayList<>();
        sleepArrListPositionChangeSleepdb = new ArrayList<>();

        //3 Min Walk SQL Array
        tmwArrListEventBatterydb = new ArrayList<>();
        tmwArrListStTimestampdb = new ArrayList<>();
        tmwArrListEndTimestampdb = new ArrayList<>();
        tmwArrListDistanceWalkeddb = new ArrayList<>();
        tmwArrListNumberOfStepsdb = new ArrayList<>();
        tmwArrListStepLengthdb = new ArrayList<>();
        tmwArrListStepImpactdb = new ArrayList<>();

        //Transaction SQL Array
        tranArrListEventBatterydbMain = new ArrayList<>();
        tranArrListStTimestampdbMain = new ArrayList<>();
        tranArrListCurrentPosturedbMain = new ArrayList<>();
        tranArrListTransitionModedbMain = new ArrayList<>();
        tranArrListPosTransitionTimedbMain = new ArrayList<>();
        tranArrListPosTransitionIntensitydbMain = new ArrayList<>();

        //Doffed SQL Array
        doffedArrListEventBatterydb = new ArrayList<>();
        doffedArrListStTimestampdb = new ArrayList<>();
        doffedArrListEndTimestampdb = new ArrayList<>();
        doffedArrListDoffedTimedb = new ArrayList<>();
    }





}
