package background;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;


public class ConnectivityReceiver extends BroadcastReceiver {
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static ConnectivityReceiverListener connectivityReceiverListener;

    Context context;
    SyncOffline synoflne;

    public BatteryLevelReceiver mBatInfoReceiver = null;


    public ConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        this.context=context;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
        }

        Toast.makeText(context.getApplicationContext(), getConnectivityStatusString(context), Toast.LENGTH_SHORT).show();


       /* if(mBatInfoReceiver==null) {
            mBatInfoReceiver = new BatteryLevelReceiver();
            IntentFilter ifilter = new IntentFilter();
            ifilter.addAction(Intent.ACTION_BATTERY_CHANGED);
            context.getApplicationContext().registerReceiver(mBatInfoReceiver, ifilter);
        }*/

        if (getConnectivityStatus(context) == 1) {

            mBatInfoReceiver = new BatteryLevelReceiver();

            IntentFilter ifilter = new IntentFilter();
            // ifilter.addAction(Intent.ACTION_BATTERY_LOW);

            ifilter.addAction(Intent.ACTION_BATTERY_CHANGED);
            context.getApplicationContext().registerReceiver(mBatInfoReceiver,ifilter);




            synoflne=new SyncOffline(context);

            synoflne.getRuntimeConfigValues(context);


            synoflne.TransactionSQLDataRetrieve(context);
            synoflne.TugSQLDataRetrieve(context);
            synoflne.SOSSQLDataRetrieve(context);
            synoflne.WaveSQLDataRetrieve(context);
            synoflne.FallDetectionSQLDataRetrieve(context);
            synoflne.WalkSQLDataRetrieve(context);
            synoflne.SleepSQLDataRetrieve(context);
            synoflne.ThreeMinWalkSQLDataRetrieve(context);

            synoflne.HeartRateSQLDataRetrieve(context);

            synoflne.respirationRateSQLDataRetrieve(context);

            synoflne.DoffedSQLDataRetrieve(context);




        }
        if (getConnectivityStatus(context) == 0) {
           /* if(mBatInfoReceiver!= null)
                context.getApplicationContext().unregisterReceiver(mBatInfoReceiver);*/

        }
    }
    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }
    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Internet Disconnected";
        }
        return status;
    }



    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }


}
