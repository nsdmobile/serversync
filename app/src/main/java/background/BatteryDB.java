package background;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import adapter.DoffedGetSet;
import adapter.FalldetectionGetSet;
import adapter.SOSGetSet;
import adapter.SleepGetSet;
import adapter.TUGGetSet;
import adapter.ThreeMWalkGetSet;
import adapter.TransactionGetSet;
import adapter.WalkGetSet;
import adapter.WaveGetSet;

/**
 * Created by Saidulu on 12/21/2017.
 */

public class BatteryDB extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "nsdserversyncbatterydb";






    // Table for battery status update

    // battery table name
    private static final String TABLE_BATTERY = "nsdserversyncbattery";
    // battery Table Columns names
    private static final String KEY_ID_BATTERYIMEI = "idbatteryimei";
    private static final String KEY_BATTERY_STATUS = "keybatterystatus";
    private static final String KEY_BATTERY_LASTUPDATE = "keybatterylastupdate";



    public BatteryDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {


        String CREATE_TABLE_BATTERY = "CREATE TABLE " + TABLE_BATTERY + "("
                + KEY_ID_BATTERYIMEI + " INTEGER PRIMARY KEY,"
                + KEY_BATTERY_STATUS + " TEXT,"
                + KEY_BATTERY_LASTUPDATE + " TEXT" + ")";





        db.execSQL(CREATE_TABLE_BATTERY);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BATTERY);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    public void addBatteryData(String bat_imei,String bat_level,String bat_lut) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_BATTERYIMEI, bat_imei);
        values.put(KEY_BATTERY_STATUS, bat_level);
        values.put(KEY_BATTERY_LASTUPDATE, bat_lut);




        // Inserting Row
        db.insert(TABLE_BATTERY, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip
    HashMap  getBatteryData(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap<String,String> batList=new HashMap<>();

        Cursor cursor = db.query(TABLE_BATTERY, new String[]{KEY_ID_BATTERYIMEI,
                        KEY_BATTERY_STATUS, KEY_BATTERY_LASTUPDATE}, KEY_ID_BATTERYIMEI + "=?",
                new String[]{id}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        batList.put(KEY_ID_BATTERYIMEI,cursor.getString(0));
        batList.put(KEY_BATTERY_STATUS,cursor.getString(1));
        batList.put(KEY_BATTERY_LASTUPDATE,cursor.getString(2));


        // return contact
        return batList;

    }

    public int  getBatteryCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        int cnt=0;
        // Select All Query
        String selectQuery = "SELECT  * FROM "+TABLE_BATTERY;

        Log.e("selectQuery", selectQuery);



        Cursor cursor = db.rawQuery(selectQuery, null);

        cnt=cursor.getCount();





        return cnt;

    }




    // Deleting single trip
    public void deleteLocalBatteryData() {
        SQLiteDatabase db = this.getWritableDatabase();
        //  db.delete(TABLE_WALK, KEY_ID + " = ?", new String[]{String.valueOf(walkGetSet.getId())});
        db.execSQL("delete from " + TABLE_BATTERY);

        db.close();
    }
    public int updateBatteryData(String imei,String batlevel,String syncdate) {
        int ret=0;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_BATTERY_STATUS, batlevel);
        values.put(KEY_BATTERY_LASTUPDATE, syncdate);




        // Log.e("key_newid",""+key_newid);
        ret= db.update(TABLE_BATTERY, values,KEY_ID_BATTERYIMEI + " = ?",
                new String[] { imei });



        Log.e("userdate "+KEY_ID_BATTERYIMEI,""+ret);

        return ret;
    }


}

