package background;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by admin on 6/21/2017.
 */

public class ServiceCall extends AsyncTask<String, Void, Void> {

    private static final String TAG = "ServiceLoginPost";
    public static String postUrl;

   // public Dialog _dialog = null;
    protected Context context = null;
    protected String reply;
    protected String Error = null;
    protected boolean isError = false;

    protected String scode="";


    public ServiceCall() {
        super();
    }

    public ServiceCall(Context context) {


        this.context = context;

    }



    public String getcode() {



        if(scode.length()==0 || scode==null)
            scode="null";

        return scode;
    }
    public String getResult() {


        if (reply != null)
            return reply;
        else if (isError && Error != null)
            return Error;
        else
            return "Failed to Connect to Server.";
    }



    @Override
    protected void onPostExecute(Void result) {
        context = null;




    }

    @Override
    public void onPreExecute() {


    }


    @Override
    protected Void doInBackground(String... params) {

        scode="";
        String requestString = null;

        if ((params != null) && (params.length > 1)) {
            requestString = params[1];

            postUrl = params[0];


        } else {


            requestString = params[0];




        }


        try {

            reply = postConnection(requestString);


        } catch (Exception ex) {



            scode = "";



        }
        return null;
    }



    public String postConnection(String postDataParams) {


        String webPage = "";
        try {





            URL hurl = new URL(postUrl);
            HttpURLConnection conn = (HttpURLConnection) hurl.openConnection();



            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));



            writer.write(postDataParams);
            writer.flush();
            writer.close();

            os.close();


            InputStream error = conn.getErrorStream();
            InputStream in = new BufferedInputStream(conn.getInputStream());

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                int responseCode = conn.getResponseCode();



                scode = String.valueOf(responseCode);


                BufferedReader bin = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = bin.readLine()) != null) {
                    sb.append(line);

                }
                webPage = sb.toString();

                in.close();


            } else
            {

                scode = String.valueOf(conn.getResponseCode());

            }


        } catch (Exception e) {
            e.printStackTrace();



        }

        return webPage;
    }








}

