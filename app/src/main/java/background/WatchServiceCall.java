package background;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class WatchServiceCall extends AsyncTask<String, Void, Void> {





    public static String smwatchurl="http://35.198.106.148:9080/api/NSDData/";




    protected Context context = null;
    protected String reply;
    protected String Error = null;
    protected boolean isError = false;

    protected String scode="";
    public WatchServiceCall() {
        super();
    }

    public WatchServiceCall(Context context) {
        this.context = context;

    }




    public String getStatuscode() {
        if(scode.length()==0 || scode==null)
            scode="null";

        return scode;
    }
    public String getResult() {
        if (reply != null)
            return reply;
        else if (isError && Error != null)
            return Error;
        else
            return "Failed to Connect to Server.";
    }



    @Override
    protected void onPostExecute(Void result) {
        context = null;




    }

    @Override
    public void onPreExecute() {


    }


    @Override
    protected Void doInBackground(String... params) {

        scode="";
        String requestString = null;

        String javaUrl="";
        try {

            if ((params != null) && (params.length > 1)) {
                //postUrl = params[0];
                requestString = params[0];
                javaUrl= smwatchurl+params[1];
            }

            Log.e("requestString",requestString);

            Log.e("javaUrl",javaUrl);

            reply = postConnection(requestString,javaUrl);

        } catch (Exception ex) {
            scode = "";
        }
        return null;
    }



    public String postConnection(String postDataParams,String jurl) {


        String webPage = "";
        try {

            URL hurl = new URL(jurl);
            HttpURLConnection conn = (HttpURLConnection) hurl.openConnection();


            Log.e("postConnection",jurl);


            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

          //  Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJoYzFAbnNkLmNvbSIsInJvbGUiOiJVU0VSIiwiaWQiOiJoYzFAbnNkLmNvbSIsImlhdCI6MTUxMzYwODE4MH0.-Jv4Copx0Ur8TneD5NAd88-lAApa9Xq0uggmMu_Xzu8Z1Ikt8Gj22Y4p50BJFqsbtc4NuzSIEtygAb1lzzEOug
            conn.setRequestProperty("Authorization","Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJoYzFAbnNkLmNvbSIsInJvbGUiOiJVU0VSIiwiaWQiOiJoYzFAbnNkLmNvbSIsImlhdCI6MTUxMzYwODE4MH0.-Jv4Copx0Ur8TneD5NAd88-lAApa9Xq0uggmMu_Xzu8Z1Ikt8Gj22Y4p50BJFqsbtc4NuzSIEtygAb1lzzEOug");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));



            writer.write(postDataParams);
            writer.flush();
            writer.close();

            os.close();


            Log.e("getResponseCode",""+conn.getResponseCode());



            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                int responseCode = conn.getResponseCode();

                InputStream error = conn.getErrorStream();
                InputStream in = new BufferedInputStream(conn.getInputStream());

                scode = String.valueOf(responseCode);


                BufferedReader bin = new BufferedReader(new InputStreamReader(in));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = bin.readLine()) != null) {
                    sb.append(line);

                }
                webPage = sb.toString();

                in.close();


            } else
            {

                scode = String.valueOf(conn.getResponseCode());

            }

            Log.e("scode",scode);


        } catch (Exception e) {
            e.printStackTrace();


            Log.e("Exception",e.getMessage());


        }

        return webPage;
    }





}
