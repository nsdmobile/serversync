package background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;



import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;


public class  BatteryLevelReceiver extends BroadcastReceiver {

    String imei_common;

    int batteryLevel;
    BatteryDB batterydb;

    @Override
    public void onReceive(Context context, Intent batteryStatus) {

      //  String action = batteryStatus.getAction();



        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        batteryLevel=(int)(((float)level / (float)scale) * 100.0f);

    //    String batper="Battery::"+String.valueOf(batteryLevel)+"%";


      //  Toast.makeText(context.getApplicationContext(), action, Toast.LENGTH_SHORT).show();


       int deviceStatus = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS,-1);

       /* if(deviceStatus == BatteryManager.BATTERY_STATUS_CHARGING){

           Toast.makeText(context,"Battery Status = Charging ",Toast.LENGTH_SHORT).show();

        }

        if(deviceStatus == BatteryManager.BATTERY_STATUS_DISCHARGING){

            Toast.makeText(context,"Battery Status = Discharging ",Toast.LENGTH_SHORT).show();


        }*/

        if (deviceStatus == BatteryManager.BATTERY_STATUS_FULL){

            batterydb=new BatteryDB(context);

           // Toast.makeText(context,"Battery Status = Full ",Toast.LENGTH_SHORT).show();
            imei_common = CommonMethods.getIMEI(context);

            batteryUpdate(context,100);



        }


        //   float batteryPct = level / (float)scale;
        //"Level::"+String.valueOf(level) +"





        batterydb=new BatteryDB(context);

        int dbstatus=batterydb.getBatteryCount();

        Log.e("local_dbstatus",""+dbstatus);

        if(dbstatus==0)
        {
            long cumisecs= Calendar.getInstance().getTimeInMillis();
            imei_common = CommonMethods.getIMEI(context);
            batterydb.addBatteryData(imei_common,String.valueOf(batteryLevel),String.valueOf(cumisecs));
            if(batteryLevel<20 || batteryLevel==100)
                batteryUpdate(context,batteryLevel);
        }
        else
        {

            imei_common = CommonMethods.getIMEI(context);
           HashMap<String,String> lupdate = batterydb.getBatteryData(imei_common);


            long cumisecs = Long.parseLong(lupdate.get("keybatterylastupdate"));
            long synctime = currentTime(cumisecs);

            int batlocstatus=Integer.parseInt(lupdate.get("keybatterystatus"));
            Log.e("onresume", "" + synctime);
            Log.e("batlocstatus", "" + batlocstatus);

            if(batteryLevel<20 && synctime<30) {
                batteryUpdate(context, batteryLevel);
            }
            else if(synctime>30)
            {
                long curmsec=Calendar.getInstance().getTimeInMillis();
                batterydb.updateBatteryData(imei_common,String.valueOf(batteryLevel),String.valueOf(curmsec));

            }

        }



      /*  if(this!= null)
            context.getApplicationContext().unregisterReceiver(this);*/

    }

    public void batteryUpdate(Context context,final int blevel)
    {




                try {


                    JSONObject jsonObj = new JSONObject();

                    jsonObj.put("i_imei", imei_common);

                    if(blevel<10)
                        jsonObj.put("i_notification_type_id","110010");
                    else if(blevel<20)
                        jsonObj.put("i_notification_type_id","110020");

                    else if(blevel==100)
                        jsonObj.put("i_notification_type_id","110100");

                    Log.e("jsonObj",jsonObj.toString());
                   new JavaServiceCall(context) {
                        @Override
                        protected void onPostExecute(Void unused) {
                            try {
                                if(getStatuscode().equals("200")) {

                                    if(BatteryLevelReceiver.this!= null)
                                        context.getApplicationContext().unregisterReceiver(BatteryLevelReceiver.this);


                                    String result = getResult();

                                    Log.e("result",result);


                                    long cumisecs= Calendar.getInstance().getTimeInMillis();

                                    batterydb.updateBatteryData(imei_common,String.valueOf(blevel),String.valueOf(cumisecs));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                        }



                    }.execute(jsonObj.toString(),"GenerateNotification");

                } catch (Exception ex) {

                }






    }

    public int currentTime(long synctime)
    {
        int minutes=0;
        long cumisecs= Calendar.getInstance().getTimeInMillis();

        long shour=cumisecs-synctime;

        minutes = (int) ((shour / 1000) / 60);

        return  minutes;

    }


}
